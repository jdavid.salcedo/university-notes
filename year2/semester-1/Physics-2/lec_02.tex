\lecture{2}{Mon 15 Mar 18:37}{Divergence and Curl of electrostatic fields}

\subsection*{Field lines, flux, and Gauss's Law}

Henceforth we will be devoted to the development of tools that can allow us to avoid the computation of formidable integrals which, turns out, may arise even when considering reasonable charge distributions. As of now, this amounts to finding the divergence and curl of $\mathbf{E}$. 

Perhaps it would be appropriate to draw build up some intuition with regards to calculating the divergence of the electric field. Suppose that there is a single charge $q$ situated at the origin. Then the electric field due to such a charge at a given point whose position is $\mathbf{r}$ is 
\[
        \mathbf{E}(\mathbf{r})=\frac{1}{4\pi\epsilon_0}\frac{q}{r^2}\hat{\mathbf{r}}.
\] 
Obviously, the intensity of this field falls off like $1 /r^2$; thus the magnitude of the vectors decreases as their distance from the origin increases. This can naturally be represented with arrows pointing radially outward that get shorter as they get farther away from the centre; but there is a nicer way to sketch the same situation, and that is to connect up the arrows to form \textbf{field lines}. This kind of picture should be interpreted not in terms of the length of the arrows, but rather in terms of the density of the field lines: The field is more intense near the centre, where the lines are close together, and weaker farther out, where they are far apart. 

\begin{figure}[ht]
    \centering
    \incfig[0.3]{field-lines}
    \caption{field lines}
    \label{fig:field-lines}
\end{figure}
Such an image is, however, quite deceptive when viewed on a two-dimensional setting, for the density of lines passing through a circle of radius $r$ is the total number of lines divided by the circumference ($n / 2\pi r$), which certainly does not abide by the inverse-square definition of the electric field. In a three-dimensional model, though, the density of lines passing through the surface of a sphere is given by $n /4\pi r^2$, which behaves as expected. Another point to note is that field lines \emph{begin on positive charges and end on negative ones, they cannot terminate mid-air} (if they did, the divergence of $\textbf{E}$would not be zero, and that cannot happen in empty space).

In this model of field lines, the \textbf{flux} of $\mathbf{E}$ through a surface $S$,
 \[
         \Phi_E = \iint_S \mathbf{E}\cdot d\mathbf{a},
\] 
is a measure of ``number of field lines'' passing through $S$. Of course, the dot product here suggests that we are interested in finding such measure with respect to the area which is \emph{perpendicular to the field lines}. Moreover, the flux through any closed surface represents a measure of the total charge inside, for field lines originating on a positive charge must either pass out through the surface, or terminate on a negative charge inside; on the other hand the charge outside the closed surface will not contribute anything to the total flux, since its lines pass in one side, and out the other. These facts account for what is known as \textbf{Gauss's Law}.

Suppose there is a point charge $q$ at the origin, the flux of $\mathbf{E}$ through a sphere of radius $r$ is
 \[
         \oiint_S \mathbf{E}\cdot d\mathbf{a} = \iint_S \frac{1}{4\pi\epsilon_0} \left( \frac{q}{r^2} \hat{\mathbf{r}} \right) \cdot ([r d\theta] [r\sin\theta d\varphi]) \hat{\mathbf{r}} = \frac{1}{4\pi\epsilon_0} q \left( \int_0^{\pi}\sin\theta d\theta \right) \left( \int_0^{2\pi}d\varphi \right) = \frac{1}{4\pi\epsilon_0}4\pi q = \frac{1}{\epsilon_0}q.
\]

In fact, we do not require that the surface be a sphere, any closed surface satisfies the condition that the same number of field lines pierce its surface. This intuitively tells us that the flux through any surface enclosing the charge is $q/\epsilon_0$.

Now suppose that instead of a single charge at the origin, we have many charges scattered about. According to the principle of superposition, the total field is the vector sum of all the individual fields:
\[
        \mathbf{E} = \sum_{i=1}^{n} \mathbf{E}_i.
\] 

The flux through a surface that encloses them all is thus
\[
        \oiint_S \mathbf{E}\cdot d\mathbf{a} = \oiint_S \left( \sum_{i=1}^{n} \mathbf{E}_i\cdot d\mathbf{a} \right) = \sum_{i=1}^{n} \left( \oiint_S \mathbf{E}_i\cdot d\mathbf{a} \right) = \sum_{i=1}^{n} \left( \frac{1}{\epsilon_0}q_i \right).  
\] 
For any enclosed surface, then,
\[
        \boxed{\oiint_S \mathbf{E}\cdot d\mathbf{a} = \frac{1}{\epsilon_0}Q_\text{enclosed},}
\]
where $Q_\text{enclosed}$ is the total charge enclosed within the surface. This is Gauss's Law.

The differential form of Gauss's Law can be easily derived by applying the divergence theorem:
\[
        \oiint_S \mathbf{E}\cdot d\mathbf{a} = \iiint_V \text{div }\mathbf{E}\,d\tau.
\]

On the other hand, we can rewrite $Q_\text{enclosed}$ in terms of the charge density $\rho$ :
\[
Q_\text{enclosed}=\iiint_V \rho d\tau.
\] 
Hence Gauss's Law becomes 
\[
        \iiint_V \text{div }\mathbf{E}\,d\tau = \frac{1}{\epsilon_0}\iiint_V \rho d\tau.
\] 
We conclude that
\[
        \boxed{\text{div }\mathbf{E} = \nabla \cdot \mathbf{E} = \frac{1}{\epsilon_o}\rho.}
\] 
