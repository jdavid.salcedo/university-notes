\lecture{2}{Tue 01 Jun 16:57}{Addition}
As of now, the natural number system has not been enriched with any fancy
operation, however, it is about time we introduce new concepts that will
thereupon allow us to derive more complex structures. We must now talk about
\emph{addition}.

Concretely, it works as follows: To add three to five should be the same as
incrementing five three times, which is one increment more than if we had added
two to five, which in turn is one increment more than adding one to five, etc.,
and to add zero to five should just give five. This is of course an inherently
recursive statement, which we pinpoint in the following definition:
\begin{definition}{Addition of natural numbers}{}
        Let $m$ be a natural number. To add 0 to $m$, we define $0 + m := m$.
        Now, suppose that we have defined how to add $n$ to $m$, that is, we
        have defined $n + m$. Then we can add $n\pp$ to $m$ by defining 
         \[
                 (n\pp) + m := (n + m)\pp \quad \text{(one increment more than
                 adding $n$ to $m$)}.
        \] 
\end{definition}

Thus we have
\begin{align*}
        0 + m &:= m,\\
        1 + m &= (0\pp) + m := (0 + m)\pp = m\pp, \\
        2 + m &= (1\pp) + m := (1 + m)\pp = (m\pp)\pp, \\
              &\vdots 
\end{align*}
for instance, we have $2 + 3 = (3\pp)\pp = 4\pp = 5$. This comes down to the
realisation that, by what has been proved in Proposition 1.1.3, we have defined
$n + m$ for every natural number $n$. In fact, if we have some element of the
sequence $a_n = n + m$, then we define $a_{n\pp} = f_n(a_n) = a_n \pp$.

Notice that this definition is asymmetric, i.e., $3 + 5$ is incrementing five
three times, whereas $5 + 3$ is incrementing three five times; even though they
yield the same value of 8. Nevertheless, it is a general fact that, for all
natural numbers $a$ and $b$, $a+b = b+a$.

As a first exercise, we could use Axioms \ref{th:1}, \ref{th:2} and induction
(Axiom \ref{th:5}) to show that \textbf{the sum of two natural numbers is again
a natural number:}
\begin{proof}
        We wish to prove that, if $n$ and $m$ are natural numbers, then $n + m$
        is a natural number. We proceed by induction on on $n$, with fixed $m$.
        To begin with, notice that $0 + m = m$, which is a natural number by
        hypothesis. 

        Now, if we suppose that, for some natural number $n$, $n + m$ is a
        natural number, then we can clearly see that $(n\pp) + m = (n + m)\pp$
        is a natural number because it is just the increment of another natural
        number (Axiom \ref{th:2}). This closes the induction, completing the
        proof.
\end{proof}

The above mentioned facts are enough to derive everything we know about
addition. We begin with some lemmas:

\begin{lemma}{}{}
        For any natural number $n$, we have $n + 0 = n$.
\end{lemma}
Notice that this cannot be immediately deduced from $0 + m = m$, for we do not
yet have commutativity!
\begin{proof}
        We proceed by induction. The base case, $0+0=0$ follows immediately,
        because we have defined $0 + m = m$ for any natural number $m$. We now
        suppose inductively that $n + 0 = n$ for some natural number $n$. Then
        we have, by definition, $(n\pp) + 0 = (n + 0)\pp$; and so, by induction
        hypothesis, $(n\pp) + 0 = n\pp$. This closes the induction.
\end{proof}

\begin{lemma}{}{}
        For any pair of natural numbers $n$ and $m$, $n + (m\pp) = (n + m)\pp$.
\end{lemma}
\begin{proof}
        We induct on $n$, with fixed $m$. For the base case, we firstly have $0
        + (m\pp) = m\pp$, since $0 + a = a$ for all natural $a$; by the same
        definition, we have $0 + (m\pp) = m\pp = (0 + m)\pp$. Now suppose that
        $n + (m\pp) = (n + m)\pp$ for some natural number $n$. Then, by
        definition, $(n\pp) + (m\pp) = (n + (m\pp))\pp$, which in turn is equal
        to $((n+m)\pp)\pp$, by the inductive hypothesis. Finally, our definition
        of addition implies that $(n\pp) + (m\pp) = ((n+m)\pp)\pp =
        ((n\pp)+m)\pp$. This closes the induction. 
\end{proof}

As a particular corollary of Lemma 1.2.1 and Lemma 1.2.2, we have that $n\pp = n
+ 1$:
\begin{proof}
        Once again, we induct on $n$. The base case, $0 + 1 = 0\pp = 1$ just
        follows by definition of addition. Now, suppose inductively that $n + 1
        = n\pp$. Then we have, by Lemma 1.2.2, $(n\pp) + 1 = (n\pp) + (0\pp) =
        ((n\pp) + 0)\pp$, but by Lemma 1.2.1, $((n\pp) + 0)\pp= (n\pp)\pp$. This
        closes the induction.
\end{proof}

Now we are finally in a position to prove commutativity:
\begin{proposition}{Addition is commutative}{}
        For any natural numbers $n$ and $m$, $n+m = m+n$.
\end{proposition}
\begin{proof}
        We proceed by induction on $n$, keeping $m$ fixed. First, we do the base
        case: By definition of addition, $0 + m = m$; on the other hand, by
        Lemma 1.2.1 we know that $m + 0 = m$; thus $0 + m = m + 0$. Now, suppose
        inductively that $n + m = m + n$ for some natural number $n$. Then we
        have $(n\pp) + m = (n + m)\pp$, by definition of addition. However,
        inductive hypothesis (first equality) and Lemma 1.2.2 (second equality)
        imply $(n + m)\pp = (m + n)\pp = m + (n\pp)$. Hence we can conclude that
        $(n\pp) + m = m + (n\pp)$, completing the proof.
\end{proof}
\begin{proposition}{Addition is associative}{}
        For any natural numbers $a, b, c$, we have $(a + b) + c = a + (b + c)$.
\end{proposition}
\begin{proof}
        Left as an exercise. 
\end{proof}

Associativity is certainly desirable, since we can now write sums such as
$a+b+c$ without worrying about the order of each of the terms. 

Now we develop a cancellation law:
\begin{proposition}{Cancellation law}{}
        Let $a,b,c$ be natural numbers such that $a + b = a + c$. Then we have
        $b = c$.
\end{proposition}
Note that we cannot simply subtract, because we have not yet defined what a
negative number is. Instead, we can write the following proof:
\begin{proof}
        We may proceed by induction on $a$. For the first case, notice that if
        $0 + b = 0 + c$, then the definition of addition tells us that $b = c$.
        Now suppose inductively that, for some natural number $a$, if $a + b = a
        + c$, then $b = c$. We can thereafter consider what happens if the
        hypothesis that $(a\pp) + b = (a\pp) + c$ is taken; indeed, this implies
        $(a + b)\pp = (a + c)\pp$, but Axiom \ref{th:4} then allows us to write
        $a+b = a+c$; then the inductive hypothesis tells us that $b = c$. This
        closes the induction.
\end{proof}

We can now turn to the concept of addition in relation with the positivity of
the natural numbers.
\begin{definition}{Positive natural numbers}{}
        A natural number $n$ is said to be positive iff it is nor equal to 0.
\end{definition}
\begin{proposition}{}{}
        If $a$ is positive and $b$ is any natural number, then $a+b$ is
        positive. (And then $b+a$ is also positive, by commutativity of
        addition).
\end{proposition}
\begin{proof}
        We proceed by induction on $b$ and fix $a$ to be any positive natural
        number. Firstly, note that $a + 0 = a$, which is obviously positive.
        Now, assume that $a + b$ is positive for some natural $b$. Then we have
        $a + (b\pp) = (a + b)\pp$, but we know that $a + b$ is a positive
        natural number, hence $a + b \neq 0$, which (by Axiom \ref{th:3})
        implies that $(a + b)\pp \neq 0$, completing the proof.
\end{proof}

\begin{corollary}{}{}
       If $a$ and $b$ are natural numbers such that $a+b = 0$, then $a=0$ and
       $b=0$. 
\end{corollary}
\begin{proof}
        Striving for a contradiction, suppose that $a + b = 0$, but that, say,
        $a$ is positive. However, this immediately yields an absurdity, since we
        know that if $a$ is positive, and $b$ is any natural number, then $a + b
        \neq 0$ (Proposition 1.2.4). This completes the proof, since considering
        $b$ to be positive leads to exactly the same contradiction.
\end{proof}

\begin{lemma}{}{}
        Let $a$ be a positive number; then there exists \emph{exactly one}
        natural number $b$ such that $b\pp = a$.
\end{lemma}
\begin{proof}
        Left as an exercise.
\end{proof}

With the notion of addition, we can now define a notion of \emph{order}.
\begin{definition}{Ordering of the natural numbers}{}
        Let $n$ and $m$ be natural numbers. We say that $n$ \emph{is greater
        than or equal to} $m$ ($n \ge m$ or $m \le n$) iff we have $n = m + a$
        for some natural number $a$. We say that $n$ \emph{is strictly greater
        than} $m$ ($n > m$ or $m < n$) iff we have $n \ge m$ and $n \neq m$.
\end{definition}

Thus for instance $8 > 5$, because $8 \neq 5$ and $8 = 5 + 3$. On the other
hand, we have shown that $n\pp = n + 1$, so we always have $n\pp > n$. The
latter fact implies that theere is no such thing as a largest natural number
(!).

\begin{proposition}{Basic properties of order of the natural numbers}{}
        Let $a, b, c$ be natural numbers. Then 
        \begin{itemize}
                \item (Order is reflective) $a \ge a$.
                \item (Order is transitive) If $a \ge b$ and $b \ge c$, then $a
                        \ge c$.
                \item (Order is anti-symmetric) If $a \ge b$ and $b\ge a$, then
                        $a = b$.
                \item (Addition preserves order) $a \ge b$ if and only if $a + c
                        \ge b + c$.
                \item $a < b$ if and only if $a\pp \le b$.
                \item $a < b$ if and only if $b = a + d$ for some positive
                        number $d$.
        \end{itemize}
\end{proposition}
\begin{proof}
        Left as an exercise.
\end{proof}

\begin{proposition}{Trichotomy of order for the natural numbers}{}
        Let $a$ and $b$ be natural numbers. Then exactly one of the following
        statements is true: $a < b$, $a = b$, or $a > b$.
\end{proposition}
\begin{proof}
        This is only a sketch of the proof, to fill the gaps is left as an
        exercise. 

        Firstly, we will show that we cannot have more than one of the
        statements ($a>b$, $a = b$, and $a<b$) holding simultaneously. If $a <
        b$, then $a \neq b$ by definition, and similarly if $a>b$, then $a \neq
        b$. If $a < b$ and $a > b$, then $a = b$, by Proposition 1.2.5; this is
        a contradiction to the very definition of the ($>$) relation. Thus no
        more than one statement is true. 

        Now we show that at least one statement holds. We perform induction on
        $a$, keeping $b$ fixed. For the base case, we have $0 \le b$ for all
        natural $b$ (\textbf{why?}), so we have either $0 < b$ or $0 = b$. Now
        we must suppose inductively that either $a < b$, $a = b$, or $a > b$,
        for some natural number $a$. We must proceed by cases:

        If  $a > b$, then $a\pp > b$ (\textbf{why?}). And if $a = b$, then $a\pp
        > b$, (\textbf{why?}). Finally, if  $a < b$, then Proposition 1.2.5
        implies that $a\pp \le b$, thus either $a < b$ or $a = b$, and in either
        case one of the statements holds. This completes the proof.
\end{proof}

The properties of order allow us to obtain a stronger version of the principle
of mathematical induction:
\begin{proposition}{Strong principle of induction}{}
        Let $m_0$ be a natural number, and let $P(m)$ be a property pertaining
        to an arbitrary natural number $m$. Suppose that for each $m \ge m_0$ we
        have:
        \begin{center}
                If $P(m')$ is true for all natural numbers $m'$ satisfying $m_0
                \le m' < m$, then $P(m)$ is also true.
        \end{center}
        (This, in particular, means that $P(m_0)$ is true, for in that case the
        hypothesis would be vacuously true.) Then we can conclude that $P(m)$ is
        true for all natural numbers $m \ge m_0$.
\end{proposition}
\begin{proof}
        Left as an exercise.
\end{proof}

\begin{remark}{}{}
        In practice, we could use $m_0 = 0$ or $m_0 = 1$.
\end{remark}

\subsection{Exercises}
The following are straightforward statements.
\begin{exercise}
        Prove Proposition 1.2.2. (Hint: fix two of the variables and induct on
        the third.)
\end{exercise}
We wish to prove that addition of natural numbers is associative, that is, $(a +
b) + c = a + (b + c)$.
\begin{proof}
        We proceed by induction on $a$, with fixed $b$ and $c$. To begin with,
        notice that $(0 + b) + c = b + c$, by definition of addition; the same
        definition implies $0 + (b + c) = b + c$; thus, $(0 + b) + c = 0 +
        (b+c)$, as required. Now we suppose that $(a + b) + c = a +(b + c)$ for
        some natural number $a$. Then definition of addition yields $((a\pp) +
        b) + c = ((a + b)\pp) + c = ((a + b) + c)\pp$, but the induction
        hypothesis allows us to write $((a+b)+c)\pp = (a+(b+c))\pp = (a\pp) +
        (b+c)$. This closes the induction, completing the proof.
\end{proof}

\begin{exercise}
        Prove Lemma 1.2.3. (Hint: Use induction.)
\end{exercise}
Here we need to show that for all positive natural numbers $a$, there exists a
unique natural number $b$ such that $b\pp = a$. In other words: ``If $a$ is a
positive natural number, then there is a unique $b$ such that $b\pp = a$.''
\begin{proof}
        We prove the statement inductively on $a$. We should first acknowledge
        that 0 is not positive, hence the statement that we are set out to prove
        is vacuously true in the base case. Now we should suppose that, for some
        natural number $a$, we have that if $a$ is positive, then there is some
        other unique natural number $b$ such that $b\pp = a$. Then we must
        consider what happens if $a\pp$ is positive: it is obviously the
        successor of some natural number $a$; on the other hand, no other
        natural number can have the same successor, for that would contradict
        Axiom \ref{th:4}. This completes the proof.
\end{proof}

\begin{exercise}
        Prove proposition 1.2.5. (Hint: Use the preceding corollaries and
        lemmas.)
\end{exercise}
We shall prove six statements: 

\begin{itemize}
        \item First, order is reflective, $a \ge a$.
                \begin{proof}
                        It is obvious from Lemma 1.2.1 that $a = a + 0$, and
                        since 0 is a natural number, we have $a \ge a$, by
                        definition of order in the natural numbers. 
                \end{proof}
        \item Second, order is transitive.
                \begin{proof}
                        Suppose that $a \ge b$ and that $b \ge c$, then $a = b +
                        \alpha$ and $b = c +\beta$, for some natural numbers
                        $\alpha$ and $\beta$. This then implies that $a$ can be
                        written as $a = (c + \beta) + \alpha$, but we already
                        know that addition is associative, and that the sum of
                        two natural numbers is a natural number, hence $a = c +
                        (\beta + \alpha) = c + \gamma$, where we have defined
                        $\gamma := \beta + \alpha$ (a natural number). This is
                        to say that $a \ge c$, by definition of order.
                \end{proof}
        \item Third, order is anti-symmetric.
                \begin{proof}
                        Assume that $a \ge b$ and that $b \ge a$, this means
                        that $a = b + \alpha$ and that $b = a + \beta$, for some
                        natural $\alpha$ and $\beta$. These equalities imply (by
                        associativity) that $b = (b + \alpha) + \beta = b +
                        (\alpha + \beta)$, otherwise written as $b + 0 = b +
                        (\alpha + \beta)$; but then Cancellation Law tells us
                        that $\alpha + \beta = 0$, which by Corollary 1.2.1
                        means that $\alpha = \beta = 0$, thus leaving us with
                        $a = b + 0 = b$ and $b = a + 0 = a$. 
                \end{proof}
        \item Fourth, addition preserves order.
                \begin{proof}
                        Firstly, suppose that $a \ge b$, this is to say that $a
                        = b + \alpha$, for some natural number $\alpha$. Then,
                        if $c$ is a natural number, we have, by associativity
                        and commutativity, $a + c = (b + \alpha) + c = (b + c) +
                        \alpha$; and since addition is closed we have, by
                        definition, $a + c \ge b + c$, as required.

                        Conversely, suppose that $a + c \ge b + c$, this implies
                        by definition of order, associativity, and commutativity
                        that $a + c = (b + c) + \alpha = (b + \alpha) + c$; then
                        Cancellation Law allows us to write $a = b + \alpha$,
                        which means that $a \ge b$, thus completing the proof.
                \end{proof}
        \item Fifth, $a < b$ iff $a\pp \le b$.
                \begin{proof}
                        To begin with, assume that $a < b$, so $b = a + \alpha$,
                        for some natural $\alpha$, and $a \neq b$. Now, if
                        $\alpha = 0$, we would immediately conclude that $a =
                        b$, a contradiction, thus $\alpha$ is positive. By Lemma
                        1.2.3, this means that there is some natural number
                        $\beta$ (not necessarily positive) such that $\beta\pp =
                        \alpha$. Hence $b = a + (\beta\pp) = (a + \beta)\pp =
                        (a\pp) + \beta$, which of course means that $a\pp \le
                        b$, as required.

                        Conversely, suppose that $a\pp \le b$, this means that
                        $b = (a\pp) + \alpha$, for some natural $\alpha$. Notice
                        then that $a\pp = a + 1$, so that (using associativity)
                        $b = a + (1 + \alpha)$, which by closure of addition
                        means that $a \le b$. Now to show $a \neq b$, we proceed
                        by contradiction, if $a = b$, then we can use
                        Cancellation Law to show that $1 + \alpha = 0$, meaning
                        that $1 = 0$, a contradiction. This completes the proof.
                \end{proof}
        \item Sixth, $a < b$ iff $b = a + d$ for some positive natural number
                $d$.
                \begin{proof}
                        In the first part of the previous proof we showed by
                        contradiction that if $a < b$, then $a = b + d$, for
                        some positive $d$. Now we must show that the converse is
                        true. 

                        Suppose that $b = a + d$, with positive $d$. Then we can
                        immediately conclude that $a \le b$; to rule out the
                        equality we proceed by contradiction: If $a = b$, then
                        $d$ would necessarily have to be 0, by Cancellation Law,
                        but this contradicts the fact that $d$ is positive, thus
                        completing the proof.
                \end{proof}
\end{itemize}

\begin{exercise}
        Justify the three statements marked (\textbf{why?}) in the proof of
        Proposition 1.2.6.
\end{exercise}
We justify these statements as follows:
\begin{itemize}
        \item We have $0 \le b$ for all natural $b$.
                \begin{proof}
                        This is easy to show by induction on $b$. For the base
                        case, notice that $0 = 0 + 0$, and since 0 is a natural
                        number, we have $0 \le 0$, by definition. Now, suppose
                        inductively that $0 \le b$ for some natural $b$. Then we
                        have $b\pp \ge b$, which implies, by transitivity with
                        the inductive hypothesis, that $0 \le b\pp$. This closes
                        the induction. 
                \end{proof}
        \item If $a > b$, then $a\pp > b$.
                \begin{proof}
                        We have $a = b + \alpha$, for some positive $\alpha$; on
                        the other hand we already know that $a\pp = a + 1$, 1
                        being obviously positive. Thus $a\pp = b + (\alpha +
                        1)$, and since $\alpha + 1$ is positive, we have $a\pp >
                        b$; completing the proof.
                \end{proof} 
        \item If $a = b$, then $a\pp > b$.
                \begin{proof}
                        If $a = b$, then we simply have $a\pp = a + 1 = b +1$,
                        this obviously means that  $a\pp > b$, because 1 is
                        positive.
                \end{proof}
\end{itemize}

\begin{exercise}
        Prove Proposition 1.2.7. (Hint: Define $Q(n)$ to be the property that
        $P(m)$ is true for all $m_0 \le m < n$; note that $Q(n)$ is vacuously
        true when $n < m_0$.)
\end{exercise}
The statement reads: Let $m_0$ be a natural number, and let $P(m)$ be a property
pertaining to an arbitrary natural number $m$. Suppose that for each $m \ge m_0$
we have:
\begin{center}
        If $P(m')$ is true for all natural numbers $m'$ satisfying $m_0 \le m' <
        m$, then $P(m)$ is also true.
\end{center}
(This, in particular, means that $P(m_0)$ is true, for in that case the
hypothesis would be vacuously true.) Then we can conclude that $P(m)$ is true
for all natural numbers $m \ge m_0$.

\begin{proof}
        As suggested by the hint, we define $Q(n)$ to be the property that
        $P(m)$ is true for all $m_0 \le m < n$. The introduction of this new
        property allows us to restate the hypothesis taken in the proposition as
        follows:
        \begin{center}
                For $n \ge m_0$, if $Q(n)$ is true, then $P(n)$ is true. 
        \end{center}

        The proof may be performed by induction on $n$ using the property
        $Q(n)$, i.e., we will show that $Q(n)$ holds for every natural number
        $n$, and thus (by the above restatement) that $P(n)$ holds for all $n
        \ge m_0$.

        So as to evaluate the base case ($n=0$), we can write the statement
        $Q(0)$: $P(m)$ is true for all $m_0 \le m < 0$, or rather: if $m_0 \le m
        < 0$, then $P(m)$ is true; but this is vacuously true because, as we
        have already shown in the previous exercise, all natural numbers $m$
        satisfy $m \ge 0$. (This is to say that $Q(0)$ is true because no
        numbers satisfying the hypothesis of $Q(0)$ exist.) 

        Now we should suppose inductively that $Q(n)$ is true for some natural
        number $n$. That is, if $m_0 \le m < n$, then $P(m)$ is true. Then we
        must evaluate the validity of $Q(n\pp)$; therefore, we suppose that $m_0
        \le m < n\pp$. In this case we claim that $m_0 \le m \le n$, that is,
        either $m_0 \le m < n$ or $m = n$, why? Because if $m < n\pp$, then we
        know by Proposition 1.2.5 that $m\pp \le n\pp$, and for all we know this
        is equivalent to $m + 1 \le n + 1$, from which preservation of order
        under addition implies $m \le n$.
 
        That said, we may continue our proof by cases; we should notice that
        whenever $m_0 \le m < n$, we can conclude that $P(m)$ is true by the
        inductive hypothesis; hence we are only left with the case $m = n$
        (i.e., can we state that $P(m)$ holds for $m = n$? Otherwise: Can we
        prove that $P(n)$ is true?). 

        We shall immediately acknowledge that $m_0 \le m < n\pp$ implies by
        transitivity that $m_0 < n\pp$, and so by Proposition 1.2.5, $m_0\pp \le
        n\pp$, otherwise written as $m_0 + 1 \le n + 1$, or $m_0 \le n$. As we
        have assumed from the beginning: If  $n \ge m_0$ and $Q(n)$ is true (as
        it is because of the inductive hypothesis), then $P(n)$ is true. Thus we
        conclude $P(n)$ is true, which completes our proof that $Q(n\pp)$ holds,
        thus closing the induction.
\end{proof}

\begin{exercise}
        Let $n$ be a natural number, and let $P(m)$ be a property pertaining to
        the natural numbers such that whenever $P(m\pp)$ is true, then $P(m)$ is
        true. Suppose further that $P(n)$ is true. Prove that $P(m)$ is true for
        all natural numbers $m \le n$; this is known as the principle of
        backwards induction. (Hint: apply induction to the variable $n$.)
\end{exercise}
\begin{proof}
        We can rewrite the hypothesis in the following way: We define $Q(n)$ to
        be the statement: 
        \begin{center}
                If $P(n)$ is true, then $P(m)$ is true for all $m \le n$.
        \end{center}
        Then we can perform induction on $n$ with the statement $Q(n)$. For the
        base case, suppose $P(0)$ is true and that $m \le 0$. I claim that the
        latter condition is equivalent to saying $m = 0$, which is the case, of
        course, because no natural number is less than 0. Hence $P(m) = P(0)$,
        which we already know is true. 

        Now we may suppose that $Q(n)$ is true for some natural number $n$,
        i.e., if $P(n)$ is true, then $P(m)$ is true for all $m \le n$.
        Therewith we can tackle the question as to what happens with $Q(n\pp)$.
        If we assume thereafter that $P(n\pp)$ and that $m \le n\pp$, then we
        can claim that either $m \le n$ or $m = n\pp$: Indeed, we can proceed by
        cases because whenever $m \neq n\pp$ we can can only have $m < n\pp$ by
        definition(otherwise it would be a contradiction between the statement
        that $m \le n\pp$ and that $m \neq n\pp$); now Proposition 1.2.5 implies
        that $m\pp \le n\pp$, which then yields $m \le n$, as claimed. On the
        other hand, if $m = n\pp$, then Trichotomy tells us that is the only
        condition that can possibly hold.

        That said, we may now continue with the induction step by cases; to
        begin with notice that one of our hypotheses was that whenever $P(m\pp)$
        is true, so is $P(m)$, so we may acknowledge that if $P(n\pp)$ is true,
        we can then say $P(n)$ is true. Whenever $m \le n$, and since $P(n)$ is
        true, induction hypothesis allows us to state that $P(m)$ is true. On
        the other hand, if $m = n\pp$, we already have $P(n\pp)$, which is
        precisely what we want to show for the induction to be closed. This
        concludes the induction.

        Finally, we have the statement $Q(m)$ being true for all natural numbers
        $m$. In particular, if we choose the value $n$, and taking the general
        hypothesis that $P(n)$ is true, we have that $P(m)$ is true for all $m
        \le n$, as required.
\end{proof}
