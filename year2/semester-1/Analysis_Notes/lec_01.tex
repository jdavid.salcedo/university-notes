\chapter{The natural numbers}

\setcounter{minitocdepth}{3} % 1: chapter level; 2: section level
\minitoc
\vspace{\baselineskip}
Throughout these notes we shall get an acquaintance of various number systems
which will be of use in real analysis; in increasing order of ``complexity''
they are: the natural numbers $\mathbb{N}$, the integers $\mathbb{Z}$, the
rational numbers $\mathbb{Q}$, and the real numbers $\mathbb{R}$. In fact, the
construction of such systems is sequential in the sense that the natural numbers
are used to build the integers, which in turn are used to define the rationals,
and so forth. Thereby it can be seen that the natural numbers $\{0,1,2,\ldots\}$
comprise the most ``basic'' of the number systems, and thus must be firstly
turned to so as to provide a basis for the peculiarities inherent in the other
systems. 

The approach we will take here is \emph{axiomatic} because it will allow us to
define certain properties tying in with the, by now well known, algebra of the
natural number system via the establishment of a fixed number of assumptions
(axioms). Before proceeding, it should be warned that, in setting up the natural
numbers, we must avoid \emph{circularity} in our arguments at all costs, since
we want to have sound grounds on which to base the more sophisticated theory of
real analysis.

As a remark on what follows: we will make use of the familiar decimal number
system, even though it is not actually intrinsic to the axioms of natural
numbers (the Peano axioms). In fact, one could apply the same conclusions about
the natural numbers to any representation thereof which is \emph{isomorphic} to
the well known sequence of symbols: 0, 1, 2, 3, 4, \ldots

\lecture{1}{Fri 28 May 11:32}{The Peano axioms}
Keep in mind that the following definition of the natural numbers is by no means
unique, one can for instance get a practically equivalent definition for the
system through cardinalites (or, simply put, ``the number of elements'') of
finite sets. However, we will not bother trying to understand differing
definitions and will restrain ourselves to the standard axioms for now. 

To begin with, we take the following intuitive characterisation:
\begin{definition}{Informal}{}
        A \textbf{natural number} is any element of the set 
        \[
                \mathbb{N} = \{0,1,2,3,\ldots\},
        \] 
i.e., the set of all numbers created by counting forward from 0, indefinitely.
The set $\mathbb{N}$ is called the \textbf{set of natural numbers}.
\end{definition}
\begin{remark}{}{}
        Notice that we are including the number 0 in the set of real numbers.
        This is a mere matter of notation, though a quite non-standard one.
        Indeed, we will refer to the set $\{1,2,3,\ldots\}$ as the
        \textbf{positive integers}, $\mathbb{Z}^{+}$.
\end{remark}

This is, obviously, a perfectly reasonable definition, at least from a purely
intuitive viewpoint; and yet it lacks completeness, for it does not tell us what
a set is, nor does it explicitly describe what ``counting'' is all about: notice
that, thus far, we do not have rules for counting (!).

That said, our main concern will be to find a set of rules as to how the
counting operation on natural numbers should work, the \emph{addition} operation
will eventually arise from there. With that in mind, we will now regard two
fundamental concepts: the zero number (0), and the increment operation
($\mathrel{+}\mathrel{\mkern-3.5mu}\mathrel{+}$). We shall denote the
\emph{successor} of a number $n$ by $n\pp$. For instance, we understand that
$3\pp = 4$, or $100\pp = 101$; we just need to state these facts in axiomatic
terms.

By now it is almost evident that $\mathbb{N}$ is the set of all elements that
can be obtained by incrementing the number 0:
\[
        0, 0\pp, (0\pp)\pp, ((0\pp)\pp)\pp, \ldots; 
\]
this primitive idea can be constructed by dint of the following axioms:
\begin{axiom}{}{1}
        0 is a natural number.
\end{axiom}
\begin{axiom}{}{2}
        If $n$ is a natural number, then $n\pp$ is also a natural number.
\end{axiom}

These first axioms imply, for instance, that $(0\pp)\pp$ is a natural number,
for we know that 0 is a natural number, and so by Axiom \ref{th:1}, $0\pp$ is,
too, a natural number, which by Axiom \ref{th:2} implies that $(0\pp)\pp$ is a
natural number. We clearly see in this argument that our notation quickly turns
inconvenient, thus justifying the adoption of a more familiar symbolism:
\begin{definition}{}{}
        1 is the number $0\pp$, 2 is the number $(0\pp)\pp$, 3 is the number
        $((0\pp)\pp)\pp$, etc. In other words,
        \begin{align*}
                1 &:= 0\pp,\\
                2 &:= (0\pp)\pp,\\
                3 &:= ((0\pp)\pp)\pp,\\
                  & \vdots
        \end{align*}
\end{definition}

Now we can actually prove some things in regard to natural numbers, for example:
\begin{proposition}{}{}
        3 is a natural number.
\end{proposition}
\begin{proof}
        By Axiom \ref{th:1}, 0 is a natural number; then, by Axiom \ref{th:2},
        we have that $1 := 0\pp$ is also a natural number. Therefore, the same
        axiom implies $2 := (0\pp)\pp$ is a natural number. We can thereupon
        apply Axiom \ref{th:2} once again, this time on the number 2, implying
        that 3 is a natural number.
\end{proof}

We should acknowledge, however, that the aforementioned conditions do not
completely settle down our discussion of the natural number system, for there
are still some pathological behaviours that abide by these two rules, but do not
conform to what is regularly expected from the natural numbers.

Indeed, it is possible to have a set of which a ``0-th element'' makes part, and
for which the increment operation is \emph{closed}, but that ``wraps around
itself,'' so to speak; i.e., we could have an undesired behaviour of the
increment operation in such a way that it eventually leads back to an initial
number: for example, we could stipulate that $6\pp = 0$, or that $101\pp = 0$.
In order to prevent these issues, we impose a new axiom:
\begin{axiom}{}{3}
        0 is not the successor of any natural number; that is, we have $n\pp
        \neq 0$ for every natural number $n$.
\end{axiom}

This allows us to rule out the sort of pathological behaviour mentioned before:
\begin{proposition}{}{}
        $4 \neq 0$.
\end{proposition}
\begin{proof}
        By definition, we have $4 = 3\pp$ (4 is the successor of 3), but 3 is a
        natural number (Proposition 1.1.1), and Axiom \ref{th:3} tells us that 0
        cannot be the successor of any natural number. Hence 4 cannot be equal
        to 0. 
\end{proof}

This is naturally a significant step forward, but, it turns out, it is just not
good enough for our purposes. Tao offers a quite enlightening example of a
strange way in which our newly defined numerical system can behave: Consider a
number system in which there are only five numbers (0, 1, 2, 3, 4), and in which
the increment operation hits a ``maximum value'' at 4. On these grounds, we have
that $0\pp = 1, 1\pp = 2, 3\pp = 4$, but $4\pp = 4$. This could be seen
otherwise as 
\[
4 = 5 = 6 = \cdots,
\] 
this is quite inconvenient of course, and yet it does not contradict our first
three axioms in any manner: we have closure of the increment operation, we have
defined our 0-th element, and neither of the elements of our ``set'' has 0 as
its successor. By virtue thereof, we may introduce a new axiom:
\begin{axiom}{}{4}
        Different natural numbers have different successors; in other words, if
        $n$ and $m$ are two distinct natural numbers ($n \neq m$), then $n\pp
        \neq m\pp$.

        This statement is equivalent to its contrapositive: if $n\pp = m\pp$,
        then $n = m$.
\end{axiom}

This yields new results about the natural numbers, for instance:
\begin{proposition}{}{}
        $6 \neq 2$.
\end{proposition}
\begin{proof}
        Striving for a contradiction, suppose that $6 = 2$. By definition we
        have $6 = 5\pp$, and $2 = 1\pp$; now by our assumption, $5\pp = 1\pp$.
        Axiom \ref{th:4} implies that 5 = 1, which is the same as $4\pp = 0\pp$.
        Applying Axiom \ref{th:4} once again we have $4 = 0$, which is absurd by
        Proposition 1.1.2.
\end{proof}

The next step in our construction is perhaps a little more subtle: We now have
to deal with the possibility of there being ``rogue'' numbers in $\mathbb{N}$;
why? Well, while the four axioms described above allow for a confirmation, and
distinction, of the numbers $1, 2, 3, 4, \ldots$ as being part of the natural
numbers, we cannot yet rule out the appearance of certain elements which are not
in the form of so-called ``whole'' numbers. We may consider the following
(informal) example, to see what is meant by this:

Suppose that $\mathbb{N}$ consisted of the following collection of
integers and half-integers:
        \[
                \mathbb{N} := \{0,0.5,1,1.5,2,2.5,3,3.5,\ldots\};
        \] 
it is obvious now that, even though an element such as 0.5 cannot be obtained by
incrementing any other natural number, this is not actually contradicting any of
the four axioms thus far stated: In fact, we could have defined a set
$\mathbb{N}$ with as many ``intruders'' as we like, while maintaining the
consistency of the axioms above (!). In other words, the first four axioms
perfectly settle down the behaviour of the so-called natural numbers, but are
unable to pin down the problem of classifying them into a \emph{unique} set.
Now, this is a real problem, but fortunately we \emph{can} introduce a new rule
so as to avoid such things. 

Said rule would effectively say that the only numbers which can comprise
$\mathbb{N}$ are those that can be obtained from 0 and the increment operation,
this would exclude possible outsiders from coming into our set $\mathbb{N}$. The
axiom we are seeking tackles this problem in a rather ingenious manner:
\begin{axiom}{Principle of mathematical induction}{5}
                Let $P(n)$ be any property pertaining to a natural number $n$.
                Suppose that $P(0)$ is true, and suppose that whenever $P(n)$ is
                true, $P(n\pp)$ is also true. Then $P(n)$ is true for
                \emph{every} natural number $n$.
\end{axiom}

\begin{remark}{}{}
        We may refer to as property to any statement about a natural number
        which can be assigned a truth value (either true or false). Some
        examples are: ``$n$ is even'', ``$n$ solves the equation $(n+1)^2 = n^2
        + 2n + 1$'', etc. These concepts have not yet been properly defined,
        but, frankly, we already have a feel for them. 

        Notice how the nature of this axiom is quite different from that of the
        first ones; in fact, since it can be used to produce (infinitely) many
        axioms, logicians refer to it as an \emph{axiom schema}. (That is to say
        that each and every general property about the natural numbers that can
        be derived through this axiom scheme is essentially another axiom). 
\end{remark}

There is a nice intuition behind the principle of mathematical induction, it
makes it almost obvious to see why the axiom should be true and can certainly
help in building up a more inspired insight into it: Suppose that $P(n)$ is such
that $P(0)$ holds true, and such that whenever $P(n)$ is true, then $P(n\pp)$ is
true. Since $P(0)$ is true, then $P(0\pp) = P(1)$ is true, and so given $P(1)$
is true, then $P(1\pp) = P(2)$ is also true, this process may repeat itself
indefinitely, thus allowing us to see that $P(0)$, $P(1)$, $P(2)$, $P(3)$, etc.
are all true. This train of thought cannot possibly let us conclude, for
instance, anything about $P(0.5)$, because 0.5 cannot be obtained by
incrementing some natural number; as a consequence thereof, a set such as the
one defined above (with half integers) does not obey the principle of induction.
Thus, it can be seen that Axiom \ref{th:5} should not hold for number systems
which have ``intruders'' in them.

This principle should not be completely novel, and we already know how to prove
things via mathematical induction: An archetypical model for this kind of proofs
is as follows:

\begin{proposition}{}{}
        A certain property $P(n)$ is true for every natural number $n$.
\end{proposition}
\begin{proof}
        We proceed by induction. Firstly, we verify the base case, $P(0)$, that
        is. \emph{(Proof of $P(0)$)}. Now, suppose that $P(n)$ is true for some
        natural number $n$ --note that we have, by now, at least one natural
        number satisfying the property. We now prove $P(n\pp)$. \emph{(Proof of
        $P(n\pp)$, assuming $P(n)$ is true)}. This completes the proof.
\end{proof}

Axioms \ref{th:1}, \ref{th:2}, \ref{th:3}, \ref{th:4}, \ref{th:5} are called the
Peano axioms for the natural numbers. They are perfectly plausible statements
that act as the basis for further development of our theory. That said, we need
to make an additional assumption:
\begin{assumption}{}{}
        There exists a number system $\mathbb N$, whose elements we will call
        the natural numbers, for which the Peano axioms are true.
\end{assumption}
This assumption shall not be proven, though it will be included in our axioms
about set theory. From these very simple 5 statements, and some sets, we will be
able to construct all of the algebra and calculus we are used to dealing with.

As an additional remark, notice that these axioms should hold, however different
a set of symbols used to describe the natural numbers is. In fact, any such set
would be equivalent to the one we are the most familiar with, because one can
establish a one-one correspondence between the set $\{0,1,2,3,5,\ldots\}$ and,
say, $\{$O, I, II, III, IV, $\ldots\}$. This type of equivalence can be better
described with some basic knowledge of set theory.

One consequence of the axioms is that we can now build sequences
\emph{recursively}. Indeed, suppose we want to build a sequence of numbers $a_0,
a_1, a_2, a_3, \ldots$, by first defining some base value $a_0$, that is, by
letting $a_0 := c$, for some number $c$; and by then letting $a_1$ be some
function of $a_0$, $a_1 = f_0(a_0)$, $a_2$ be some function of $a_1$, $a_2 :=
f_1(a_1)$, and so on. In general, we can set $a_{n\pp}$ to be some function of
$a_n$, $a_{n\pp} := f_n(a_n)$, for some function $f_n : \mathbb N \to \mathbb
N$. All the axioms together allow for the conclusion that a single value will be
assigned to each element $a_n$ of the sequence, with $n$ being a natural number.
(Notice that this discussion and the proposition bellow are still informal, for
we have not yet defined what a function is, however, it will be left as an
exercise to formalise the argument after set theory has been handled.)
\begin{proposition}{Recursive definitions}{}
        Suppose that for each natural number $n$ we have some function $f_n :
        \mathbb N \to \mathbb N$. Let $c$ be a natural number. Then we can
        assign a \emph{unique} natural number $a_n$ to \emph{each} natural
        number $n$, such that $a_0 = c$ and $a_{n\pp} = f_n(a_n)$ for each
        natural number $n$.
\end{proposition}
\begin{proof}
        (Informal) We may use mathematical induction (Axiom \ref{th:5}). It is
        evident that we can assign a single value to $a_0$, namely $c$. None of
        the other definitions $a_{n\pp} = f_n(a_n)$ can possibly redefine $a_0$,
        for the natural numbers do not ``wrap around'' themselves ---which is to
        say that no other $a_{n\pp}$ can be equal (in the sense of being the
        same entity, not in the sense of having the same value, which is
        actually no problem at all) to $a_0$ (Axiom \ref{th:3}). Now, suppose
        inductively that $a_n$ can be assigned a unique value through this
        procedure. Then we can give $a_{n\pp}$ a single value, $f_n(a_n)$ (which
        is unique by how we understand functions). Once again, there is no room
        for redefinitions of $a_{n\pp}$ with other values $a_{m\pp}$, since
        different natural numbers must have different successors (the successor
        of $n$ is unique, Axiom \ref{th:4}). This completes the proof.
\end{proof}

We should acknowledge that all of the axioms had to be used here, even those
which were not explicitly cited, for we need 0 to be part of $\mathbb N$ (Axiom
\ref{th:1}) and the increment operation to be closed (Axiom \ref{th:2}) in order
for the induction to follow. Basically, recursive definitions work because we
are not allowed to arbitrarily redefine a certain number $a_n$ in the sequence;
that is the most important thing to take out from here. 
\begin{remark}{}{}
        We have discussed this last topic in a rather intuitive fashion; that
        notwithstanding, there will be no circularity in the argument when we
        later introduce functions because recursive functions and sets do not
        require recursive definitions to be properly established from a
        theoretical viewpoint.
\end{remark}
