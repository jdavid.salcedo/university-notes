\lecture{7}{Tue 13 Jul 19:23}{Cartesian products}

In addition to the basic operations such as union, intersection, and
differencing, another fundamental operation on sets is that of \emph{Cartesian
Product}. So as to define this new operation, it is necessary to introduce the
concept of ordered pairs:

\begin{definition}{Ordered pair}{}
        If $x$ and $y$ are any objects (possibly equal), we define the
        \textbf{ordered pair} $(x,y)$ to be a new object, consisting of $x$ as
        its first component and $y$ as its second component. Two ordered pairs
        $(x,y)$ and $(x',y')$ are considered equal if and only if both their
        components match, i.e.,
        \[
                (x,y) = (x',y') \iff (x = x' \text{ and } y = y').
        \] 
\end{definition}

This definition obeys the usual axioms of equality (left as an exercise). Notice
that the order in which components are listed does matter, hence the ordered
pair $(1,2)$ is equal to $(0+1,1+1)$, but is distinct from the pairs $(2,1)$,
$(2,2)$, and $(1,1)$.

\begin{remark}{}{}
        Strictly speaking, this definition is actually another axiom of set
        theory; we are simply postulating that given any two objects $x$ and
        $y$, there exists a new object $(x,y)$. However, it is possible to
        define an ordered pair in such a way that we do not need to acknowledge
        any further axioms.
\end{remark}

With this definition in mind, we proceed with the definition of Cartesian
products.

\begin{definition}{Cartesian product}{}
        If $X$ and $Y$ are any two sets, then we define the \textbf{Cartesian
        product} $X \times Y$ to be the collection of ordered pairs whose first
        component lies in $X$ and second component lies in $Y$. Thus
        \[
                X \times Y = \{(x,y) : x \in X, y \in Y\},
        \] 
        or equivalently
        \[
                a \in (X \times Y) \iff (a = (x,y) \text{ for some } x \in X \text{ and some } y \in Y).
        \] 
\end{definition}

As a remark, we shall simply assume that our notion of ordered pair is such that
whenever $X$ and $Y$ are set, the Cartesian product $X \times Y$ is also a set
(a proof of this fact will be left as an exercise).

For example, if $X := \{1,2\}$ and $Y := \{3,4,5\}$, then
\[
        X \times Y = \{(1,3), (1,4), (1,5), (2,3), (2,4), (2,5)\}
\]
and
\[
        Y \times X = \{(3,1), (4,1), (5,1), (3,2), (4,2), (5,2)\}.
\]
Thus, in general, $X \times Y$ and $Y \times X$ are different sets, although
they ere very similar ones. For instance, it can be proven that they always have
the same number of elements (see \emph{cardinality}).

Let $f : X \times Y \to Z$ be a function whose domain $X \times Y$ is a
Cartesian product. Then $f$ can either be thought of as a function of one
variable, mapping the single input of an ordered pair $(x,y)$ in $X \times Y$ to
an output $f(x,y)$, or as a function of two variables, mapping an input $x \in
X$ and an input $y \in Y$ to a single output $f(x,y)$. While these notions are
strictly different, we will not bother to distinguish the two of them. Thus for
instance the addition operation $+$ on the natural numbers can now be
re-interpreted as a function $+ : \mathbb{N} \times \mathbb{N} \to \mathbb{N}$,
defined by $(x,y) \mapsto x + y$.

The concept of ordered pairs can be easily generalised to ordered triples,
quadruples, and so forth:

\begin{definition}{\boldmath Ordered $n$-tuple and $n$-fold Cartesian product}{}
        Let $n$ be a natural number. An ordered $n$-tuple $(x_i)_{1\le i\le n}$
        (also denoted $(x_1,\ldots,x_n)$) is a collection of objects $x_i$, one
        for every natural number $i$ between $1$ and $n$; we refer to $x_i$ as
        the $i$-th component of the ordered $n$-tuple. Two ordered $n$-tuples
        $(x_i)_{1\le i\le n}$ and $(y_i)_{1\le i\le n}$ are said to be equal iff
        $x_i = y_i$ for all $i$ satisfying $1\le i\le n$.

        If $(X_i)_{1\le i\le n}$ is an ordered $n$-tuple of sets, we define
        their Cartesian product $\prod_{1\le i\le n} X_i$ (also denoted
        $\prod_{i=1}^{n} X_i$ or $X_1 \times \cdots \times X_n$ ) by
        \[
                \prod_{1\le i\le n} X_i = \{(x_i)_{1\le i\le n} : x_i \in X_i \text{ for all } 1\le i\le n\}.
        \] 
        Again, this definition simply postulates the existence of an ordered
        $n$-tuple and a Cartesian product, whenever needed. However, it is
        possible to explicitly construct these objects using the axioms of set
        theory (left as an exercise).
\end{definition}

\begin{remark}{}{}
        One can show that $\prod_{1\le i\le n} X_i$ is a set. Indeed, from the
        power set axiom, we can consider the set of all functions $i \mapsto
        x_i$ from the domain $ \{i \in \mathbb{N} : 1\le i\le n\}$ to the range
        $\bigcup_{1\le i\le n} X_i$, and then we can use the axiom of
        specification to restrict to those functions $i \mapsto x_i$ for which
        $x_i \in X_i$ for all $1\le i\le n$. One can generalise this
        construction to infinite Cartesian products (which shall be introduced
        later on).
\end{remark}

Let $a_1, b_1, a_2, b_2, a_3, b_3$ be objects, and let $X_1 := \{a_1,b_1\}$,
$X_2 := \{a_2,b_2\}$, and $X_3 := \{a_3, b_3\}$. Then we have
\begin{align*}
        \MoveEqLeft[3] X_1 \times X_2 \times X_3 \\
        ={}& \{(a_1, a_2, a_3), (a_1, a_2, b_3), (a_1, b_2, a_3), (a_1, b_2, b_3),\\
        ={}& (b_1, a_2, a_3), (b_1, a_2, b_3), (b_1, b_2, a_3), (b_1, b_2, b_3)\},
\end{align*}
but
\begin{align*}
        \MoveEqLeft[3] (X_1 \times X_2) \times X_3 \\
        ={}& \{(a_1,a_2), (a_1, b_2), (b_1,a_2), (b_1,b_2)\} \times \{a_3,b_3\} \\
        \begin{split}
                ={}& \{((a_1,a_2),a_3), ((a_1, a_2),b_3), ((a_1,b_2),a_3), ((a_1,b_2),b_3),\\
                   & ((b_1,a_2),a_3), ((b_1,a_2),b_3), ((b_1,b_2),a_3), ((b_1,b_2),b_3)\},
        \end{split}
\end{align*}
and
\begin{align*}
        \MoveEqLeft[3] X_1 \times (X_2 \times X_3) \\
        ={}& \{a_1,b_1\} \times \{(a_2,a_3), (a_2, b_3), (b_2,a_3), (b_2,b_3)\} \\
        \begin{split}
                ={}& \{(a_1,(a_2,a_3)), (a_1,(a_2,b_3)), (a_1,(b_2,a_3)), (a_1,(b_2,b_3)),\\
                   & (b_1,(a_2,a_3)), (b_1,(a_2,b_3)), (b_1,(b_2,a_3)), (b_1,(b_2,b_3))\}.
        \end{split}
\end{align*}
Thus, strictly speaking, the sets $X_1 \times X_2 \times X_3$, $(X_1 \times X_2)
\times X_3$, and $X_1 \times (X_2 \times X_3)$ are all distinct. However, they
are clearly very similar (there are, for instance, obvious bijections between
any two of these sets), and it is common in practice to neglect the minor
distinctions between these sets and pretend they are in fact equal. Thus a
function $f : X_1 \times X_2 \times X_3 \to Y$ can be thought of as a function
of one variable $(x_1,x_2,x_3) \in X_1 \times X_2 \times X_3$, or as a function
of three variables, $x_1 \in X_1$, $x_2 \in X_2$, and $x_3 \in X_3$, or as a
function of two variables, $x_1 \in X_1$ and $(x_2,x_3) \in X_2 \times X_3$, and
so forth. We will not bother with the distinction between these perspectives.

An ordered $n$-tuple $(x_1,\ldots,x_n)$ of objects is also called an ordered
sequence of $n$ elements, or a \emph{finite sequence}, for short. In further
chapters we shall introduce the notion of an \emph{infinite sequence}.

If $x$ is an object, then $(x)$ is a 1-tuple, which we will identify with $x$
itself (even though they are, strictly speaking, distinct objects). Hence if
$X_1$ is any set, then the Cartesian product $\prod_{1\le i\le 1} X_i$ is just
$X_1$, because it would be defined, for the sequence $(X_1)$, as the set
$\{(x_i)_{1\le i\le 1} : x_i \in X_i \text{ for all } 1 \le i\le 1\} = \{(x_1) :
x_1 \in X_1\} $; i.e., the set of all 1-tuples containing the objects of $X_1$;
since we regard $(x) = x$, the equality follows. Also, the \emph{empty Cartesian
product} $\prod_{1\le i\le 0} X_i$ gives, not the empty set, but rather the
singleton set $\{()\}$, containing the 0-\emph{tuple} (also known as the
\emph{empty tuple}) $()$.

If $n$ is a natural number, we often write $X^{n}$ as a shorthand for the
$n$-fold Cartesian product $X^{n} := \prod_{1\le i\le n} X$. Thus $X^{1}$ is
essentially the same set as $X$ (if we ignore the distinction between $x$ and
the 1-tuple  $(x)$ ), whereas $X^2$ is the Cartesian product $X \times X$. The
set $X^{0}$ is a singleton set $\{()\}$, since it is just the empty Cartesian
product.

We can now generalise the single choice lemma (which we proved in the first
section of this chapter) to allow for multiple, yet finite, choices.

\begin{lemma}{Finite choice}{}
        Let $n \ge 1$ be a natural number, and for each natural number $i$
        satisfying $1\le i\le n$, let $X_i$ be a nonempty set. Then there exists
        an $n$-tuple $(x_i)_{1\le i\le n}$ such that $x_i \in X_i$ for all $1\le
        i\le n$. In other words, if each $X_i$ is nonempty, then the set
        $\prod_{1\le i\le n} X_i$ is also nonempty.
\end{lemma}
\begin{proof}
        We proceed by induction on $n$ (starting with the base case $n = 1$; the
        claim is vacuously true for $n = 0$, because there are no natural
        numbers $i$ satisfying $1 \le i \le 0$, but is not particularly
        interesting in that case). When $n = 1$, we know by the single choice
        axiom that there exists some $x_1$ such that $x_1 \in X_1$. Therefore,
        the 1-tuple $(x_1)$ satisfies the claimed conditions. Now, suppose
        inductively that the statement holds for some natural number $n$; we
        shall prove it also holds for $n\pp$. 

        Let $X_1, \ldots, X_{n\pp}$ be a collection of nonempty sets, as
        proposed in the hypotheses. We know for certain that we can find an
        $n$-tuple $(x_i)_{1\le i\le n}$ such that $x_i \in X_i$ for all $1\le
        i\le n$. Furthermore, since $X_{n\pp}$ is nonempty, we may find some
        object $a \in X_{n\pp}$. Hence we can define an $n\pp$-tuple
        $(y_i)_{1\le i\le n\pp}$ by setting $y_i = x_i$ whenever $1\le i\le n$,
        and $x_{n\pp} = a$. It is clear that $y_i \in X_i$ for all $1\le i\le n$
        by induction hypothesis, and also $y_i \in X_i$ for $i = n\pp$, because
        of how we defined $y_{n\pp}$, thus closing the induction.
\end{proof}

As a final remark, it should be noted that the above lemma may be extended to
allow for an infinite number of choices, but this requires the additional
postulate of the \emph{axiom of choice}.

\subsection{Exercises}

\begin{exercise}
        Suppose we define the ordered pair $(x,y)$ for any objects $x$ and $y$
        by the formula $(x,y) := \{\{x\},\{x,y\}\}$ (thus using several
        applications of the singleton and pair sets axiom). Show that such a
        definition indeed obeys the property of ordered pairs, and also that
        whenever $X$ and $Y$ are sets the Cartesian product $X \times Y$ is also
        a set. Thus this definition can be validly used as a definition of
        ordered pairs. 

        \textbf{Challenge.} Show that the alternate definition $(x,y) :=
        \{x,\{x,y\}\}$ also verifies the definition of an ordered pair and is
        thus acceptable. (For this latter task one needs the axiom of
        regularity, see the section on Russell's paradox.)
\end{exercise}

We shall prove the first part of the exercise, the challenge is taken care of
later on. To begin with, it must be shown that, using $(x,y) :=
\{\{x\},\{x,y\}\}$, we can claim
\[
        (x,y) = \left( x',y' \right) \iff (x = x' \text{ and } y = y').
\]
The proof is laid out here:
\begin{proof}
        Let us first regard two ordered pairs satisfying $(x,y) = \left( x',y'
        \right)$, then by definition of set equality we know that $a \in (x,y)
        \iff a \in \left( x',y' \right)$. Because of how we have defined these
        ordered pairs, we know that $a \in (x,y)$ means that $a = \{x\}$ or $a =
        \{x,y\}$; similarly, $a \in \left( x',y' \right)$ is equivalent to
        saying $a = \{x'\}$ or $a = \{x',y'\}$. Thus the following statement
        stems from our hypotheses:
        \[
        a = \{x\} \text{ or } a = \{x,y\} \iff a = \{x'\} \text{ or } a = \{x',y'\} .
        \] 
        Equivalently, we have 
        \begin{gather*}
        a = \{x\} \text{ or } a = \{x,y\} \implies a = \{x'\} \text{ or } a = \{x',y'\}, \\
        a = \{x\} \text{ or } a = \{x,y\} \impliedby a = \{x'\} \text{ or } a = \{x',y'\}.
\end{gather*} 
Hence the fact that if $a = \{x\}$, then either $a = \{x'\}$ or $a = \{x',y'\}$,
and if $a = \{x,y\}$, then either $a = \{x'\}$ or $a = \{x',y'\}$. Now, we have
two cases: 

First, suppose $x \neq y$, then whenever $a = \{x,y\}$, we know for sure that
$a$ cannot be equal to $\{x'\}$, because that would imply $x = x' = y$ (a
contradiction); thus in this case we necessarily have $a = \{x',y'\}$, so
$\{x,y\} = \{x',y'\}$. Similarly, whenever $a = \{x\}$, we can conclude $\{x\} =
\{x'\}$. Since we must guarantee that the sets at issue are equal for any
condition on $a$, we must simultaneously have $x = x'$ and $\{x,y\} =
\{x',y'\}$, which in turn implies that $\{x,y\} = \{x,y'\}$, i.e., $y = y'$.  

Now suppose $x = y$. In this case we must clearly have $a = \{x\}$, which in
turn means that either $a = \{x'\}$ or $a = \{x',y'\}$. In the first case $x =
x'$, and so $x = y = x'$, in the second case $x = x' = y'$, and since $x = y$,
we have $x = x' = y = y'$. For the equality to follow in both cases, we require
that $x = x' = y = y'$, as desired.

On the other hand, whenever $x = x'$, and $y = y'$, we obviously have
$\{\{x\},\{x,y\}\} = \{\{x'\},\{x',y'\}\}$, completing the proof.
\end{proof}

Now we turn to the definition of Cartesian products. That is, we need to show
that our definition of ordered pairs justifies the existence of the set $X
\times Y$, whenever $X$ and $Y$ are sets.

\begin{proof}
        Recall that $X \times Y := \{(x,y) : x \in X \text{ and } y \in Y\}$. By
        our definition this readily implies that $X \times Y = \{
        \{\{x\},\{x,y\}\} : x \in X \text{ and } y \in Y\}$. Clearly, $\{x\},
        \{x,y\}$ are elements of the power set $2^{X \cup Y}$ (they are both
        possible subsets of $X \cup Y$). Hence the set $\{\{x\}, \{x,y\}\}$ is a
        subset of the set $2^{X \cup Y}$, which is to say it belongs in the set
        $2^{2^{X \cup Y}}$, which we know exists by previous theorems. Thus we
        can simply claim 
        \[
        X \times Y = \{a \in 2^{2^{X \cup Y}} : a = \{\{x\},\{x,y\}\} \text{ for some $x \in X$ and some $y \in Y$}\}, 
        \] 
        which is justified by the axiom of specification. It is very easy to
        show that this set equality holds using the above definitions of ordered
        pair.
\end{proof}

\begin{exercise}
        Suppose we define an ordered $n$-tuple to be a surjective function $x :
        \{i \in \mathbb{N} : 1 \le i\le n\} \to X$, whose range is some
        arbitrary set $X$ (so different ordered $n$-tuples are allowed to have
        different ranges); we then write $x_i$ for $x(i)$ and also write $x$ as
        $(x_i)_{1\le i\le n}$. Using this definition, verify that we have
        $(x_i)_{1\le i\le n} = (y_i)_{1\le i\le n}$ iff $x_i = y_i$ for all
        $1\le i\le n$. Also, show that if $(X_i)_{1\le i\le n}$ is an ordered
        $n$-tuple of sets, then the Cartesian product is indeed a set.
\end{exercise}

To begin with, we will take care of the first claim: i.e., we show that the
mapping described above indeed satisfies the main property of ordered
$n$-tuples. So as to ease notation, I shall write $x := (x_i)_{1\le i\le n}$ and
$y := (y_i)_{1\le i\le n}$.
\begin{proof}
        Firstly, suppose that we have two $n$-tuples $x,y : \{1\le i\le n\} \to
        A$ satisfying $x = y$. Then, by definition of function equality, we must
        have $x_i = y_i$ for all $i$ such that $1\le i\le n$. (Obviously, these
        functions must agree on their domain and range.) 
        
        Matter-of-factly, it is interesting to note that we cannot weaken the
        hypothesis by just supposing $x$ and $y$ have the same domain and range,
        i.e., $x,y : \{1\le i\le n\} \to A$. Of course, we could pick an
        arbitrary $i$, with $1\le i\le n$, knowing that if $a = x_i$, then $a
        \in A$; and since both $x$ and $y$ are surjective mappings, we would
        know that for this particular $a$ we could write $a = y_j$ with some
        $1\le j\le n$, therefore $x_i = y_j$ for an arbitrary $i$ and some $j$.
        This is \emph{not} what we set out to prove because we \emph{cannot}
        assure that $i = j$.

        Conversely, suppose that we have two mappings, $x : \{1\le i\le n\} \to
        X$, $y : \{1\le i\le n\} \to Y$, such that $x_i = y_i$ for all $1\le
        i\le n$. We must ensure that both functions have the exact same range,
        for we already know by definition that they have the same domain. Since
        both mappings are surjective, we can pick an object $a \in X$ and be
        certain that $a = x_i$ for some $1\le i\le n$. Hence we see that $a =
        x_i = y_i$, which is to say (since $y_i \in Y$) that $a \in  Y$ as well.
        Similarly, we can see that if $a \in Y$, then $a \in X$, thus implying
        $X = Y$. Therefore both functions agree on their domain and range, and
        so are equal, as desired.
\end{proof}

Now we turn to the second portion of the statement. We show that if $(X_i)_{1\le
i\le n}$ is an ordered $n$-tuple of sets, then the Cartesian product, as defined
before, is indeed a set.
\begin{proof}
        Recall that we define the Cartesian product of $(X_i)_{1\le i\le n}$ as
        \[
                \prod_{1\le i\le n} X_i = \{(x_i)_{1\le i\le n} : x_i \in X_i \text{ for all } 1\le i\le n\}.
        \] 
        We know that each $(x_i)_{1\le i\le n}$ above is a mapping from $ I =
        \{1\le i\le n\}$ to some subset $X'$ of $X$, where $X = \bigcup_{1\le
        \alpha\le n} X_\alpha$, because if $x \in X'$, then we certainly have
        (by surjectivity) $x = x_i$ for some $1\le i\le n$, which is to say $x
        \in X$ by definition of union. Also, there exists a set containing all
        partial functions from $I$ to $X$ (we proved this before). Let $\Omega$
        be such a set of partial functions, we can apply the axiom of
        specification to construct the set 
        \[
                \mathcal{C} := \{\varphi \in \Omega : \varphi \text{ is onto, } \varphi(i) \in X_i \text{ for all } 1\le i\le n\}.
        \] 
        Hence we have proven the existence of $\mathcal{C}$. I claim that this
        set is exactly the same as the Cartesian product $\Pi$ of the
        hypothesised sequence of sets, which is easy to see: First, if $x \in
        \Pi$, we have $x : I \to X' \subseteq X$ is onto, and $x(i) \in X_i$ for
        all $i \in I$, which is exactly the condition for $x \in \mathcal{C}$;
        conversely, if $x \in \mathcal{C}$, then $x : I \to X' \subseteq X$ is
        an onto mapping with $x(i) \in X_i$ for all $i \in I$, which means $x$
        is in fact an ordered $n$-tuple belonging in $\Pi$. This completes the
        proof.
\end{proof}

\begin{exercise}
        Show that the definitions of equality for ordered pairs and ordered
        $n$-tuples obey the reflexivity, symmetry, and transitivity axioms.
\end{exercise}
\begin{itemize}
        \item (Reflexivity for ordered pairs) $(x,y) = (x,y)$.
                \begin{proof}
                        Obviously, $x = x$ and $y = y$. Now, by definition of
                        ordered pair equality, we have $(x,y) = (x,y)$.
                \end{proof}
        \item (Reflexivity for $n$-tuples) $(x_i)_{1\le i\le n} = (x_i)_{1\le
                i\le n}$.
                \begin{proof}
                        Let $1\le i\le n$. Then we know that $x_i = x_i$;
                        therefore, for any arbitrary $i$, $x_i = x_i$, which
                        implies by definition that $(x_i)_{1\le i\le n} =
                        (x_i)_{1\le i\le n}$, as desired.
                \end{proof}
        \item (Symmetry for ordered pairs) If $(a,b) = (c,d)$, then $(c,d) =
                (a,b)$.
                \begin{proof}
                        Since $(a,b) = (c,d)$, we have $a = c$ and $b = d$;
                        hence, by symmetry of equality, $c = a$ and $d = b$.
                        Therefore, by definition of ordered pair equality,
                        $(c,d) = (a,b)$, as desired.
                \end{proof}
        \item (Symmetry for $n$-tuples) If $(x_i)_{1\le i\le n} = (y_i)_{1\le
                i\le n}$, then $(y_i)_{1\le i\le n} = (x_i)_{1\le i\le n}$.
                \begin{proof}
                        Because $(x_i)_{1\le i\le n} = (y_i)_{1\le i\le n}$, we
                        know that $x_i = y_i$ for all $1\le i\le n$. Whence $y_i
                        = x_i$ for all $1\le i\le n$, so that $(y_i)_{1\le i\le
                        n} = (x_i)_{1\le i\le n}$.
                \end{proof}
        \item (Transitivity for ordered pairs) If $(x,y) = (x',y')$ and $(x',y')
                = (x'', y'')$, then  $(x,y) = (x'',y'')$.
                \begin{proof}
                        Considering all the hypotheses, we have $x = x'$, $y =
                        y'$ and $x' = x''$, $y' = y''$. Therefore, $x = x''$ and
                        $y = y''$, which means $(x',y') = (x'',y'')$, as
                        claimed.
                \end{proof}
        \item (Transitivity for $n$-tuples) If $(x_i)_{1\le i\le n} =
                (y_i)_{1\le i\le n}$ and $(y_i)_{1\le i\le n} = (z_i)_{1\le i\le
                n}$, then $(x_i)_{1\le i\le n} = (z_i)_{1\le i\le n}$.
                \begin{proof}
                        Considering the assumptions, we have $x_i = y_i$ and
                        $y_i = z_i$ for all $1\le i\le n$. Thus for any specific
                        $1\le j\le n$, we have $x_j = z_j$ (by transitivity).
                        Therefore $(x_i)_{1\le i\le n} = (z_i)_{1\le i\le n}$,
                        as we had claimed.
                \end{proof}
\end{itemize}

\begin{exercise}
        Let $A,B,C$ be sets. Show that $A \times (B \cup C) = (A \times B) \cup
        (A \times C)$, that $A \times (B \cap C) = (A \times B) \cap (A \times
        C)$, and that $A \times (B \setdif C) = (A \times B) \setdif (A \times
        C)$.
\end{exercise}

First claim: $A \times (B \cup C) = (A \times B) \cup (A \times C)$.
\begin{proof}
        Suppose first that $x \in A \times (B \cup C)$, then we know, by
        definition of Cartesian product, that $x = (y,z)$, with $y \in A$ and $z
        \in B \cup C$. There are two possibilities: If $y \in A$ and $z \in B$,
        then $x = (y,z) \in A \times B$, and so $x \in (A \times B) \cup (A
        \times C)$. Similarly, if $y \in A$ and $z \in C$, then $x = (y,z) \in A
        \times C$, which means that $x \in (A \times B) \cup (A \times C)$. In
        both cases we draw the same conclusion.

        Now, assume that $x \in (A \times B) \cup (A \times C)$, then there are
        two possibilities: If $x \in A \times B$, then $x = (y,z)$ with $y \in
        A$ and $z \in B$. Therefore, $x = (y,z)$, with some $y \in A$ and some
        $z \in B \cup C$; this is to say $x \in A \times (B \cup C)$. We proceed
        similarly when $x \in A \times C$, which concludes our proof.
\end{proof}

Second claim: $A \times (B \cap C) = (A \times B) \cap (A \times C)$.
\begin{proof}
        First assume $x \in A \times (B \cap C)$, then $x = (y,z)$ for some $y
        \in A$ and some $z \in B \cap C$. Therefore we are certain that $x =
        (y,z)$ for some $y \in A$ and some $z \in B$, but $x = (y,z)$ for some
        $y \in A$ and some $z \in C$, this is to say that $x \in A \times B$,
        but $x \in A \times C$ as well. Hence the fact that $x \in (A \times B)
        \cap (A \times C)$.

        Now suppose that $x \in (A \times B) \cap (A \times C)$, then $x \in A
        \times B$ and $x \in A \times C$, which means $x = (y,z)$ with $y \in A$
        and $z \in B$, and $x = (y',z')$ with  $y' \in A$ and $z' \in C$. It is
        straightforwardly seen that $y = y'$ and $z = z'$ (because $x = x$), and
        so $x = (y,z)$ for some $y \in A$ and some $z \in B \cap C$. This latter
        statement is precisely $x \in A \times (B \cap C)$.
\end{proof} 

Third claim: $A \times (B \setdif C) = (A \times B) \setdif (A \times C)$
\begin{proof}
        Suppose $x \in A \times (B \setdif C)$, then $x = (y,z)$, with $y \in A$
        and $z \in B \setdif C$. Therefore $x = (y,z)$, for some $y \in A$, and
        some $z$ satisfying $z \in B$ and $z \not\in C$. Thus we see that $x$
        cannot possibly be in $A \times C$ (because $z \not\in C$), while it
        \emph{is} an element of $A \times B$. This is to say $x \in (A \times B)
        \setdif (A \times C)$.

        Conversely, assume $x \in (A \times B) \setdif (A \times C)$, then $x
        \in a \times B$ and $x \not\in A \times C$. Hence $x = (y,z)$ with some
        $y \in A$ and some $z \in B$, but $x \neq (y',z')$ for all $y' \in A$ or
        for all $z' \in C$; since there is at least one $y' \in A$ for which $x
        = (y',z')$ (namely, $y$), we know that  $y \neq (y',z')$ for all $z' \in
        C$. In summary, $x = (y,z)$, with some $y \in A$ and some $z$ which must
        simultaneously satisfy $z \in B$ and $z \not\in C$; hence $x \in A
        \times (B \setdif C)$.
\end{proof}

\begin{exercise}
        Let $A,B,C,D$ be sets. Show that $(A \times B) \cap (C \times D) = (A
        \cap C) \times (B \cap D)$. Is it true that $(A \times B) \cup (C \times
        D) = (A \cup C) \times (B \cup D)$? Is it true that $(A \times B)
        \setdif (C \times D) = (A \setdif C) \times (B \setdif D)$? 
\end{exercise}
\begin{proof}
        Suppose first that $x \in (A \times B) \cap (C \times D)$, then we have
        $x \in A \times B$ and $x \in C \times D$. This readily implies $x = (y,
        z)$ for some  $y \in A$ and some $z \in B$, and $x = (y',z')$ for some
        $y' \in C$ and some $z' \in D$. Since $x = x$, we see that $y = y'$, and
        $z = z'$, hence resulting in  $x = (y,z)$ with $y \in A$, $y \in C$, $z
        \in B$, and $z \in D$, thus with $y \in A \cap C$ and $z \in B \cap D$.
        We conclude $x \in (A \cap C) \times (B \cap D)$.

        Conversely, suppose $x \in (A \cap C) \times (B \cap D)$. Then $x =
        (y,z)$ for some $y \in A \cap C$ and some $z \in B \cap D$. Therefore $y
        \in A$, $y \in C$, $z \in B$, and $z \in D$. This is to say that $x =
        (y,z)$ for some $y \in A$ and $z \in B$, but also $x = (y,z)$ for some
        $y \in C$ and $z \in D$, which means $x \in (A \times B) \cap (C \times
        D)$.
\end{proof}

As for the remaining statements, we can determine whether or not they are true
by relatively straightforward procedures.

Suppose first that $x \in (A \times B) \cup (C \times D)$, then, by definition,
either $x \in A \times B$, or $x \in C \times D$; this in turn implies that $x =
(y, z)$, with either $y \in A$ or $y \in C$, and either $z \in B$ or $z \in D$.
Hence $x \in (A \cup C) \times (B \cup D)$. However, the converse is not
necessarily true: If $x \in (A \cup C) \times (B \cup D)$, then we can merely
state that $x = (y,z)$, with $y \in A \cup C$ and $z \in B \cup D$; at this
point the conditions on $y$ and $z$ yield exactly four combinations of Cartesian
products in which $x$ could possibly lie; the conclusion is that these sets are
not necessarily the same.

Likewise, we may consider an element $x \in (A \times B) \setdif (C \times D)$.
Then $x \in A \times B$, but $x \not\in C \times D$. Consequently, $x = (y,z)$
with $y \in A$ and $z \in B$, while it is certain that either $y \not\in C$ or
$z \not\in D$. Not even this first implication is necessarily true!

% once again the first implication is true, for the above argument
% shows that $x \in (A \setdif C) \times (B \setdif D)$. Even though this may seem
% encouraging, the converse fails to be true in a similar way as before.
