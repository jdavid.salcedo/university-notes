\lecture{3}{Wed 02 Jun 14:21}{Multiplication}
We previously showed some facts about both addition and order in the natural
number system. We will now allow ourselves to use all the rules of algebra
concerning addition that we are familiar with. Thus we may write things like $a
+ b + c = c + b + a$ without need for further justification. Now we are
concerned with multiplication, and just as addition was the repeated increment
operation, multiplication is the repeated addition operation. 
\begin{definition}{Multiplication of natural numbers}{}
        Let $m$ be a natural number. To multiply zero to $m$, we define $0
        \times m := 0$. Now suppose inductively that we have defined how to
        multiply $n$ to $m$, that is, we have defined $n \times m$. Then we can
        multiply $n\pp$ to $m$ by defining $(n\pp) \times m := (n \times m) +
        m$. 
\end{definition}

Thus for instance, 
\begin{align*}
        0 \times m &:= 0, \\
        1 \times m &= (0\pp) \times m := (0 \times m) + m = 0 + m, \\
        2 \times m &= (1\pp) \times m := (1 \times m) + m = 0 + m + m, \\
        3 \times m &= (2\pp) \times m := (2 \times m) + m = 0 + m + m + m, \\
                   &\vdots 
\end{align*}

Now, we could use induction to prove that \textbf{the multiplication of two
natural numbers is a natural number:}
\begin{proof}
        We wish to prove that if $n$ and $m$ are natural numbers, then $n \times
        m$ is a natural number. We proceed by induction on $n$, with fixed $m$.
        To begin with, notice that $0 \times m = 0$, by definition. Since 0 is a
        natural number, this proves the base case.

        Now, suppose that $n \times m$ is a natural number for some natural
        number $n$. Then $(n\pp) \times m = (n \times m) + m$, but we know by
        induction hypothesis that $n \times m$ is a natural number, and that $m$
        is also a natural number. Given that addition is closed on the natural
        numbers, we conclude  $(n\pp) \times m$ is a natural number, thus
        completing the proof. 
\end{proof}

\begin{lemma}{Multiplication is commutative}{}
        Let $n, m$ be natural numbers. Then $n \times m = m \times n$.
\end{lemma}
\begin{proof}
        Left as an exercise.
\end{proof}

Henceforth, we shall abbreviate $n \times m$ as $nm$, and use the usual
convention that multiplication takes precedence over addition, thus for instance
$ab + c$ means $(a \times b) + c$, not $a \times (b + c)$. Also, we will use the
usual notational conventions of precedence for all the other arithmetic
operations when they are defined. 

\begin{lemma}{Positive natural numbers have no zero divisors}{}
        Let $n, m$ be natural numbers. Then $n \times m = 0$ if and only if at
        least one of the $n, m$ is equal to 0. In particular, if $n$ and $m$ are
        both positive, then $nm$ is also positive.
\end{lemma}
\begin{proof}
        Left as an exercise.
\end{proof}

\begin{proposition}{Distributive law}{}
        For any natural numbers $a, b, c$, we have $a(b + c) = ab + ac$ and  $(b
        + c)a = ba + ca$.
\end{proposition}
\begin{proof}
        Since multiplication is commutative, it suffices to show that $a(b + c)
        = ab + ac$. We keep $a$ and $b$ fixed, and use induction on $c$. For the
        base case, we have $a(b + 0) = ab$, but by definition of multiplication
        and commutativity, $a(b + 0) = ab + 0 = ab + a0$, this completes the
        base case. Now, suppose that $a(b + c) = ab + ac$, for some natural
        number $c$. Then we have, for all we know and the induction hypothesis,
        $a(b + (c\pp)) = a(b + c)\pp = a(b + c) + a = ab + (ac + a) = ab +
        a(c\pp)$. This closes the induction. 
\end{proof}

\begin{proposition}{Multiplication is associative}{}
        For any natural numbers $a, b, c$, we have $(a \times b) \times c = a
        \times (b \times c)$.
\end{proposition}
\begin{proof}
        Left as an exercise.
\end{proof}

\begin{proposition}{Multiplication preserves order}{}
        If $a, b$ are natural numbers such that $a < b$, and $c$ is positive,
        then $ac < bc$.
\end{proposition}
\begin{proof}
        Since $a < b$, then $b = a + d$, for some positive natural number $d$.
        Then, multiplying by $c$ and using distributive law, we get $bc = ac +
        dc$; of course, the multiplication of two positive natural numbers,
        i.e., $dc$, is positive, so that $ac < bc$, as required. 
\end{proof}

\begin{corollary}{Cancellation law}{}
        Let $a, b ,c$ be natural numbers such that $ac = bc$ and such that $c$
        is non-zero. Then $a = b$.
\end{corollary}
\begin{proof}
        We proceed by contradiction. By trichotomy, there are three
        possibilities: either $a < b$, $a = b$, or $a > b$. Suppose that $a <
        b$, then, because multiplication preserves order, we have $ac < bc$, a
        contradiction. Similarly, a contradiction arises when considering $a >
        b$. Hence $a = b$, as desired. 
\end{proof}

By dint of these propositions, it is easy to deduce all the familiar rules of
algebra involving addition and multiplication of natural numbers, as we shall
see in the exercises. 

Now that we have these familiar operations, the more primitive notion of
increment will be put aside a little bit, in fact we will rarely see it from
this point onwards. In any case, we can use addition to represent
incrementation, since $n\pp = n + 1$.

\begin{proposition}{Euclidean division algorithm}{}
        Let $n$ be a natural number, and let $q$ be a positive number. Then
        there exist natural numbers $m, r$ such that $0 \le r < q$ and $n = mq +
        r$.
\end{proposition}
\begin{proof}
        Left as an exercise.
\end{proof}
\begin{remark}{}{}
        This last proposition tells us that we can divide a natural number $n$
        by a positive number $q$ to obtain a quotient $m$ (which is another
        natural number) and a remainder (which is less than $q$). This algorithm
        is one of the first results in \emph{number theory}.
\end{remark}

Just like we have used the increment operation to recursively define addition,
and the addition operation to define multiplication, we can use multiplication
to recursively define exponentiation:
\begin{definition}{Exponentiation for natural numbers}{}
        Let $m$ be a natural number. To raise $m$ to the power 0, we define
        $m^{0} := 1$; in particular, we define $0^{0} := 1$. Now suppose
        recursively that $m^{n}$ has been defined for some natural number $n$,
        then we define $m^{n\pp} := m^{n} \times m$.
\end{definition}
Thus for instance:
\begin{align*}
        m^{0} &:= 1, \\
        m^{1} &= m^{0\pp} := m^{0} \times m = 1 \times m, \\
        m^{2} &= m^{1\pp} := m^{1} \times m = 1 \times m \times m, \\
        m^{3} &= m^{2\pp} := m^{2} \times m = 1 \times m \times m \times m, \\
                   &\vdots 
\end{align*}
This definition defines $m^{n}$ for all natural $n$. The theory pertaining to
exponentiation will be more deeply developed once we have defined the integers
and rationals. 

\subsection{Exercises}
We now prove some statements:
\begin{exercise}
        Prove Lemma 1.3.1. (Hint: Modify the proofs of the corresponding lemmas
        from the addition section.)
\end{exercise}
We shall prove three statements, two of which are lemmas for the final
proposition, which is actually Lemma 1.3.1:
\begin{itemize}
        \item \textbf{Lemma.} For any natural number $n$, $n \times 0 = 0$.
                \begin{proof}
                        We use induction. For the base case we should
                        acknowledge that $0 \times 0 = 0$, by definition of
                        multiplication. Now suppose that for some natural number
                        $n$, we have $n \times 0 = 0$. Then we have, by
                        definition, $(n\pp) \times 0 = (n \times 0) + 0$, but by
                        the induction hypothesis, $(n\pp) \times 0 = 0 + 0 = 0$,
                        as desired. 
                \end{proof}
        \item \textbf{Lemma.} For any natural numbers $n$ and $m$, $n \times
                (m\pp) = (n \times m) + n$.
                \begin{proof}
                        We proceed by induction on $n$, with fixed $m$. Firstly,
                        notice that $0 \times (m\pp) = 0$, by definition; but
                        this is equivalent to $0 \times (m\pp) = 0 + 0 = (0
                        \times m) + 0$, completing the base case. Now suppose
                        inductively that some natural number $n$ satisfies $n
                        \times (m\pp) = (n \times m) + n$. Then, by definition
                        of multiplication and the inductive hypothesis, $(n\pp)
                        \times (m\pp) = (n \times (m\pp)) + (m\pp) = (n \times
                        m) + (n + (m\pp)) = ((n \times m) + m) + (n\pp) =
                        ((n\pp) \times m) + (n\pp)$. This closes the induction.
                \end{proof}
        \item \textbf{Proposition.} Let $n, m$ be natural numbers. Then $n
                \times m = m \times n$.
                \begin{proof}
                        We proceed by induction on $n$, with fixed $m$. The base
                        case, $0 \times m = 0 = m \times 0$, follows immediately
                        from the definition and by the first lemma. Now suppose
                        inductively that $n \times m = m \times n$, for some
                        natural number $n$. Then we have, by definition and
                        induction hypothesis, $(n\pp) \times m = (n \times m) +
                        m = (m \times n) \times m$; finally, second lemma
                        implies that $(m \times n) + m = m \times (n\pp)$, which
                        completes the induction.
                \end{proof}
\end{itemize}

\begin{exercise}
        Prove lemma 1.3.2. (Hint: Prove the second statement first.)
\end{exercise}
The statement reads: Let $n, m$ be natural numbers. Then $n \times m = 0$ if and
only if at least one of the $n, m$ is equal to 0. In particular, if $n$ and $m$
are both positive, then $nm$ is also positive.
\begin{proof}
        As suggested by the hint, we begin by proving the second part of the
        statement. Take $n$ and $m$ to be positive natural numbers, then we know
        that $n = \alpha\pp$, $m = \beta\pp$, for some natural numbers $\alpha,
        \beta$. Hence $nm = (\alpha\pp)(\beta\pp) = \alpha(\beta\pp) +
        (\beta\pp)$ but $\beta\pp$ is obviously positive, and so by Proposition
        1.2.4, we can conclude that $nm$ is indeed positive. 

        With that in mind, we now proceed to the main statement. To begin with,
        suppose that $n$ and $m$ are natural numbers such that $n \times m = 0$;
        suppose, for the sake of contradiction that both $n$ and $m$ are
        positive: This is absurd, for if that were the case then $nm$ would have
        to be positive too. 

        Conversely, suppose that at least one of the $n, m$ is 0. Then we
        obviously have, by commutativity and definition of multiplication by 0,
        that $nm = 0$. This completes the proof.
\end{proof}

\begin{exercise}
        Prove Proposition 1.3.2. (Hint: Modify the equivalent proposition for
        addition and use distributive law).
\end{exercise}

Here we are set out to prove that multiplication of natural numbers is
associative: $(a \times b) \times c = a\times (b\times c)$.
\begin{proof}
        We proceed by induction on $a$ with fixed $b$ and $c$. The base case,
        $(0 \times b) \times c = 0 \times c = 0 = 0 \times (b \times c)$,
        follows immediately from the definition of multiplication by 0. Now
        suppose inductively that $(a \times b) \times c = a \times (b \times c)$
        for some natural $n$. Then we have, by definition, $((a\pp) \times b)
        \times c = ((a \times b) + b) \times c$; now, using distributive law and
        inductive hypothesis, we get $((a\pp) \times b) \times c = (a \times b)
        \times c + (b \times c) = a \times (b \times c) + (b\times c) = (a\pp)
        \times (b \times c)$. This closes the induction.
\end{proof}

\begin{exercise}
        Prove the identity $(a + b)^2 = a^2 + 2ab + b^2$ for all natural numbers
        $a, b$.
\end{exercise}
\begin{proof}
        To begin with, we have, by our brief discussion of exponentiation,
        \[
                (a + b)^2 = (a + b)(a + b);
        \] 
        from this point we can justify the following reasoning:
        \begin{align*}
                (a + b)^2 &= a(a + b) + b(a +b),  &\text{(Distributive law)}\\
                          &= aa + ab + ba + bb, &\text{(Distributive law)} \\
                          &= a^2 + ab + ab + b^2, &\text{(Definition of exponentiation)}\\
                          &= a^2 + (ab + ab) + b^2, &\text{(Associativity of addition)}\\
                          &= a^2 + 2ab + b^2 &\text{(Definition of multiplication)}.
        \end{align*}
        This completes the proof.
\end{proof}

\begin{exercise}
        Prove Proposition 1.3.4. (Hint: Fix $q$ and induct on $n$.)
\end{exercise}

Here we will prove the Euclidean division algorithm: Let $n$ be a natural
number, and let $q$ be a positive number. Then there exist natural numbers $m,
r$ such that $0 \le r < q$ and $n = mq + r$.
\begin{proof}
        As suggested, we proceed by induction on $n$ with fixed $q$. For the
        base case, we have $0 = 0 \times q + 0$ and it is obviously true that $0
        \le 0 < q$, because of the reflexivity of order on the natural numbers
        and because $q$ was defined to be positive. 

        Now, suppose that for some natural number $n$ we have $n = mq + r$, with
        $0 \le r < q$. For the inductive step we must show that there exist
        numbers $m', r'$ such that $0 \le r' < q$ and $n\pp = m'q + r'$. We
        should acknowledge that $n\pp = (mq + r)\pp = mq + r +1$, and that, by
        trichotomy, there are two possibilities for $r \pp$: Either $r\pp = q$,
        or $r\pp \neq q$. If $r\pp = q$, then we can write $n\pp = n + 1 = mq +
        (r + 1) = mq + q = (m+1)q = (m\pp)q$, in other words, $m' = m\pp$, and
        $r' = 0$. 

        On the other hand, if $r\pp \neq q$, then we have $0 \le r\pp < q$,
        because we have already stipulated that $0 \le r < q$ (see basic
        properties of order). Then we can write $n\pp = n + 1 = mq + (r + 1) =
        mq + (r\pp)$, but this already satisfies our purposes, i.e., we have $
        m' = m$ and $r' = r\pp$. Hence the induction is closed.
\end{proof}
