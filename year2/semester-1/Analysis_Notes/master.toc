\selectlanguage *[variant=british]{english}
\contentsline {chapter}{\numberline {1}The natural numbers}{3}{chapter.1}%
\contentsline {section}{\numberline {1.1}The Peano axioms}{4}{section.2}%
\contentsline {section}{\numberline {1.2}Addition}{10}{section.19}%
\contentsline {subsection}{\numberline {1.2.1}Exercises}{15}{subsection.35}%
\contentsline {section}{\numberline {1.3}Multiplication}{20}{section.42}%
\contentsline {subsection}{\numberline {1.3.1}Exercises}{23}{subsection.53}%
\contentsline {chapter}{\numberline {2}Set theory}{27}{chapter.59}%
\contentsline {section}{\numberline {2.1}Fundamentals}{27}{section.60}%
\contentsline {subsection}{\numberline {2.1.1}Exercises}{36}{subsection.86}%
\contentsline {section}{\numberline {2.2}Functions}{43}{section.98}%
\contentsline {subsection}{\numberline {2.2.1}Exercises}{48}{subsection.109}%
\contentsline {section}{\numberline {2.3}Images and inverse images}{53}{section.122}%
\contentsline {subsection}{\numberline {2.3.1}Exercises}{58}{subsection.131}%
\contentsline {section}{\numberline {2.4}Cartesian products}{68}{section.143}%
\contentsline {subsection}{\numberline {2.4.1}Exercises}{72}{subsection.150}%
