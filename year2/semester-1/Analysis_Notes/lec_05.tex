\lecture{5}{Sat 19 Jun 10:33}{Functions}

In order to do analysis, it is necessary to introduce the notion of a function
from one set to another. Informally, $f : X \to Y$ denotes an operation that
assigns  to each element $x \in X$, a \emph{unique} element $f(x) \in Y$. The
formal definition is the following:
\begin{definition}{Functions}{}
        Let $X, Y$ be sets, and let $P(x,y)$ be a property pertaining to an
        object $x \in X$ and an object $y \in Y$ ,such that for every $x \in X$
        there is \emph{exactly one} $y \in Y$ for which $P(x,y)$ is true. Then
        we define the function $f : X \to Y$ defined by $P$ on the domain $X$
        and range $Y$ to be the object which, given any input $x \in X$, assigns
        an output $f(x) \in Y$, defined to be the unique object $f(x)$ for which
        $P(x, f(x))$ is true. Thus, for any $x \in X$ and $y \in Y$, 
        \[
                y = f(x) \iff P(x, f(x)) \text{ is true.}
        \] 
\end{definition}

Functions are also known as \emph{maps} or \emph{transformations}, depending on
the context.

As an example, consider $X = \mathbb N$,  $Y = \mathbb N$, and let $P(x,y)$ be
the property that $y = x\pp$. Then we know that for each $x \in \mathbb N$ there
is exactly one $y$ for which this property holds. Hence we can define a function
$f : \mathbb N \to \mathbb N$ associated to this property, such that $f(x) =
x\pp$ for all $x$. This is the \emph{increment function on $\mathbb N$}. One
might as well define a \emph{decrement function} $g : \mathbb N \to \mathbb N$,
associated to a property $P(x,y)$ defined by $x = y\pp$; unfortunately, this
does not define a proper function, for there is no number $y \in \mathbb N$
whose successor is 0 (recall the axioms). Nevertheless, we can define this
decrement function $h : \mathbb N \setdif \{0\} \to \mathbb N$ associated to $x
= y\pp$, because each \emph{positive} natural number is the successor of some
natural number (we proved this in section 1).

Also, we might try to define the \emph{square root function} $\sqrt{} : \mathbb
R \to \mathbb R$ (this example will be informal, for we have not yet defined the
set $\mathbb R$), by associating it to the property $P(x,y)$ defined by $y^2 =
x$, that is, we would like $\sqrt{x}$ to be the number that satisfies
$\left(\sqrt{x}\right)^2 = x$. However, there are two problems with this
definition; Firstly, there are some real numbers $x$ for which there are no $y$
satisfying $y^2 = x$; think about $x = -1$. However, this problem can be avoided
by restricting the domain of our function, so that we can only take $x \in [0,
\infty)$. Secondly, even when $x \in [0, \infty)$, there might be more that one
number $y$ in the range $\mathbb R$ such that $y^2 = x$; for instance, if $x =
4$, then both $y = 2$ and $y = -2$ satisfy $P(x,y)$. This is usually solved by
\emph{taking the positive branch} of the square root, i.e., restricting the
range of the function so that $\sqrt{} : [0,\infty) \to [0,\infty)$. 

We see that specifying a function requires the specifications of its domain, its
range, and some rule for generating the output $f(x)$ from each input; this is
the \emph{explicit} definition of a function. Commonly though, functions are
specified in an abbreviated manner, inasmuch as it is not strange to find
phrases such as ``the function $f(x) := x\pp$'', or ``the function $x \mapsto
x\pp$'', or even ``the function $x\pp$''. These are called \emph{implicit}
definitions, and one should manage them with extreme care. 

We should acknowledge that functions obey the axiom of substitution: If $x =
x'$, then $f(x) = f(x')$; why? Because we have $y = f(x) \iff P(x, y)$, but
since $x = x'$ then we know $P(x', y)$, which happens iff  $y = f(x')$; now
equality is transitive, so  $f(x) = f(x')$. This means that equal inputs imply
equal outputs. On the other hand, unequal inputs do not necessarily imply
unequal outputs; consider the following example:

Let  $X = \mathbb N$, $Y = \mathbb N$, and let $P(x,y)$ be the property $y = 7$.
Then we know for sure that for al $x \in X$, there is a unique $y$ such that
$P(x,y)$, namely, there is only one number 7. Hence we can create the function
$f : \mathbb N \to \mathbb N$ associated to this property: It is simply a
constant function for which $f(x) = 7$. Thus it is possible for different inputs
to generate equal outputs.

\begin{remark}{}{}
        Strictly speaking, functions are not sets, and sets are not functions;
        it does not make sense to ask whether an object $x$ is an element of the
        function $f$, and it does not make sense to apply the set $A$ to an
        input $x$ to create the output $A(X)$. However, we can define a set
        called the \emph{graph} of a function: Thus if $f : X \to Y$, then
        $\text{graph } f = \{(x, f(x)) : x \in X\}$. The graph set completely
        specifies the function, and it is related to the concept of Cartesian
        product.
\end{remark}

Now we regard some some basic concepts and notions for functions. 
\begin{definition}{Equality of functions}{}
        Two functions $f : X \to Y$, $g : X \to Y$ with the same domain and
        range are said to be \textbf{equal}, $f = g$, if and only if $f(x) =
        g(x)$ for all $x \in X$. (If $f$ and $g$ agree for some values, but not
        others, then we do not consider $f$ and $g$ to be equal.\footnote{In
        subsequent chapters, we shall introduce a weaker notion of function
equality, that of two functions being \emph{equal almost everywhere}.})
\end{definition}

The functions $x \mapsto x^2 + 2x + 1$ and $x \mapsto (x + 1)^2$ are equal on
the domain $\mathbb R$. The functions $x \mapsto x$ and $x \mapsto |x|$ are
equal on the domain $\mathbb R ^{+}$, but are not equal on $\mathbb R$.
Obviously, the concept of function equality depends on the choice of domain.

There is a rather boring example of a function, which we call the \emph{empty
function} $f : \emptyset \to X$ from the empty set to an arbitrary set $X$.
Since the empty set has no elements at all, we do not actually need to specify
the property with which the empty function is associated. Note that for each set
$X$, there exists only one function from $\emptyset$ to $X$, since the
definition of set equality asserts that all functions from with those properties
are equal; why? Well, suppose that we have two functions $f : \emptyset \to X$
and $g : \emptyset \to X$, then, trivially, $f(x) = g(x)$ for all $x \in
\emptyset$, because there are no such elements; this means that $f = g$, as
desired.

This notion of equality obeys the usual axioms (left as an exercise). 

A fundamental operation available for functions is that of
\emph{composition}:        
\begin{definition}{Composition}{}
        Let $f : X \to Y$ and $g : Y \to Z$ be two functions, such that the
        range of $f$ is the same set as the domain of $g$. We then define the
        \textbf{composition} $g \circ f : X \to Z$ of the two functions $g$ and
        $f$ to be the function defined explicitly by the formula
        \[
                (g \circ f) (x) := g\left( f(x) \right). 
        \] 
        If the range of $f$ does not match the domain of $g$, we leave the
        composition undefined.
\end{definition}

It is easy to check that composition obeys the axiom of substitution (left as an
exercise).

As a particular example, let $f : \mathbb N \to \mathbb N$ be the function $f(n)
:= 2n$, and let $g : \mathbb N \to \mathbb N$ be the function $g(n) := n + 3$.
Then  $g \circ f$ is the function 
\[
        (g \circ f)(n) = g\left( f(n) \right) = g(2n) = 2n + 3.
\] 
Meanwhile, $f \circ g$ is the function
\[
        (f \circ g)(n) = f\left( g(n) \right) = f(n + 3) = 2(n + 3) = 2n + 6.
\] 

This example clearly shows that composition is not commutative: $f \circ g$ is
not necessarily the same function as $g \circ f$. However, composition is
associative:
\begin{lemma}{Composition is associative}{}
        Let $f : Z \to W$, $g : Y \to Z$, and $h : X \to Y$ be functions. Then
        $f \circ (g \circ h) = (f \circ g) \circ h$.
\end{lemma}
\begin{proof}
        Since $g \circ h : X \to Z$, we have $f \circ (g \circ h) : X \to W$.
        Similarly, since $f \circ g : Y \to W$, we have $(f \circ g) \circ h : X
        \to W$. Thus both functions have the same domain and range. In order to
        see that they are equal, we let $x \in X$, then we have 
        \begin{align*}
                \left( f \circ (g \circ h) \right)(x) &= f\left( (g \circ h)(x) \right) \\
                                                      &= f \left( g (h(x)) \right)  \\
                                                      &= (f \circ g)\left( h(x) \right)  \\
                                                      &= \left( (f \circ g) \circ h \right)(x),
        \end{align*}
        as desired.
\end{proof}

We now describe certain special types of functions: \emph{one-to-one} functions,
\emph{onto} functions, and \emph{invertible} functions.
\begin{definition}{One-to-one functions}{}
        A function $f$ is \textbf{one-to-one} (or \textbf{injective}) if
        different elements map to different elements:
        \[
                x \neq x' \implies f(x) \neq f(x').
        \] 
        Equivalently, a function is one to one if
        \[
                f(x) = f(x') \implies x = x'.
        \] 
\end{definition}
Take, in particular, the function $f : \mathbb Z \to \mathbb Z$ defined by $f(n)
= n^2$. This function is not one-to-one, because the distinct elements $1, -1$
both map to $1$. However, if we restrict the function to just the natural
numbers, $g : \mathbb N \to \mathbb Z$ with $g(n) = n^2$, we see that the
function is in fact one-to-one. Hence the newly introduced definition not only
depends on the operation, but also on the domain.

If a function is not one-to-one, that means we can find $x$ and $x'$ in the
domain $X$ such that $f(x) = f(x')$; that is, two elements that map to the same
output.

\begin{definition}{Onto functions}{}
         A function $f : X \to Y$ is \textbf{onto} (or \textbf{surjective}) if
         $f(X) = Y$, i.e., every element in $Y$ comes from applying $f$ to some
         element in $X$:
          \begin{quote}
                  For every $y \in Y$, there exists $x \in X$ such that $f(x) =
                  y$.
         \end{quote}
\end{definition}

As another informal example, take $f : \mathbb Z \to \mathbb Z$ with $f(n) =
n^2$; we see that this function is not onto, because the negative elements of
$\mathbb Z$ are not the image of any integer under $f$. However, if we restrict
the range to $A = \{n^2 : n \in \mathbb Z\}$, we would have $g : \mathbb Z \to
A$ is an onto function. Thus we see that being onto does not only depend on the
applied operation, but also on the range of the regarded function.

\begin{remark}{}{}
The concepts of injectivity and surjectivity are in many ways dual to each
other, we will leave the assessment of some of these as exercises.         
\end{remark}

\begin{definition}{Bijective functions}{}
        Functions $f : X \to Y$ which are both one-to-one and onto are also
        called \textbf{bijective} or \textbf{invertible}.
\end{definition}

TODO: Last examples on bijectivity!

If $f : X \to Y$ is bijective, then for every $y \in Y$, there is a unique $x
\in X$ such that $f(x) = y$ (there is at least one because of surjectivity, and
the uniqueness is given by virtue of injectivity). This value of $x$ can be
denoted $f^{-1}(y)$; thus $f^{-1}$ is a function from $Y$ to $X$. We call
$f^{-1}$ the \emph{inverse} of $f$.

\subsection{Exercises}
\begin{exercise}
        Show that the definition of equality of functions is reflexive,
        symmetric, and transitive. Also, verify the substitution property: If
        $f, \tilde{f} : X \to Y$ and $g, \tilde{g} : Y \to Z$ are functions such
        that $f = \tilde{f}$ and  $g = \tilde{g}$, then  $g \circ f = \tilde{g}
        \circ \tilde{f}$.
\end{exercise}
Throughout this proof we will exploit the fact that equality of elements in $Y$
satisfies the usual axioms on the equality relation.
\begin{itemize}
        \item (Reflexivity) $f = f$.
                \begin{proof}
                        Suppose $f : X \to Y$. Now take some $x \in X$; we then
                        see that $f(x) = f(x)$, because the equality of elements
                        in $Y$ is reflexive. This shows that $f = f$, as
                        desired.
                \end{proof}
        \item (Symmetry) If $f = g$, then $g = f$.
                \begin{proof}
                        Suppose that $f, g : X \to Y$; suppose further that $f =
                        g$, this means that for all $x \in X$ we have $f(x) =
                        g(x)$. Since there is a sense of symmetry in equality of
                        elements in $Y$, we see that $g(x) = f(x)$ for all  $x
                        \in X$; this shows that $g = f$, completing the proof.
                \end{proof}
        \item (Transitivity) If $f = g$ and $g = h$, then $f = h$.
                 \begin{proof}
                         Let us define the functions $f, g, h : X \to Y$.
                         Suppose, furthermore, that $f = g$ and $g = h$; then
                         $f(x) = g(x)$ and $g(x) = h(x)$ for all $x \in A$. Now,
                         we may use transitivity in the equality of elements in
                         $Y$ to write $f(x) = h(x)$ for \emph{any} $x \in X$,
                         but this means that $f = h$,as desired.
                \end{proof}
        \item The substitution property.
                \begin{proof}
                        Take the hypotheses stated in the exercise. We see that
                        $g \circ f : X \to Z$, and also that $\tilde{g} \circ
                        \tilde{f} : X \to Z$. Now, choose some $x \in X$, we can
                        use the stated equalities to write
                        \begin{align*}
                                (g \circ f)(x) &= g(f(x)) \\
                                               &= g(\tilde{f}(x)).
                        \end{align*}
                        Also, since $\tilde{f}(x) \in Y$, we can write 
                        \begin{align*}
                                (g \circ f)(x) &= \tilde{g}(\tilde{f}(x)) \\
                                               &= (\tilde{g} \circ \tilde{f})(x);
                        \end{align*}
                        this completes the proof.
                \end{proof}
\end{itemize}

\begin{exercise}
        Let $f : X \to Y$ and $g : Y \to Z$ be functions. Show that if $f$ and
        $g$ are both injective, then so is $g \circ f$. Similarly, show that if
        $f$ and $g$ are both surjective, then so is $g \circ f$.
\end{exercise}
\begin{itemize}
        \item Proof for injective functions.
                \begin{proof}
                        Suppose that $f : X \to Y$ and $g : Y \to Z$ are both
                        injective, which is to say that if $x, x' \in X$ and $x
                        \neq x'$, then $f(x) \neq f(x')$, and if $y, y' \in Y$
                        and $y \neq y'$, then $g(y) \neq g(y')$.

                        Now, for what we want to prove, choose $\chi, \chi' \in
                        X$ such that $\chi \neq \chi'$. Then we know that
                        $f(\chi) \neq f(\chi')$, and since $f(\chi), f(\chi')
                        \in Y$, we can write $g(f(\chi)) \neq g(f(\chi'))$ or
                        simply $(g \circ f)(\chi) \neq (g \circ f)(\chi)$. This
                        completes the argument.
                \end{proof}
        \item Proof for surjective functions.
                \begin{proof}
                        Suppose that $f : X \to Y$ and $g : Y \to Z$ are both
                        surjective, which means that for every $y \in Y$, we can
                        write $y = f(x)$, with some $x \in X$, and that for ever
                        $z \in Z$, we can write $z \in g(y')$, for some $y' \in
                        Y$. 

                        Now, for what we want to prove, choose $z \in Z$. Then
                        we know by hypothesis that $z = g(y)$, for some $y \in
                        Y$; in turn, we can write $y$ as  $y = f(x)$, for some
                        $x \in X$. Therefore, $z = g( f(x) ) = (g \circ f)(x)$.
                        Since the choice of $z$ was arbitrary, we have
                        effectively closed the argument.
                \end{proof}
\end{itemize}

\begin{exercise}
        When is the empty function injective? surjective? bijective?
\end{exercise}
Let us begin the argument by recalling the definitions: A function $f : X \to Y$
is one-to-one if $x \neq x' \implies f(x) \neq f(x')$, for some $x, x' \in X$.
Now, the empty function is of the form $g : \emptyset \to X$, so if we suppose
that $x, x' \in \emptyset$ and that $x \neq x'$, we automatically have the
implication (because there cannot be such elements in the empty set). Therefore,
\emph{the empty function is always one-to-one}.

Now we turn to surjectivity: In simple terms, a function $f : X\to Y$ is onto if
$f(X) = Y$. Consider the empty function $g : \emptyset \to Y$; if $Y$ contains
any element $y$, we cannot possibly write $y = f(x)$, because there exists no $x
\in \emptyset$, on the other hand, if $Y = \emptyset$, we can indeed write $y =
f(x)$ with $x \in \emptyset$ for all $y \in Y$, because there would be no such
$y \in Y$ (the implication is vacuously true). Hence \emph{the empty function is
onto if its range is $\emptyset$}.

\begin{exercise}
        In this section we give some cancellation laws for composition. Let $f,
        \tilde{f} : X\to Y$, $g, \tilde{g} : Y \to Z$ be functions. Show that if
        $g \circ f = g \circ \tilde{f}$ and $g$ is injective, then $f =
        \tilde{f}$. Is the same statement true if $g$ is not injective? Show
        that if $g \circ f = \tilde{g} \circ f$ and $f$ is surjective, then $g =
        \tilde{g}$. Is the same statement true when $f$ is not surjective?
\end{exercise}
\begin{itemize}
        \item Proof of the first statement.
                \begin{proof}
                        Suppose that $g \circ f = g \circ \tilde{f}$, and that
                        $g$ is one-to-one. By definition, if we choose some $x
                        \in X$, then $g(f(x))=g(\tilde{f}(x))$, and since $g$ is
                        one to one, we know for sure that $f(x) = \tilde{f}(x)$.
                        Because this is true for an arbitrary $x \in X$, we
                        conclude that $f = \tilde{f}$, as desired. 
                \end{proof}
        \item Proof of the second statement.
                \begin{proof}
                        Assume that $g \circ f = \tilde{g} \circ f$, and that
                        $f$ is onto. Choose $y \in Y$, we can then write $f(x) =
                        y$, for some fixed $x \in X$. But for $x$ in particular
                        we have $(g \circ f)(x) = (\tilde{g} \circ f)(x)$, and
                        so $f(y) = \tilde{g}(y)$, as required.
                \end{proof}
\end{itemize}

\begin{exercise}
        Let $f : X \to Y$ and $g : Y \to Z$ be functions. Show that if $g \circ
        f$ is injective, then $f$ must be injective. Is it true that $g$ must
        also be injective? Show that if $g\circ f$ is surjective, then $g$ must
        be surjective. Is it true that $f$ must also be surjective?
\end{exercise}
\begin{itemize}
        \item Proof of the first statement.
                \begin{proof}
                        To prove that $f$ is injective, let us consider $x, x'
                        \in X$ such that $f(x) = f(x')$. Since functions satisfy
                        the axiom of substitution, we have $(g \circ f)(x) = (g
                        \circ f)(x')$; and given that $g \circ f$ is one-to-one,
                        we know for certain that  $x = x'$, as required.
                \end{proof}
        \item Proof of the second statement.
                \begin{proof}
                        To prove that $g$ is onto, choose $z \in Z$; then, since
                        $g \circ f : X \to Z$ is onto, we can write $z = (g
                        \circ f)(x) = g(f(x))$, for some fixed $x \in X$.
                        However, $f(x)$ is itself an element of $Y$, which we
                        can write as $f(x) = y$. Hence for $z$ we can write $z =
                        g(y)$.
                \end{proof}
\end{itemize}

\begin{exercise}
        Let $f : X \to Y$ be a bijective function, and let $f^{-1} : Y \to X$ be
        its inverse. Verify the cancellation laws $f^{-1}(f(x)) = x$ for all $x
        \in X$ and $f(f^{-1}(y)) = y$ for all $y \in Y$. Conclude that $f^{-1}$
        is also invertible, and has $f$ as its inverse (thus $(f^{-1})^{-1}=f$).
\end{exercise}
\begin{proof}
        Tao defines, if $f$ is bijective, that the only $x \in X$ for which
        $f(x) = y$ (for some fixed $y \in Y$) is denoted as $x = f^{-1}(y)$.
        Hence we trivially have
        \[
                f(x) = y \iff f(f^{-1}(y)) = y;
        \] 
        and since all $y$ can be written as $y = f(x)$ (surjectivity), this
        proves the second statement.
        
        Now, to prove the first statement, we must regard the above fact (that
        is, that $f(f^{-1}(y)) = y$ for all $y \in Y$) by choosing the
        particular quantity $f(x) \in Y$. We then have
        \[
                f(f^{-1}(f(x))) = f(x);
        \] 
        and now, since $f$ is injective, this implies $f^{-1}(f(x)) = x$, as
        required.
\end{proof}

\begin{exercise}
        Let $f : X \to Y$ and $g : Y \to Z$ be functions. Show that if $f$ and
        $g$ are bijective, then so is $g \circ f$, and that we have $(g \circ
        f)^{-1} = f^{-1} \circ g^{-1}$.
\end{exercise}
\begin{proof}
        The first part of the statement follows immediately from the facts
        proven in exercise 2 of this section. 

        On the other hand, we see that $g \circ f : X \to Z$ and so $(g \circ
        f)^{-1} : Z \to X$; similarly, $f^{-1} \circ g^{-1} : Z \to X$, so we
        only need to check whether these function agree in their images. We have
        already shown that $(g \circ f)\left( (g \circ f)^{-1}(z) \right) = z$
        for all $z \in Z$. Hence for any $z$ in particular:
        \begin{align*}
                & (g \circ f)\left( (g \circ f)^{-1}(z) \right) = z, \\
                \implies & g\left( f\left( (g\circ f)^{-1}(z) \right)  \right) = z,\\
                \implies & f\left( (g\circ f)^{-1}(z) \right)= g^{-1}(z), \\
                \implies & (g\circ f)^{-1}(z) = (f^{-1} \circ g^{-1})(z).
        \end{align*}
        Here, each step follows from the axiom of substitution along with the
        cancellation laws for inverse functions, first applying the function
        $g^{-1}$ and then applying $f^{-1}$.
\end{proof}

\begin{exercise}
        If $X$ is a subset of $Y$ ($X \subseteq Y$), let $\iota_{X \to Y} : X
        \to Y$ (shorthand, just $\iota_{X \to Y}$) be the \textbf{inclusion
        mapping} from $X$ to $Y$, defined by mapping $x \mapsto x$ for all $x
        \in X$, i.e., $\iota_{X\to Y}(x) := x$ for all $x \in X$. The mapping
        $\iota_{X \to X}$ in particular is called the \textbf{identity mapping}
        on $X$.
        \begin{enumerate}
                \item Show that if $X \subseteq Y \subseteq Z$, then
                        $\iota_{Y\to Z} \circ \iota_{X\to Y} = \iota_{X\to Z}$.
                        \begin{proof}
                                It is easy to check that both $\iota_{Y \to Z}
                                \circ \iota_{X \to Y}$ and $\iota_{X \to Z}$
                                have the same domain and range (namely, $X$ and
                                $Z$, respectively). Now we only need to assure
                                that they agree in their outputs. 

                                Notice that if $x\in X$, then $x \in Y$ and $x
                                \in Z$, so we have $(\iota_{Y \to Z} \circ
                                \iota_{X \to Y})(x) = \iota_{Y \to Z}(x) = x$;
                                also, $\iota_{X \to Z}(x) = x$, and since
                                equality is transitive, we conclude that
                                $(\iota_{Y \to Z} \circ \iota_{X \to Y})(x) =
                                \iota_{X\to Z}(x)$ for all $x \in X$; this
                                completes the proof.
                        \end{proof}
                \item Show that if $f : A \to B$ is any function, then $f = f
                        \circ \iota_{A\to A} = \iota_{B \to B} \circ f$. 
                        \begin{proof}
                                The conditions for domain and range are easy to
                                show. Now, choose $a \in A$; we obviously have
                                $f(a) = f(a)$, and $\iota_{A \to A}(a) = a$, so
                                that $(f \circ \iota_{A \to A})(a) = f(a)$;
                                finally, $(\iota_{B \to B} \circ f)(a) =
                                \iota_{B \to B}(f(a)) = f(a)$ (because $f(a) \in
                                B$). Hence, by transitivity,
                                \[
                                        f(a) = (f \circ \iota_{A\to A})(a) =
                                        (\iota_{B \to B} \circ f)(a),
                                \]  
                                completing the proof.
                        \end{proof}
                \item Show that if $f : A\to B$ is a bijective function, then $f
                        \circ f^{-1} = \iota_{B \to B}$, and $f^{-1} \circ f =
                        \iota_{A \to A}$.
                        \begin{proof}
                                For the first statement, we note that both $f
                                \circ f^{-1}$ and $\iota_{B \to B}$ have domain
                                $B$ and range $B$. Also, $\iota_{B\to B}(b) = b$
                                for all $b \in B$, whereas $(f \circ f^{-1})(b)
                                = b$ for all $b \in B$. Whence, by transitivity,
                                $f \circ f^{-1} = \iota_{B \to B}$.

                                The second statement is analogous.
                        \end{proof}
                \item Show that if $X$ and $Y$ are disjoint sets, and $f : X \to
                        Z$, $g : Y \to Z$ are functions, then there is a unique
                        function $h : X \cup Y \to Z$ such that $h \circ
                        \iota_{X \to X \cup Y} = f$ and $h \circ \iota_{Y \to X
                        \cup Y} = g$.
                        \begin{proof}
                                We first show the existence of $h$. In order to
                                guarantee that such a function is well defined,
                                we take advantage of the fact that $X$ and $Y$
                                are disjoint sets. Let $h : X \cup Y \to Z$ be
                                the function defined by $h(a) = f(a)$ if $a \in
                                X$, and $h(a) = g(a)$ if $a \in Y$. Let us see
                                that this function is well defined: Assume, for
                                the sake of contradiction, that there are $a, a'
                                \in X\cup Y$ such that $a = a'$, but $h(a) \neq
                                h(a')$, since $f$ and $g$ are both well-defined
                                functions, this can only possibly happen if
                                $h(a) = f(a)$ and  $h(a') = g(a)$, which by
                                definition would necessarily mean that $a \in X$
                                and $a' \in Y$; now the axiom of replacement
                                would show that $a \in X$ and $a \in Y$, which
                                is to say $a \in X \cap Y$. This is obviously
                                absurd, since the sets $X$ and $Y$ are disjoint,
                                so there is no way in which our new function $h$
                                can be ill-defined.

                                The function $h \circ \iota_{X \to X\cup Y}$ has
                                domain $X$ and range $Z$, which is in accordance
                                with the domain and range of $f$. Now choose
                                some $x \in X$, we then have $(h \circ \iota_{X
                                \to X \cup Y})(x) = h(x) = f(x)$, by definition
                                of the inclusion mapping and of the $h$
                                function. Hence $h \circ \iota_{X \to X \cup Y}
                                = f$, as required. 
                                
                                In a similar spirit, the function $h \circ
                                \iota_{Y \to X \cup Y}$ has domain $Y$ and range
                                $Z$, which match those of function $g$. If we
                                choose $y \in Y$, we see that $(h \circ \iota_{Y
                                \to X \cup Y})(y) = h(y) = g(y)$, so that $h
                                \circ \iota_{Y \to X \cup Y} = g$, as desired.

                                We finally prove the uniqueness of this
                                function. Suppose we have another function
                                $\tilde{h} : X \cup Y \to Z$ such that the
                                conditions herein hold. If we choose $a \in X
                                \cup Y$, then we have either $a \in X$ or $a \in
                                Y$, but not both (they are disjoint); if $a \in
                                X$, then $(\tilde{h} \circ \iota_{X \to X \cup
                                Y})(a) = f(a)$, and it is evident from the
                                definition of the inclusion mapping that
                                $\tilde{h}(a) = (\tilde{h} \circ \iota_{X \to X
                                \cup Y})(a)$; hence $\tilde{h}(a) = f(a) = h(a)$
                                (because $a \in X$). On the other hand, if $a
                                \in Y$, then $(\tilde{h} \circ \iota_{Y \to X
                                \cup Y})(a) = g(a)$, and a similar argument
                                shows that $\tilde{h}(a) = g(a) = h(a)$.
                                Inasmuch as these functions agree in both cases,
                                we have $h = \tilde{h}$, completing the proof.
                        \end{proof}
        \end{enumerate}
\end{exercise}
