\lecture{6}{Thu 24 Jun 09:26}{Images and inverse images}

We know, by definition of function, that $f : X \to Y$ takes individual elements
$x \in X$ and maps them to elements $f(x)$. Function can also take subsets in
$X$ to subsets in $Y$. This is the key concept of \emph{image}.

\begin{definition}{Images of sets}{}
        If $f : X \to Y$ is a function from $X$ to $Y$, and $S$ is a set in $X$,
        we define $f(S)$ to be the set 
        \[
                f(S) = \{f(x) : x \in S\}.
        \] 
        This set is a subset of $Y$, and is sometimes called the \textbf{image}
        of $S$ under the mapping $f$. We sometimes call $f(S)$ the forward image
        of $S$, so as to distinguish it from the concept of \textbf{inverse
        image} $f^{-1}(S)$ of $S$, which we define below. 
\end{definition}

Notice that the set $f(S)$ is well-defined by the axiom of replacement, but it
is also possible to define it via the axiom of specification. (Recall that the
axiom of replacement implies the axiom of specification, and not vice versa.)
This is left as a challenge to the reader (!).

As an example, take the mapping $f : \mathbb{N} \to \mathbb{N}$ so that $f(x) =
2x$. Then the forward image of $\{1,2,3\}$ is $\{2,4,6\}$, that is:
\[
        f\left( \{1,2,3\} \right) = \{2,4,6\}.
\] 
Intuitively speaking, to compute $f(S)$ we take every element $x \in S$ and
apply $f$ to each of them individually. The image is then the set containing the
resulting objects.

In the above example, the image had the same ``size'' as the original set. But
sometimes the image can be smaller; specifically, when $f$ is not one-to-one.
Take for instance the following example.

(Informal) Let $\mathbb{Z}$ be the set of integers, and let $f : \mathbb{Z} \to
\mathbb{Z}$ be the mapping $f(x) = x^2$, then
\[
        f\left( \{-1,0,1,2\} \right) = \{0,1,4\}.  
\] 
Here $f$ is not one to one because, for example, $f(-1) = f(1) = 1$, which in
turn implies that the image of this particular set is smaller than the set
itself. 

We may acknowledge that, since not all functions are one-to-one, we always have
\[
        x \in S \implies f(x) \in f(S),
\] 
but \textbf{not necessarily}
\[
        f(x) \in f(S) \implies x \in S.
\] 
Indeed, in the above informal example, $f(-2) = 4$ lies in the set
$f(\{-1,0,1,2\})$, but  $-2$ is not in $ \{-1,0,1,2\}$. The statement that
always holds is 
\[
        y \in f(S) \iff y = f(x) \text{ for some } x \in S.
\] 
Why? Well by definition, $y \in f(S) \iff y \in \{f(x) : x \in S\}$, which is a
shorthand for $y \in \{z : z = f(x) \text{ for some } x \in S\}$; the axiom of
replacement then tells us that this happens if and only if the property $P(z) :
x = f(x)$ for some $x \in S$ holds for $y$ in particular; whence $y = f(x)$ for
some $x \in S$, as claimed.

\begin{definition}{Inverse images}{}
        Let $f : X \to Y$ be a function. If $U$ is a subset of $Y$, we define
        the set $f^{-1}(U)$ to be the set
        \[
                f^{-1}(U) = \{x \in X : f(x) \in U\}.
        \] 
        In other words, $f^{-1}(U)$ consists of all the elements that map into
        $U$:
        \[
                x \in f^{-1}(U) \iff f(x) \in U.
        \] 
        We call $f^{-1}(U)$ the \textbf{inverse image} of $U$ under $f$.
\end{definition}

As an example, take the mapping $f : \mathbb{N} \to \mathbb{N}$ such that $f(x)
= 2x$, then $f\left( \{1,2,3\} \right) = \{2,4,6\}$, but $f^{-1}\left( \{1,2,3\}
\right) = \{1\}$. Thus the forward image of $\{1,2,3\}$ and its inverse image
are quite different. Moreover, note that
\[
        f\left( f^{-1}\left(\{1,2,3\}\right) \right) \neq \{1,2,3\}.
\] 
Why? Simply because this function is not onto: There are some elements in the
set $\{1,2,3\}$ that cannot be written as $f(x) = 2x$ for some $x \in \mathbb N$
(they do not have some ``preimage''); in this particular case, the inverse image
yields a singleton set, whose forward image can only be another singleton
because an element of the domain can only map to a single element in the range.
Thus the inverse image cannot possibly have the same ``size'' as the original
set, and hence will obviously not be equal to it.

Now we present an informal example: If $f : \mathbb Z \to \mathbb Z$ is the
mapping $f(x) = x^2$, then
\[
        f^{-1}(\{0,1,4\}) = \{-2,-1,0,1,2\}.
\] 
Notice that $f$ does not have to be invertible in order for $f^{-1}(U)$ to make
sense. Also, images and inverse images do not quite invert each other, for
instance, we have 
\[
        f^{-1}\left( f\left( \{-1,0,1,2\} \right) \right) \neq \{-1,0,1,2\}. 
\] 
Why? Basically because of the fact that our given function is not one-to-one, so
we might have $f(x) = f(x')$ with $x \neq x'$, which ultimately implies that we
might have chosen some ``incomplete'' original values that map into a certain
subset of the range: I categorise them as incomplete in the sense that they may
not be the complete set of numbers that \emph{can} possibly map into their
corresponding values (in this case, for instance, we forgot to include the
element $-2$, which also produces a value in the forward image).

\begin{remark}{}{}
        If $f$ is a bijective function, we have defined $f^{-1}$ in two slightly
        different ways, but this is not an issue, because both definitions are
        actually equivalent.
\end{remark}

We may notice that functions are not sets. However, we do consider functions as
a type of object, so we may construct sets of functions. In particular, we
should be able to consider the set of \emph{all} functions mapping from a set
$X$ into a set $Y$. To do this, we need to introduce another axiom:
\begin{axiom}{Power set axiom}{}
        Let $X$ and $Y$ be sets. Then there exists a set, denoted by $Y^{X}$,
        which consists of all functions mapping from $X$ to $Y$, thus
        \[
                f \in Y^{X} \iff \text{($f$ is a function with domain $X$ and
                range $Y$).}
        \] 
\end{axiom}

For instance, if $X = \{4,7\}$ and  $Y = \{0,1\}$, then the set $Y^{X}$ consists
of four functions: The function mapping $4 \mapsto 0$ and $7 \mapsto 1$; the
function that maps $4 \mapsto 1$ and $7 \mapsto 0$; the function that assigns $4
\mapsto 0$ and $7 \mapsto 0$; and the one that maps $4 \mapsto 1$ and $7\mapsto
1$.

A consequence of the power set function is the following lemma:
\begin{lemma}{}{}
        Let $X$ be a set. Then there is a well-defined set 
         \[
        \{Y : Y \text{ is a subset of } X\}.
        \] 
\end{lemma}
\begin{proof}
        Left as an exercise.
\end{proof}
\begin{remark}{}{}
        The set $\{Y : Y \text{ is a subset of } X\}$ is called the
        \textbf{power set} of $X$, and is denoted by $2^{X}$. For instance, if
        $a,b,c$ are distinct objects, we have 
        \[
        2^{\{a,b,c\}} = \{\emptyset, \{a\}, \{b\}, \{c\}, \{a,b\}, \{a,c\},
        \{b,c\}, \{a,b,c\}\}.
        \] 
\end{remark}

This notation suggests the \emph{cardinality} of out power set, so while
$\{a,b,c\}$ has three elements, $2^{\{a,b,c\}}$ has $2^3 = 8$ elements. This
issue will be further discussed in the chapter about infinite sets.

For the sake of completeness, we introduce one further axiom that will allow us
to enhance the axiom of pairwise union by defining unions of much larger
collections of sets.

\begin{axiom}{Union}{}
        Let $A$ be a set, all of whose elements are themselves sets. Then there
        exists a set $\bigcup A$, whose elements are precisely those objects
        that are elements of the elements of $A$. Thus for all objects $x$, we
        have
        \[
                x \in \bigcup A \iff \text{($x \in S$ for some $S \in A$).}
        \] 
\end{axiom}

So, for instance, if $A = \{\{2,3\}, \{3,4\}, \{4,5\}\}$, then $\bigcup A =
\{2,3,4,5\}$.

The axiom of union, together with the axiom of pair sets, implies the axiom of
pairwise union (this is actually left as an exercise). 

Another important consequence of the axiom of union is that we can now formally
define unions of \emph{families} of \emph{indexed} sets. Suppose that we have
some set $I$ (an ``index'' set), and that we can assign some set $A_{\alpha}$ to
each $\alpha \in I$ (i.e., we can \emph{label} some sets with the elements of
our index set); then we can form the union set $\bigcup_{\alpha \in I}
A_{\alpha}$ by defining
\[
\bigcup_{\alpha \in I} A_{\alpha} := \bigcup \{A_{\alpha} : \alpha \in I\} ,
\] 
which is well defined thanks to the axiom of replacement and the axiom of union.
Thus we see that for any object $y$ :
\[
        y \in \bigcup_{\alpha \in I} A_{\alpha} \iff \text{($y \in A_{\alpha}$
        for some $\alpha \in I$).}
\] 

Thus for instance, if $I = \{1,2,3\}$, $A_1 = \{2,3\}$, $A_2 = \{3,4\}$, and
$A_3 = \{4,5\}$, then $\bigcup_{\alpha \in \{1,2,3\}} A_{\alpha} = \{2,3,4,5\}$.

Note that if $I = \emptyset$, then $\bigcup_{\alpha \in I} A_{\alpha} =
\emptyset$; why? Well it suffices to show that assuming $y \in \bigcup_{\alpha
\in I} A_{\alpha}$ is absurd in that situation. Notice that the above condition
would happen iff $y \in \bigcup \{A_\alpha : \alpha \in i\}$, which by axiom of
union means that $y \in S$ for some $S \in \{A_{\alpha} : \alpha \in I\}$. In
turn, axiom of replacement tells us that would happen iff $x \in S$ and $S =
A_{\alpha}$ for some $\alpha \in I$, so $x \in A_{\alpha}$ for some $\alpha \in
I$. However, there would exist no such $\alpha \in I$ (for $I = \emptyset$),
which is the expected contradiction.

We can similarly form intersections of families of sets, as long as the index
set is not empty. More specifically, given any set $I$, and given an assignment
of a set $A_{\alpha}$ to each $\alpha \in I$, we can define the intersection
$\bigcap_{\alpha \in I} A_{\alpha}$ by first choosing some element $\beta$ of
$I$ (which we can do since $I \neq \emptyset$), and setting
\[
        \bigcap_{\alpha \in I} A_{\alpha} := \{x \in A_{\beta} : x \in
        A_{\alpha} \text{ for all } \alpha \in I\}.
\] 
This set is well-defined by the axiom of specification. There is a remarkable
fact in this definition: Even though it seems like it depends on the initial
choice of $\beta$, it does not (left as an interesting exercise). It is easy to
see from this definition and the axiom or specification that
\[
        y \in \bigcap_{\alpha \in I} A_{\alpha} \iff \text{($y \in A_{\alpha}$
        for all $\alpha \in I$)}.
\] 
\begin{remark}{}{}
        As  a final remark, we should say that the set theory axioms introduce
        so far are known as the Zermelo-Fraenkel axioms of set theory. There is
        one further axiom that one normally introduces, and that is the
        \emph{axiom of choice}. Such an axiom will be used in developing the
        theory of infinite sets.
\end{remark}

\subsection{Exercises}
\begin{exercise}
        Let $f : X \to Y$ be a bijective function, and let $f^{-1} : Y \to X$ be
        its inverse. Let $V$ be any subset of $Y$. Prove that the forward image
        of $V$ under $f^{-1}$ is the same as the inverse image of $V$ under $f$.
        Thus the fact that both sets are denoted y $f^{-1}(V)$ will not lead to
        any inconsistency. (This problem does not arise when $f$ is not
        invertible, for then there would exist only one object $f^{-1}$ denoting
        the inverse image.)
\end{exercise}
\begin{proof}
        We may momentarily denote the forward image of $V$ under $f^{-1}$ by
        $A$, and the inverse image of $V$ under $f$ as $B$. We shall now prove
        that $A = B$.

        To begin with, suppose that $x \in A$, then by definition of forward
        image we know that $x = f^{-1}(y)$ for some $y \in V$. Since both $x$
        and $f^{-1}(y)$ are in  $X$, we can apply the axiom of substitution with
        the function $f$:
         \[
                 f(x) = f\left( f^{-1}(y) \right),
        \] 
        and since $f$ is bijective, this means that $f(x) = y$. Now, we know
        that $y \in V$, so that $f(x) \in V$ too. So $x$ is an element that maps
        into $V$ under $f$; this implies, by definition of inverse image, that
        $x \in B$, as desired.

        Conversely, suppose that $x \in B$, then we must have $f(x) \in V$,
        which can be otherwise written as $f(x) = y$ for some $y \in V$. Since
        $f$ is bijective, this implies that $x = f^{-1}(y)$, and since $y \in
        V$, we have that $x \in A$, completing the proof.
\end{proof}

\begin{exercise}
        Let $f : X \to Y$ be a function from one set $X$ to another set $Y$, let
        $S$ be a subset of $X$, and let $U$ be a subset of $Y$. What, in
        general, can be said about $f^{-1}(f(S))$ and $f\left( f^{-1}(U) \right)
        $
\end{exercise}

Based on the examples in the text, I would make the following two claims:
\begin{itemize}
        \item $f^{-1}(f(S)) \supseteq S$
                \begin{proof}
                        Suppose that $x \in S$. Then we obviously have $f(x) \in
                        f(S)$. Whence, $x$ maps into the set $f(S) \subseteq Y$
                        upon application of the function $f$, which by
                        definition means that $x \in f^{-1}(f(S))$, as claimed.
                \end{proof}
        \item $f\left( f^{-1}(U) \right) \subseteq U$
                \begin{proof}
                        Suppose we have $y \in f\left( f^{-1}(U) \right)$, this
                        means that $y = f(x)$, for some $x \in f^{-1}(U)$, which
                        in turn tells us that $y = f(x)$ with $f(x) \in U$.
                        Since the $\in$ relation abides by the axiom of
                        substitution, we have $y \in U$
                \end{proof}
\end{itemize}

\begin{exercise}
        Let $A$, $B$ be two subsets of a set $X$, and let $f : X \to Y$ be a
        function. Show that $f(A \cap B) \subseteq f(A) \cap f(B)$, that $f(A)
        \setdif f(B) \subseteq f(A \setdif B)$, and that $f(A \cup B) = f(A)
        \cup f(B)$.
\end{exercise}
\begin{itemize}
        \item $f(A \cap B) \subseteq f(A) \cap f(B)$.
                \begin{proof}
                        Let $y \in f(A \cap B)$, then we know that $y = f(x)$
                        for some $x \in A \cap B$, that is, for some $x$
                        satisfying both $x \in A$ and $x \in B$. Hence $y =
                        f(x)$ for some $x \in A$, but also $y = f(x)$ for the
                        same $x \in B$; we conclude that $y \in f(A)$ and $y \in
                        f(B)$, simultaneously: $y \in f(A) \cap f(B)$. This
                        completes the proof. 

                        (The opposite direction does not hold because if $y \in
                        f(A) \cap f(B)$, then in writing $y = f(x)$ for $x \in
                        A$, and $y = f(x')$ for $x' \in B$, we cannot guarantee
                        that $x = x'$.)
                \end{proof}
        \item $f(A) \setdif f(B) \subseteq f(A \setdif B)$.
                \begin{proof}
                        Let $y \in f(A) \setdif f(B)$, then $y \in f(A)$, but $y
                        \not\in f(B)$, i.e., $y = f(x)$ for some $x \in A$, and
                        $y \neq f(x')$ \emph{for all} $x' \in B$. In other
                        words, $y = f(x)$ for some $x \in A$ which must strictly
                        satisfy $x \not\in B$; we conclude that $y = f(x)$ for
                        some  $x \in A \setdif B$, which by definition implies
                        $y \in f(A \setdif B)$.

                        (The converse does not work because in writing $y =
                        f(x)$ for some $x$ satisfying $x \in A$ and $x \not\in
                        B$, we cannot assure in general that $y \neq f(x')$
                        \emph{for all} $x' \in B$. In other words, it may be
                        true that this $x$ in particular is not in $B$, but if
                        the function in not one-to-one, there might be another
                        $x'$ which does lie in $B$, such that $f(x) = f(x')$.)
                \end{proof}
        \item $f(A \cup B) = f(A) \cup f(B)$.
                \begin{proof}
                        Suppose fist that $y \in f(A \cup B)$, this means that
                        $y = f(x)$ for some $x$ satisfying $x \in A$ or $x \in
                        B$. If $y = f(x)$ for some $x \in A$, we can safely
                        write $x \in A \cup B$, so $y \in f(A\cup B)$;
                        similarly, if $y = f(x)$ for some  $x \in B$, then we
                        can as well write $y \in f(A \cup B)$.

                        Now suppose that $y \in f(A) \in f(B)$, then $y = f(x)$
                        with  $x \in A$, or $y = f(x)$ with $x \in B$. If $x \in
                        A$, then we also have $x \in A \cup B$, so $y = f(x)$
                        for an $x$ value satisfying $x \in A \cup B$; we
                        conclude that $y \in f(A \cup B)$. The other case is
                        analogous. This completes the proof.
                \end{proof} 
\end{itemize}

\begin{exercise}
        Let $f : X \to Y$ be a function from one set $X$ to another set $Y$, and
        let $U$, $V$ be subsets of $Y$. Show that $f^{-1}(U \cup V) = f^{-1}(U)
        \cup f^{-1}(V)$, that $f^{-1}(U \cap V) = f^{-1}(U) \cap f^{-1}(V)$, and
        that $f^{-1}(U \setdif V) = f^{-1}(U) \setdif f^{-1}(V)$
\end{exercise}
\begin{itemize}
        \item $f^{-1}(U \cup V) = f^{-1}(U) \cup f^{-1}(V)$.
                \begin{proof}
                        Suppose first that $x \in f^{-1}(U \cup V)$, then we
                        have $f(x) \in U \cup V$. If $f(x) \in U$, then $x \in
                        f^{-1}(U)$, and so by definition of set union it is safe
                        to say $x \in f^{-1}(U) \cup f^{-1}(V)$; similarly, if
                        $f(x) \in V$, then $x \in f^{-1}(V)$, so $x \in
                        f^{-1}(U) \cup f^{-1}(V)$. Thus in both cases we see
                        that $x \in f^{-1}(U) \cup f^{-1}(V)$.

                        Conversely, if $x \in f^{-1}(U) \cup f^{-1}(V)$, then $x
                        \in f^{-1}(U)$ or $x \in f^{-1}(V)$. If $x \in
                        f^{-1}(U)$, then we know $f(x) \in U$, which implies
                        $f(x) \in U \cup V$; hence $x \in f^{-1}(U \cup V)$. The
                        other case is analogous. This completes the proof.
                \end{proof}
        \item $f^{-1}(U \cap V) = f^{-1}(U) \cap f^{-1}(V)$.
                \begin{proof}
                        Suppose that $x \in f^{-1}(U \cap V)$, then $f(x) \in U
                        \cap V$; this can be written as $f(x) \in U$ and $f(x)
                        \in V$, which are exactly the definitions of $x \in
                        f(U)$ and $x \in f(V)$. Thus $x \in f(U) \cap f(V)$.

                        Now, if $x \in f^{-1}(U)$ and $x \in f^{-1}(V)$, we have
                        both $f(x) \in U$ and $f(x) \in V$, which can be
                        rewritten as $f(x) \in U \cap V$, or just $x \in f(U
                        \cap V)$.
                \end{proof}
        \item $f^{-1}(U \setdif V) = f^{-1}(U) \setdif f^{-1}(V)$.
                \begin{proof}
                        If $x \in f^{-1}(U \setdif V)$, we have $f(x) \in U
                        \setdif V$, which means that $f(x) \in U$ but $f(x)
                        \not\in V$. This statement is logically equivalent to $x
                        \in f(U)$ and $x \not\in f(V)$, or $x \in f(U) \setdif
                        f(V)$.

                        Conversely, if $x \in f(U) \setdif f(V)$, then $x \in
                        f(U)$, but $x \not\in f(V)$, otherwise written as $f(x)
                        \in U$ and $f(x) \not\in V$. Thus $f(x) \in U \setdif
                        V$, which is to say $x \in f(U \setdif V)$. Completing
                        the proof.
                \end{proof}
\end{itemize}

\begin{exercise}
        Let $f : X \to Y$ be a function. Show that $f\left( f^{-1}(U) \right) =
        U$ for every $U \subseteq Y$ if and only if $f$ is surjective. Show that
        $f^{-1}(f(S)) = S$ for all $S \subseteq X$ if and only if $f$ is
        injective.
\end{exercise}
\begin{itemize}
        \item $f\left( f^{-1}(U) \right) = U$ for every $U \subseteq Y$ if and
                only if $f$ is surjective.
                \begin{proof}
                        Suppose first that for every $U \subseteq Y$ we have
                        $f\left( f^{-1}(U) \right) = U$. Since we know $Y
                        \subseteq Y$, we have in particular $f\left( f^{-1}(Y)
                        \right) = Y$. Hence $y \in Y$ iff $y = f(x)$ with $x \in
                        f^{-1}(Y)$; and since $f^{-1}(Y) \subseteq X$,we
                        conclude $y \in Y$ iff $y = f(x)$ with $x \in X$. This
                        means that if we pick an arbitrary element of $Y$, then
                        we are certain we can write it as $f(x')$ for dome $x'
                        \in X$. Hence $f$ is surjective.

                        For the converse, we may acknowledge that 
                        \[
                                f\left( f^{-1}(U) \right) \subseteq U
                        \] 
                        is always true (see the second exercise in this
                        section). Now, if $f$ is surjective and we pick an
                        arbitrary subset $U \subseteq Y$ and an arbitrary
                        element $y \in U$, we can write $y$ as $y = f(x)$ for
                        some $x \in X$; however, we know indeed that $f(x) \in
                        U$, so $x \in f^{-1}(U)$. Thus $y = f(x)$ for  $x \in
                        f^{-1}(U)$, i.e., $y \in f\left( f^{-1}(U) \right)$, as
                        required.
                \end{proof}
        \item $f^{-1}(f(S)) = S$ for all $S \subseteq X$ if and only if $f$ is
                injective.
                \begin{proof}
                        Suppose that for every $S \subseteq X$ we have
                        $f^{-1}(f(S)) = S$. Now choose $x, x' \in X$ such that
                        $f(x) = f(x')$, and aiming for a contradiction suppose
                        $x \neq x'$. Now pick in particular the set $\{x\}$, we
                        know $f(\{x\}) = \{f(x)\}$, and since both $x$ and $x'$
                        yield the same value $f(x)$ under $f$, we would have
                        $f^{-1}(f(\{x\})) = \{x,x'\}$, which contradicts the
                        fact that  $f^{-1}(f(S)) = S$ for \emph{all} subsets
                        $S$.

                        Considering the converse, notice that 
                        \[
                                f^{-1}(f(S)) \supseteq S
                        \]
                        is always true. Now, if $f$ is injective and we pick an
                        arbitrary subset $S \subseteq X$ and an arbitrary
                        element $x \in f^{-1}(f(S))$, we have $f(x) \in f(S)$;
                        this means that $f(x) = f(x')$ for some $x' \in S$, and
                        since $f$ is injective we conclude $x = x'$. Therefore,
                        by the axiom of substitution, $x \in S$, as desired.
                \end{proof}
\end{itemize}

\begin{exercise}
        Prove Lemma 2.3.1. (Hint: start with the set $\{0,1\}^{X}$ and apply the
        replacement axiom, replacing each function $f$ with the object
        $f^{-1}(\{1\})$.)
\end{exercise}
\begin{proof}
        As suggested, we consider the set $\{0,1\}^{X}$, which is the set of all
        functions $f : X \to \{0,1\}$. I claim that every subset $Y$ of $X$ can
        be associated to a function $f_{Y} : X \to \{0,1\}$, such that $y
        \mapsto 0$ whenever $y \not\in Y$, and $y \mapsto 1$ whenever $y \in Y$.
        This is to say that we can represent any subset of $X$ as $Y =
        f^{-1}(\{1\})$. Hence, using the axiom of replacement, we construct the
        set
        \[
                P := \left\{ Y : Y = f^{-1}(\{1\}) \text{ for some } f \in
                \{0,1\}^{X} \right\}. 
        \] 
        Now we need to prove that $P = 2^{X}$, where $2^{X} := \{Y : Y  \text{
        is a subset of } X\}$.

        Suppose first that $A \in P$, then we know $A = f^{-1}(\{1\})$, for some
        $f : X \to \{0,1\}$. Now, we need to see that $A$ is indeed a subset of
        $X$, thus implying $A$ is in $2^{X}$. Pick $a \in A$, we see that $f(a)
        = 1$, and so $a$ must be in the domain $X$ of $f$ for the function to be
        well-defined. Whence $A$ is a subset of $X$, or rather $A \in 2^{X}$.

        Now suppose that $A \in 2^{X}$, then $A \subseteq X$. Since the set
        $\{0,1\}^{X}$ contains all mappings from $X$ to $\{0,1\}$, we know in
        particular that there exists a mapping $f : X \to \{0,1\}$ for which
        $f(A) = \{1\}$ and $f(X \setdif A) = \{0\}$. I claim that these
        conditions imply $f^{-1}(\{1\}) = A$; Suppose, for the sake of
        contradiction that $f^{-1}(\{1\}) \neq A$, then there are two
        possibilities: Either
        \begin{itemize}
                \item There is some $x \in A$ such that that $x \not\in
                        f^{-1}(\{1\})$, which would imply $f(x) \neq 1$. This
                        would in turn yield a contradiction, because hypotheses
                        tell us that whenever $x \in A$, we have $f(x) = 1$.
        \end{itemize}
        Or 
        \begin{itemize}
                \item There is some $x \in f^{-1}(\{1\})$ such that $x \not\in
                        A$. In this case for $f$ to make sense, we require that
                        $x \in X \setdif A$, and we already know that $f(x) =
                        1$. This contradicts the fact that if $x \in X \setdif
                        A$, then $f(x) = 0$.
        \end{itemize}
        Thus we conclude that $f^{-1}(\{1\}) = A$, which means that $A \in P$,
        as desired. This completes the proof.
\end{proof}

\begin{exercise}
        Let $X, Y$ be sets. Define a partial function from $X$ to $Y$ to be any
        function $f : X' \to Y'$ whose domain $X'$ is a subset of $X$, and whose
        range $Y'$ is a subset of $Y$. Show that the collection of all partial
        functions from $X$ to $Y$ is itself a set. (Hint: Use exercise 6 of this
        section, the power set axiom, the replacement axiom, and the union
        axiom.)
\end{exercise}
\begin{proof}
        As suggested, we will start from the fact that we can define a set
        $\left(Y'\right)^{X'}$ consisting of all functions mapping from
        \emph{any} subset $X' \subseteq X$ to some fixed subset $Y' \subseteq
        Y$. 

I was initially tempted to start from the set 
\[
        \left\{f : f \in \left(Y\right)'^{X'} \text{ for some } X' \in
        2^{X}\right\},
\] 
but the reader should notice that the above ``set'' is not actually
well-defined, for in order to apply the replacement axiom, there must be only
one $f$ satisfying $f \in \left(Y'\right)^{X'}$ for any specific $X'$, which is
not always necessarily the case, because there might be multiple different
functions sharing the same domain and range. 

More appropriately, we are allowed to use the axiom of replacement on the set
$2^{X}$ (containing all subsets of $X$), putting in place sets that contain
mappings from any $X' \in 2^{X}$ to some \emph{fixed} subset $Y'$ of $Y$;
specifically:
\[
        \left\{ A : A = \left(Y'\right)^{X'} \text{ for some } X' \in
        2^{X}\right\}.
\] 
We can readily apply the union of these sets to simply get the set of all
functions from any $X' \subseteq X$ to some specific $Y' \subseteq Y$ (we need
to do this because we have a set of sets containing functions, but we just want
a set containing functions):
\[
        \bigcup \left\{ A : A = \left(Y'\right)^{X'} \text{ for some } X' \in
        2^{X}\right\}.
\] 

Finally, we need to consider all possible subsets $Y'$ of $Y$, for which we can
proceed in a similar fashion: by using replacement on $2^{Y}$ and putting a set
of the above form in place. That is,
\[
        \left\{ B : B = \bigcup \left\{ A : A = \left(Y'\right)^{X'} \text{ for
        some } X' \in 2^{X}\right\} \text{ for some } Y' \in 2^{Y} \right\},
\] 
which we shall abbreviate as
\[
        \left\{ \bigcup \left\{ \left(Y'\right)^{X'} : X' \in 2^{X}\right\} : Y'
        \in 2^{Y} \right\}.
\] 
Finally, I claim that this nested replacement can be transformed into the
desired set (the set of all partial functions from $X$ to $Y$) by applying once
again the union operator:
\[
        \mathcal{S} := \bigcup \left\{ \bigcup \left\{ \left(Y'\right)^{X'} : X'
        \in 2^{X}\right\} : Y' \in 2^{Y} \right\}.
\] 

This whole argument is reasonable enough, we now need to write our proof that
the set $\mathcal{S}$ is indeed equal to the set $\mathcal{F}$, which contains
all partial functions from $X$ to $Y$.

Firstly, suppose $f \in \mathcal{S}$, then by the union axiom we have $f \in A$
for some
\[ 
        A \in \left\{ \bigcup \left\{ \left(Y'\right)^{X'} : X' \in
        2^{X}\right\} : Y' \in 2^{Y} \right\}.
\]
Hence, we have
\[
A = \bigcup \left\{ \left(Y'\right)^{X'} : X' \in 2^{X}\right\}
\] 
for some $Y' \subseteq Y$; this in turn means that
\[
f \in \bigcup \left\{ \left(Y'\right)^{X'} : X' \in 2^{X}\right\}
\] 
for some $Y' \subseteq Y$.

Thus $f \in B$ for some
\[
B \in \left\{ \left(Y'\right)^{X'} : X' \in 2^{X}\right\},
\]
and for some $Y' \subseteq Y$. Then $B = \left(Y'\right)^{X'}$ for some $X'
\subseteq X$. We therefore conclude that $f \in \left(Y'\right)^{X'}$ for some
$Y' \subseteq Y$ and some $X' \subseteq X$. This means that $f$ is a partial
function from $X$ to $Y$, or $f \in \mathcal{F}$, as desired.

Conversely, suppose that $f \in \mathcal{F}$, so $f$ is a partial function from
$X$ to $Y$. Without loss of generality, suppose that $f : X'' \to Y''$, with all
the usual hypotheses on $X''$ and $Y''$. It is clear by definition that $f \in
\left(Y''\right)^{X''}$, which readily implies (if we define $A :=
\left(Y''\right)^{X''}$) that $f \in A$ for some
\[
A \in \left\{ \left(Y''\right)^{X'} : X' \in 2^{X}\right\}
\]
(note that we have written $Y''$, the specific range of our hypothesised
function). Hence
\[
f \in \bigcup \left\{
\left(Y''\right)^{X'} : X' \in 2^{X}\right\},
\]
which can be taken to mean (if we
define
\[
B := \bigcup \left\{ \left(Y''\right)^{X'} : X' \in 2^{X}\right\}
\])
that $f \in B$ for some
\[
B \in \bigcup \left\{ \left(Y'\right)^{X'} : X' \in
2^{X}\right\},
\]
with some $Y' \in 2^{Y}$ (namely, $Y'' \subseteq Y$). By the
replacement axiom, this implies that $f \in B$, for some
\[
B \in \left\{ \bigcup \left\{ \left(Y'\right)^{X'} : X' \in 2^{X}\right\} : Y'
\in 2^{Y} \right\}.
\]
And so 
\[
f \in \bigcup \left\{ \bigcup \left\{ \left(Y'\right)^{X'} : X' \in
2^{X}\right\} : Y' \in 2^{Y} \right\} = \mathcal{S},
\]
completing the proof.
\end{proof}

\begin{exercise}
        Show that the axiom of pairwise union can be deduced from the first
        axiom of set theory (sets are objects), the axiom of singleton and pair
        sets, and the axiom of union.
\end{exercise}
\begin{proof}
        This amounts to showing that we can construct exactly the same sets that
        could be constructed with the pairwise union axiom, just using the
        listed axioms. Suppose we have any two sets $A$ and $B$; because sets
        are objects the set $ \{A, B\} $ is well-defined, and so we can apply
        the union axiom to construct the new set $U := \bigcup \{A,B\}$. I claim
        this set is exactly the same as $A \cup B$.

        Indeed, by the union axiom we have:
        \[
                x \in U \iff x \in S \text{ for some } S \in \{A,B\} \iff x \in
                A \text{ or } x \in B.
        \] 
        This completes the proof.
\end{proof}

\begin{exercise}
        Show that if $\beta$ and $\beta'$ are two elements of a set $I$, and to
        each $\alpha \in I$ we assign a set $A_\alpha$, then
        \[
                \{x \in A_\beta : x \in A_{\alpha} \text{ for all } \alpha \in
                I\} = \{x \in A_{\beta'} : x \in A_{\alpha} \text{ for all }
        \alpha \in I\},
        \] 
        and so the definition of $\bigcap_{\alpha \in I} A_{\alpha}$ does not
        depend on $\beta$. Also, explain why 
         \[
                 y \in \bigcap_{\alpha \in I} A_{\alpha} \iff \text{($y \in
                 A_{\alpha}$ for all $\alpha \in I$)}.
        \] 
\end{exercise}
\begin{proof}
        We need to prove a usual set equality. Suppose that $y \in \{x \in
        A_\beta : x \in A_{\alpha} \text{ for all } \alpha \in I\}$; then we
        have, by the specification axiom, $y \in A_\beta$ and $y \in A_{\alpha}$
        for all $\alpha \in I$. Hence we need to have $y \in A_{\alpha}$ for all
        $\alpha \in I$, which in particular means that $y \in A_{\beta'}$, and
        $y \in A_{\alpha}$ for all $\alpha \in I$. This allows us to conclude
        that $y \in \{x \in A_{\beta'} : x \in A_{\alpha} \text{ for all }
        \alpha \in I\}$. The converse is shown analogously.

        Furthermore, we see that, as an intermediate stage in the whole
        derivation above, we have 
        \begin{align*}
                y \in \{x \in A_\beta : x \in A_{\alpha} \text{ for all } \alpha \in I\} 
                &\iff y \in \{x \in A_{\beta'} : x \in A_{\alpha} \text{ for all } \alpha \in I\} \\
                &\iff \text{($y \in A_{\alpha}$ for all $\alpha \in I$)},
        \end{align*} 
        which justifies the stated fact.
\end{proof}

\begin{exercise}
        Suppose that $I$ and $J$ are two sets, and for all $\alpha \in I \cup B$
        let $A_{\alpha}$ be a set. Show that 
        \[
                \left( \bigcup_{\alpha \in I} A_{\alpha} \right) \cup \left(
                \bigcup_{\alpha \in J} A_{\alpha} \right) = \bigcup_{\alpha \in
        I\cup J} A_{\alpha}.
        \] 
        If $I$ and $J$ are non-empty, show that
        \[
                \left( \bigcap_{\alpha \in I} A_{\alpha} \right) \cap \left(
                \bigcap_{\alpha \in J} A_{\alpha} \right) = \bigcap_{\alpha \in
        I\cup J} A_{\alpha}. 
        \] 
\end{exercise}

Proof for unions (first claim):
\begin{proof}
        Suppose first that we have some object $x$ satisfying
         \[
                 x \in \left( \bigcup_{\alpha \in I} A_{\alpha} \right) \cup
                 \left( \bigcup_{\alpha \in J} A_{\alpha} \right);
        \] 
        by definition, this means that either $x \in A_{\alpha}$ for some
        $\alpha \in I$, or $x \in A_{\alpha'}$ for some $\alpha' \in J$. In
        either case, it is straightforward to conclude that $x \in A_{\alpha}$
        with some $\alpha \in I$ or some $\alpha \in J$ (i.e., for some $\alpha
        \in I \cup J$). Therefore, 
        \[
        x \in \bigcup_{\alpha \in I\cup J} A_{\alpha},
        \] 
        as desired.

        Conversely, if we suppose
        \[
        x \in \bigcup_{\alpha \in I\cup J} A_{\alpha},
        \] 
        then we know that $x \in A_{\alpha}$ for some $\alpha \in I \cup J$. In
        the case where $\alpha \in I$, we clearly have $x \in A_{\alpha}$ for
        some $\alpha \in I$, which by definition means
        \[
        x \in \bigcup_{\alpha \in I} A_{\alpha},
        \] 
        and so by definition of set union we conclude
        \[
        x \in \left( \bigcup_{\alpha \in I} A_{\alpha} \right) \cup \left(
        \bigcup_{\alpha \in J} A_{\alpha} \right).
        \] 
        the case in which $\alpha \in J$ is analogous. This completes the proof.
\end{proof}

Proof for intersections (second claim):
\begin{proof}
        Suppose first that there is some object $x$ satisfying
        \[
                x \in \left( \bigcap_{\alpha \in I} A_{\alpha} \right) \cap
                \left( \bigcap_{\alpha \in J} A_{\alpha} \right).
        \] 
        Hence we have $x \in A_{\alpha}$ for all $\alpha \in I$ and $x \in
        A_{\alpha'}$ for all $\alpha' \in J$. Therefore, for all $\alpha$
        satisfying either $\alpha \in I$ or $\alpha \in J$ (i.e., for all
        $\alpha \in I \cup J$), it follows that $x \in A_{\alpha}$; that is,
        \[
        x \in \bigcap_{\alpha \in I\cup J} A_{\alpha}.
        \] 

        Conversely, suppose that
        \[
        x \in \bigcap_{\alpha \in I\cup J} A_{\alpha}.
        \] 
        We then have that, for all $\alpha \in I \cup J$, $x \in A_{\alpha}$.
        Hence for all $\alpha \in I$ and for all $\alpha \in J$, since they all
        in turn satisfy $\alpha \in I \cup J$, we must have $x \in A_{\alpha}$;
        thereby implying that $x \in A_{\alpha}$ for all $\alpha \in I$, and $x
        \in A_{\alpha'}$ for all $\alpha' \in J$, as desired.
\end{proof}

\begin{exercise}
        Let $X$ be a set, let $I$ be a non-empty set, and fora all $\alpha \in
        I$ let $A_{\alpha}$ be a subset of $X$. Show that
        \[
                X \setdif \bigcup_{\alpha \in I} A_{\alpha} = \bigcap_{\alpha
                \in I}(X \setdif A_{\alpha}),
        \] 
        and
        \[
                X \setdif \bigcap_{\alpha \in I} A_{\alpha} = \bigcup_{\alpha
                \in I}(X \setdif A_{\alpha}).
        \] 
\end{exercise}

First statement:
\begin{proof}
        Suppose first that
        \[
                x \in X \setdif \bigcup_{\alpha \in I} A_{\alpha};
        \] 
        then we know that $x \in X$, but $x \not\in \bigcup_{\alpha \in I}
        A_{\alpha}$, which is to say that $x \not\in A_{\alpha}$ for all $\alpha
        \in I$. Hence, for all $\alpha \in I$, $x \in X \setdif A_{\alpha}$; and
        this means that
        \[
        x \in \bigcap_{\alpha \in I}(X \setdif A_{\alpha}).
        \] 

        Conversely, if
        \[
        x \in \bigcap_{\alpha \in I}(X \setdif A_{\alpha}),
        \] 
        then $x \in X \setdif A_{\alpha}$ for all $\alpha \in I$, which means
        that $x \in X$, but $x \not\in A_{\alpha}$ for all $\alpha \in I$. The
        latter statement is logically equivalent to $x \in X$, but $x \not\in
        \bigcup_{\alpha \in I} A_{\alpha}$, completing the proof.
\end{proof}

Second statement:
\begin{proof}
        Suppose that 
        \[
        x \in X \setdif \bigcap_{\alpha \in I} A_{\alpha}.
        \] 
        Then $x \in X$, but $x \not\in \bigcap_{\alpha \in I} A_{\alpha}$; that
        is, $x \not\in A_{\alpha}$ for some $\alpha \in I$. We thus conclude
        that $x \in X \setdif A_{\alpha}$ for some $\alpha \in I$. Therefore
        \[
        x \in \bigcup_{\alpha \in I}(X \setdif A_{\alpha}).
        \] 

        Conversely, if
        \[
        x \in \bigcup_{\alpha \in I}(X \setdif A_{\alpha}),
        \] 
        then we have $x \in X$, but $x \not\in A_{\alpha}$ for some $\alpha \in
        I$. This is equivalent to saying $x \in X$, but $x \not\in
        \bigcap_{\alpha \in I} A_{\alpha}$, which completes the proof.
\end{proof}
