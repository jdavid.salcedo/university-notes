\lecture{5}{Tue 16 Mar 14:50}{Uniform Convergence and Power Series}

Now our attention will shift from particular infinite sums to equations like the following:
\[
e^{x}= 1+\frac{x}{1!}+\frac{x^2}{2!}+\cdots,
\] 
which concern sums of quantities that depend on $x$. In other words, we are interested in \textbf{functions} defined by equations of the form
\[
        f(x) = f_1(x) + f_2(x) +f_3(x) +\cdots
\] 
(in our previous example each $f_n = x^{n-1} /(n-1)!$). In such a situation, $(f_n)$ is a sequence of functions; for each $x$ we obtain a sequence of numbers $(f_n(x))$, and $f(x)$ is the sum of this sequence. To analyse such functions, it will be certainly necessary to remember the fact that each sum 
\[
        f_1(x)+f_2(x)+f_3(x)+\cdots = \sum_{n=1}^{\infty} f_n(x)
\] 
is, by definition, the \emph{limit of the sequence of partial sums:}
\[
        f_1(x),f_1(x)+f_2(x),f_1(x)+f_2(x)+f_3(x),\ldots.
\] 
Hence, if we define a new sequence of functions $(s_n) $ by
\[
s_n = f_1+\cdots+f_n,
\] 
then we can express this fact more succinctly as 
\[
        f(x) = \lim_{n \to \infty} s_n(x).
\] 

Nevertheless, and as to begin with, we shall concentrate on \emph{functions defined as limits of sequences of functions}:
\[
        f(x) = \lim_{n \to \infty} f_n(x),
\] 
rather than on functions defined directly as infinite sums. Everything about such functions may be summarised relatively easily by stating that: \textbf{nothing one would expect to be true actually is!} Our intuition deceives us as we can get counterexamples for statements that, in principle, sound quite reasonable.

The first one of these counterexamples shows that even if each $f_n$ is continuous, the function $f$ may not be; one does not even need to define the functions $f_n$  in a complicated way:
\[
        f_n(x) =\begin{cases}
                x^{n}, \quad 0\le x\le 1 \\
        1, \quad x> 1.
        \end{cases}
\] 
These functions are all continuous, but the function $f(x) = \lim_{n \to \infty} f_n(x)$ is not continuous; in fact,
\[
        \lim_{n \to \infty} f_n(x) = \begin{cases}
                0, \quad 0\le x<1 \\
                1, \quad x\ge1.
        \end{cases}
\] 
\begin{figure}[ht]
    \centering
    \incfig[0.25]{first-counterexample}
    \caption{first counterexample}
    \label{fig:first-counterexample}
\end{figure}
Another example of this same phenomenon is illustrated in the following figure, where the functions $f_n$ are defined by 
\[
        f_n(x) = \begin{cases}
                -1, \quad x\le -\frac{1}{n} \\
                nx, \quad -\frac{1}{n}\le x\le \frac{1}{n}\\
                1, \quad \frac{1}{n}\le x.
        \end{cases}
\] 
\begin{figure}[ht]
    \centering
    \incfig[0.3]{another-function}
    \caption{another function}
    \label{fig:another-function}
\end{figure}
In this case, if $x<0$, then $f_n(x)$ is equal to $-1$, for large enough  $n$, and if $x>0$, then $f_n(x)$ is eventually $1$, while $f_n(0) = 0$, for all $n$. Thus 
 \[
         \lim_{n \to \infty} f_n(x) = \begin{cases}
                 -1,\quad x<0 \\
                 0,\quad x=0 \\
                 1,\quad x>0;
         \end{cases}
\] 
so, once again, the function $f(x) = \lim_{n \to \infty} f_n(x)$ is not continuous, even though each $f_n$ is continuous.

By ``rounding off the corners'' in the previous example it is even possible to produce a sequence of \emph{differentiable} functions $(f_n)$ for which the function $f(x) = \lim_{n \to \infty} f_n(x)$ is not continuous. One such sequence is not difficult to define explicitly by regarding the fact that the function defined by $\sin(x\pi /2)$ on an interval $[-1,1]$ yields a line that curves near the edges at $-1$ and 1:
\[
        f_n(x) = \begin{cases}
                -1, \quad x\le -\frac{1}{n}\\
                \sin\left( \frac{n\pi x}{2} \right), \quad -\frac{1}{n}\le x\le \frac{1}{n}\\
                1, \quad \frac{1}{n}\le x.
        \end{cases}
\]
\begin{figure}[ht]
    \centering
    \incfig[0.2]{sequence-of-differentiable-functions}
    \caption{sequence of differentiable functions}
    \label{fig:sequence-of-differentiable-functions}
\end{figure}
These functions are differentiable, but we still have
\[
        \lim_{n \to \infty} f_n(x) = \begin{cases}
                -1, \quad x<0\\
                0, \quad x=0\\
                1, \quad x>0.
        \end{cases}
\] 

Continuity and differentiability are, moreover, not the only properties for which problems arise. Another difficulty is illustrated by the sequence $(f_n)$ that is defined as follows: On the interval $[0,1 /n]$, the graph of $f_n$ forms an isosceles triangle of altitude $n$, and $f_n(x) = 0$ for $x\ge 1 /n$. These functions may be defined explicitly by considering the formula for a line on the Cartesian plane, which ultimately give
\[
        f_n(x) = \begin{cases}
                2n^{2}x, \quad 0\le x\le \frac{1}{2n} \\
                2n - 2n^2x, \quad  \frac{1}{2n}\le x\le \frac{1}{n}\\
                0, \quad  \frac{1}{n}\le x\le 1.
        \end{cases}
\] 
Because this sequence varies so erratically near 0, our intuition might suggest that  $\lim_{n \to \infty} f_n(x)$ does not always exist. Nevertheless, this limit does exist for all $x$ in the domain, and the function $f(x)$ is even continuous. In fact, note that if $x>0$, then  $f_n(x)$ is eventually 0 for $n$ large enough; moreover, $f_n(0)= 0$ for all $n$, so that we certainly have $\lim_{n \to \infty} f_n(0)=0$. In other words, $f(x) = \lim_{n \to \infty} f_n(x) = 0$, for all x. Also, the integral reveals the strange behaviour of this sequence:

Obviously, since the area of a triangle is half the base times the altitude,
\[
        \int_0^{1}f_n(x) = \frac{1}{2n}\cdot n = \frac{1}{2},
\] 
but
\[
        \int_0^{1}f(x) = 0.
\] 
Thus, in this case,
\[
        \lim_{n \to \infty} \int_0^{1}f_n(x)dx \neq \int_0^{1}\lim_{n \to \infty} f_n(x) dx.
\] 

Notice that, for all the above mentioned functions, although it is true that
\[
        f(x) = \lim_{n \to \infty} f_n \quad \text{ for all $x$ in some set $A$,}
\] 
the graphs of the functions $f_n$ do not approach the graph of $f $ in the sense of lying close to it; if we draw a strip of total width $2\varepsilon$ around $f$, then the graphs of $f_n$ do not lie completely within this strip, no mater how large an $n$ we choose. Of course, for each $x$ there is some $N$ such that the point $(x, f_n(x))$ lies within the strip for $n>N$ (this amounts to the fact that $\lim_{n \to \infty} f_n(x) = f(x)$. But there is a subtlety: It is necessary to choose larger and larger $N$ 's as $x$ is chosen closer and closer to 0, \emph{no one $N$ will work for all $x$ at once}.

This means that
\[
        \text{for all $\varepsilon >0$, and for all $x$ in $A$, there is some $N$ such that if $n>N$, then $|f(x) - f_n(x)|<\varepsilon$. }
\] 
But in each case different $N$'s must be chosen for different $x$'s, and in each case it is \emph{not true} that 
\[
        \text{for all $\varepsilon >0$, there is some $N$ such that for all $x$ in $A$, if $n>N$, then $|f(x) - f_n(x)|<\varepsilon$. }
\] 

These two statements have totally different meanings. If a sequence $(f_n)$ satisfies the second condition, then the graphs of  $f_n$ eventually lie close to the graph of $f$; this condition turns out to be the one which makes the study of limits of functions reasonable. 
\begin{definition}{Uniform Convergence}{}
        Let $f(n)$ be a sequence of functions defined on $A$, and let $f$ be a function which is also defined on $A$. Then $f$ is called the \textbf{uniform limit} of $(f_n)$ on $A$ if for every $\varepsilon>0$, there is some $N$ such that for all $x$ in $A$,
        \[
                \text{if } n>N \text{ then } |f(x) -f_n(x)|<\varepsilon.
        \] 
        We also say that $(f_n)$ \textbf{converges uniformly to $f$ on $A$ }, or that $f_n$ \textbf{approaches $f$ uniformly on $A$}.
\end{definition}

As a contrast to this definition, if we know only that
\[
        f(x) = \lim_{n \to \infty} f_n(x) \quad \text{for each $x$ in $A$},
\] 
then we say that $(f_n)$ \textbf{converges pointwise to $f$ on $A$}. Clearly, uniform convergence implies pointwise convergence, but not the other way around.

Uniform convergence is quite useful, and we shall demonstrate it with the following theorems. Firstly, we note that integration is a particularly easy topic to tackle with this concept.

\begin{theorem}{}{}
         Suppose that $(f_n)$ is a sequence of functions which are integrable on $[a,b]$, and that $(f_n)$ converges uniformly on $[a,b]$ to a function $f$ which is integrable on $[a,b]$. Then 
         \[
         \int_a^{b}f = \lim_{n \to \infty} \int_a^{b}f_n.
         \] 
\end{theorem}
\begin{proof}
        Let $\varepsilon > 0$. We know that there is some $N$ such that for all $n>N$, 
        \[
                |f(x) - f_n(x)|<\varepsilon \quad \text{for all $x$ in $[a,b]$.}
        \] 
        Note that if $g$ is some real function integrable on $[a,b]$, then $-|g(x)|\le g(x)\le |g(x)|$, which means that $-\int_a^{b}|g(x)|dx \le \int_a^{b}g(x)dx\le \int_a^{b}|g(x)|dx$, or $|\int_a^{b}f(x)dx| \le \int_a^{b}|g(x)|dx$. Thus if $n>N$ we have
        \begin{align*}
                \left|\int_b^{b}f(x)dx - \int_a^{b}f_n(x)dx\right| &= \left| \int_a^{b} [f(x) - f_n(x)] dx \right|\\
                                                                   &\le \int_a^{b}|f(x)-f_n(x)| dx \\
                                                                   &< \int_a^{b}\varepsilon dx \\
                                                                   &= \varepsilon (b-a).
        \end{align*}
       Since this is true for any given $\varepsilon>0$, it follows that 
       \[
               \lim_{n \to \infty} \int_a^{b}f_n = \int_a^{b}f,
       \] 
       completing the proof.
\end{proof}

The treatment of continuity is just a bit more difficult, involving an ``$\varepsilon /3$-argument,'' that is, a three- step estimate of $|f(x)-f(x+h)|$. 

\begin{theorem}{}{}
        Suppose that $(f_n)$ is a sequence of functions which are continuous on $[a,b]$, and that $(f_n) $ converges uniformly on $[a,b]$ to $f$. Then $f$ is also continuous on $[a,b]$.
\end{theorem}
\begin{proof}
        For all $x$ in $[a,b]$ we must prove that $f$ is continuous at $x$. We will deal only with $x$ in $(a,b)$ ; the cases $x=a$and $x=b$ require the usual simple modifications (calculating the limits from above or from bellow).

        Let $\varepsilon>0$. Since $(f_n)$ converges uniformly to $f$ on $[a,b]$, there is some $n$ such that 
        \[
                |f(y)-f_n(y)|<\frac{\varepsilon}{3} \quad \text{for all $y$ in $[a,b]$.}
        \] 
        In particular, for all $h$ such that both $x$ and $x+h$ are in $[a,b]$, we have
        \begin{gather}
                |f(x)-f_n(x)|<\frac{\varepsilon}{3}, \\
                |f(x+h)-f_n(x+h)|<\frac{\varepsilon}{3}.
        \end{gather}
        Now, $f_n$ is continuous, so there is some $\delta>0$ such that for $|h|<\delta$ we have
        \begin{gather}
                |f_n(x)-f_n(x+h)|<\frac{\varepsilon}{3}.
        \end{gather}
        Thus if $|h|<\delta$ for delta sufficiently small, all three conditions are met simultaneously (this is guaranteed by uniform convergence) and
        \begin{align*}
                |f(x+h)-f(x)| &= |f(x+h)-f_n(x+h)+f_n(x+h)-f_n(x)+f_n(x)-f(x) \\
                              &\le |f(x+h)-f_n(x+h)|+|f_n(x+h)-f_n(x)|+|f_n(x)-f(x)| \\
                              &< \frac{\varepsilon}{3} + \frac{\varepsilon}{3} + \frac{\varepsilon}{3} \\
                              &= \varepsilon.
        \end{align*}
        This proves that $f$ is continuous at $x$.
\end{proof}

The situation for differentiability is quite different; if each $f_n$ is differentiable, and if $(f_n)$ converges uniformly to $f$, it is still not necessarily true that $f$ is differentiable. For instance, the figure shows that there is a sequence of differentiable functions which converge uniformly to $f(x) = |x|$, which is not differentiable at 0.

\begin{figure}[ht]
    \centering
    \incfig[0.3]{sequence-converging-to-absolute-value}
    \caption{sequence converging to absolute value}
    \label{fig:sequence-converging-to-absolute-value}
\end{figure}

Even if $f$ is differentiable, it may not be true that 
\[
        f'(x) = \lim_{n \to \infty} {f_n}'(x);
\] 
this is actually not surprising if we consider the fact that a smooth function can be approximated by very rapidly oscillating functions. For example, if 
\[
        f_n (x) = \frac{1}{n}\sin\left( n^2x \right),
\] 
then $(f_n)$ converges uniformly to $f(x) = 0$, but 
\[
        {f_n}'(x) = n\cos\left( n^2x \right),
\] 
and $\lim_{n \to \infty} n\cos(n^2x)$ does not always exist (for example, it does not exist for $x = 0$).

However, the fundamental theorem of calculus practically guarantees that something can be said about derivatives of a sequence of functions, by virtue of the first theorem on integrals. 
\begin{theorem}{}{}
Suppose that $(f_n)$ is a sequence of functions which are differentiable on $[a,b]$, with integrable derivatives ${f_n}'$, and that  $(f_n) $ converges pointwise to $f$. Suppose further that $({f_n}')$ converges uniformly on $[a,b]$ to some continuous function $g$. Then $f$ is differentiable and 
         \[
                 f'(x) = \lim_{n \to \infty} {f_n}'(x).
         \] 
\end{theorem}
\begin{proof}
        Applying the first theorem of this chapter to the interval $[a,x]$, we see that for each $x$ we have
        \begin{align*}
                \int_a^{x}g &= \lim_{n \to \infty} \int_a^{x}{f_n}' \\
                            &= \lim_{n \to \infty} \left( f_n(x) - f_n(a) \right)  \\
                            &= f(x)-f(a).
        \end{align*}
        Now, since $g$ is continuous, it follows that $f'(x) = g(x) = \lim_{n \to \infty} {f_n}'(x)$ for any $x $ in the interval $[a,b]$.
\end{proof}

Now that the basics have been cleared, it is clear how to treat functions defined as infinite sums,
\[
        f(x) = f_1(x)+f_2(x)+f_3(x)+\cdots.
\] 
This equations means, of course, that
\[
        f(x) = \lim_{n \to \infty} f_1(x)+\cdots+f_n(x);
\] 
and thus our previous theorems apply when the new sequence 
 \[
         f_1, f_1+f_2, f_1+f_2+f_3,\ldots
\] 
converges uniformly to $f$. This is the only case we are interested in.
\begin{definition}{Uniformly Convergent Series}{}
        The series $\displaystyle \sum_{n=1}^{\infty} f_n$ \textbf{converges uniformly} (more formally: The sequence $(f_n)$ is \textbf{uniformly summable}) \textbf{to $f$ on $A$ }, if the sequence
        \[
         f_1, f_1+f_2, f_1+f_2+f_3,\ldots
        \] 
        converges uniformly to $f$ on $A$.
\end{definition}

We can now apply each of our known theorems to uniformly convergent series; the results may be stated in one common corollary.

\begin{corollary}{}{}
        Let $\displaystyle \sum_{n=1}^{\infty} f_n$ converge uniformly to $f$ on $[a,b]$. 
        \begin{enumerate}
                \item If each $f_n$ is continuous on $[a,b]$, then $f$ is continuous on $[a,b]$.
                \item If $f$ and each $f_n$ are integrable on $[a,b]$, then 
                        \[
                        \int_a^{b}f= \sum_{n=1}^{\infty} \int_a^{b}f_n.
                        \] 
                \item Moreover, if $\displaystyle \sum_{n=1}^{\infty} f_n$ converges pointwise to $f$ on $[a,b] $, each $f_n$ has an integrable derivative ${f_n}'$, and $\displaystyle \sum_{n=1}^{\infty} {f_n}'$ converges uniformly on $[a,b]$ to some continuous function, then 
                         \[
                                 f'(x) = \sum_{n=1}^{\infty} {f_n}'(x) \quad \text{for all $x$ in $[a,b]$.}
                        \] 
        \end{enumerate}
\end{corollary}
\begin{proof}
        \begin{enumerate}
                \item If each $f_n$ is continuous, then so is each partial sum $f_1+\cdots+f_n$; since  $f$ is the uniform limit of the sequence $f_1,f_1+f_2,f_1+f_2+f_3,\ldots$, we conclude $f$ is continuous by previous theorems.
                \item Since $f_1,f_1+f_2,f_1+f_2+f_3,\ldots$ converges uniformly to $f$, it follows from the first theorem that
                        \begin{align*}
                                \int_a^{b}f &= \lim_{n \to \infty} \int_a^{b}(f_1+\cdots+f_n) \\
                                            &= \lim_{n \to \infty} \left( \int_a^{b}f_1 +\cdots + \int_a^{b}f_n \right)  \\
                                            &= \sum_{n=1}^{\infty} \int_a^{b}f_n.
                        \end{align*}
                \item Each function $f_1+\cdots+f_n$ is differentiable with derivative ${f_1}'+\cdots+{f_n}'$, and ${f_1}',{f_1}'+{f_2}',{f_1}'+{f_2}'+{f_3}', \ldots$ converges uniformly to a continuous function by hypothesis. It follows from the third theorem that
                        \begin{align*}
                                f'(x) &= \lim_{n \to \infty} (f_1(x)+\cdots+f_n(x))' \\
                                      &= \lim_{n \to \infty} {f_1(x)}'+\cdots+{f_n}'(x) \\
                                      &= \sum_{n=1}^{\infty} {f_n}'(x)
                        \end{align*}
        \end{enumerate}
        This completes the proof.
\end{proof}

Now we need some sort of condition that ensures that a sequence of the form $f_1,f_1+f_2,f_1+f_2+f_3,\ldots$ converges uniformly. The most important condition is given by the following theorem

\begin{theorem}{The Weierstrass M-Test}{} 
        Let $(f_n)$ be a sequence of functions defined on $A$, and suppose that $(M_n)$ is a sequence of numbers such that
\[
        |f_n(x)|<M_n \quad \text{for all $x$ in $A$.}
\] 
Suppose, moreover, that $\displaystyle \sum_{n=1}^{\infty} M_n$ converges. Then for each $x$ in $A$, the series $\displaystyle \sum_{n=1}^{\infty} f_n(x)$ converges (in fact, absolutely), and $\displaystyle \sum_{n=1}^{\infty} f_n$ converges uniformly on $A$ to the function 
\[
        f(x) = \sum_{n=1}^{\infty} f_n(x)
\] 
\end{theorem}
\begin{proof}
        For each $x$ in $A$ the series $\displaystyle \sum_{n=1}^{\infty} |f(x)|$ converges, by the comparison test.Hence the series $\displaystyle \sum_{n=1}^{\infty} f_n(x)$ converges absolutely. This completes the first half of the proof.

        Now, let $f(x) = \displaystyle \sum_{n=1}^{\infty} f_n(x)$. Then for all $x$ in $A$ we have
        \begin{align*}
                |f(x) - [f_1(x)+\cdots+f_n(x)]| &= \left| \sum_{n=N+1}^{\infty} f_n(x) \right| \\
                                                &\le \sum_{n=N+1}^{\infty} |f_n(x)| \\ 
                                                &\le \sum_{n=N+1}^{\infty} M_n.
        \end{align*}
        Since $\displaystyle \sum_{n=1}^{\infty} M_n$ converges, the number $\displaystyle \sum_{n=N+1}^{\infty} M_n$ can be made as small as desired (by the vanishing condition of convergent series), by choosing $N$ sufficiently large.
\end{proof}
(TODO: Example of everywhere continuous, nowhere differentiable function.)

The Weierstrass M-test is ideal for analysing functions which are very well behaved. We will give special attention to functions of the form 
\[
        f(x) = \sum_{n=0}^{\infty} a_n(x-a)^{n},
\] 
which can also be described by the equation
 \[
         f(x) = \sum_{n=0}^{\infty} f_n(x),
\] 
for $f_n(x) = a_n(x-a)^{n}$. Such an infinite sum of functions whcih depend only on powers of $(x-a)$ is called a \textbf{power series centred at $a$}. For the sake of simplicity, we will usually concentrate on power series centred at 0,
\[
        f(x) = \sum_{n=0}^{\infty} a_nx^{n}.
\] 

One especially important group of power series are those of the form 
\[
        \sum_{n=0}^{\infty} \frac{f^{(n)}(a)}{n!}(x-a)^{n},
\] 
where $f$ is some function which \emph{has derivatives of all orders at $a$}; this series is called the \textbf{Taylor series for $f$ at $a$}. Of course, it is not necessarily true that
\[
        f(x) = \sum_{n=0}^{\infty} \frac{f^{(n)}(a)}{n!}(x-a)^{n};
\] 
this equation holds only when the remainder terms satisfy $\lim_{n \to \infty} R_{n,a}(x) = 0$.

We already know that a power series $\displaystyle \sum_{n=0}^{\infty} a_nx^{n}$ does not necessarily converge for all $x$. For instance, the power series 
\[
x-\frac{x^{3}}{3}+\frac{x^{5}}{5}-\frac{x^{7}}{7}+\cdots
\]
can only converge for $|x|\le 1$, for otherwise the limit of the absolute value of the terms in the sequence $(x^{n+1}/(n+1) : n \in \mathbb{N})$ would not converge to 0. On the other hand, the power series
\[
x-\frac{x^{2}}{2}+\frac{x^3}{3}-\frac{x^{4}}{4}+\frac{x^{5}}{5}- \cdots
\] 
converges only for $-1<x\le 1$, for the same reasons as above; we exclude $-1$ for that would yield the (negative) harmonic series, which diverges. 

It is even possible to produce a power series which converges only for $x=0$. For example, the power series
\[
\sum_{n=0}^{\infty} n!x^{n}
\] 
does not converge for $x\neq 0$ ; indeed, we can apply the ratio test to see that the expressions
\[
        \frac{(n+1)!\left( x^{n+1} \right) }{n!x^{n}} = (n+1)x
\] 
are unbounded for any $x \neq 0$. If, however, a power series $\displaystyle \sum_{n=0}^{\infty} a_nx^{n}$ converges for some $x_0 \neq 0$, then we can say many things about the series $\displaystyle \sum_{n=0}^{\infty} a_nx^{n}$ for $|x|< |x_0|$.

\begin{theorem}{}{}
        Suppose that the series 
        \[
                f(x_0) = \sum_{n=0}^{\infty} a_n{x_0}^{n}
        \] 
        converges, and let $a$ be any number with $0<a<|x_0|$. Then on $[-a,a]$ the series 
         \[
                 f(x) = \sum_{n=0}^{\infty} a_nx^{n}
        \] 
        converges uniformly (and absolutely). Moreover, the same is true for the series
        \[
                g(x) = \sum_{n=1}^{\infty} na_nx^{n-1}.
        \] 
        Finally, $f$ is differentiable and 
        \[
                f'(x) = \sum_{n=1}^{\infty} na_nx^{n-1}
        \] 
        for all $x$ with $|x|<|x_0|$.
\end{theorem}
\begin{proof}
        Since $\displaystyle \sum_{n=0}^{\infty} a_n{x_0}^{n}$ converges, the terms $a_n{x_0}^{n}$ approach 0. Hence they are surely bounded (because all convergent series are bounded): There is some number $M$ such that 
         \[
                 |a_n{x_0}^{n}|=|a_n|\cdot |{x_0}^{n}| \le M \quad \text{for all $n$.}
        \] 
        Let $0<a<|x_0$. Now, if $x$ is in $[-a,a]$, then $|x| \le  |a|$, so
        \begin{align*}
                |a_nx^{n}| &= |a_n|\cdot |x^{n}| \\
                           &\le |a_n|\cdot |a^{n}|\\
                           &= |a_n|\cdot|x_0|^{n} \left|\frac{a}{x_0}\right|^{n} \\
                           &\le M\left|\frac{a}{x_0}\right|^{n}.
        \end{align*}
        But since $x_0>a$, we have $|a/x_0|<1$, so the geometric series 
        \[
        \sum_{n=0}^{\infty} M\left|\frac{a}{x_0}\right|^{n} = M \sum_{n=0}^{\infty} \left|\frac{a}{x_0}\right|^{n}
        \] 
        converges. Choosing $M\cdot |a /x_0|^{n}$ as the number $M_n$ in the Weierstrass M-test, it follows that $\displaystyle \sum_{n=0}^{\infty} |a_nx^{n}|$ converges uniformly, so $\displaystyle \sum_{n=0}^{\infty} a_nx^{n}$ converges uniformly on $[-a,a]$.

        To prove the same assertion for  $g(x) = \displaystyle \sum_{n=1}^{\infty} na_nx^{n-1}$, suppose that $x$ is in $[-a,a]$; notice that
        \begin{align*}
                |na_nx^{n-1}|&=n|a_n|\cdot |x^{n-1}|\\
                &\le n|a_n|\cdot |a^{n-1}| \\
                &= \frac{|a_n|}{|a|}\cdot |x_0|^{n}n\left|\frac{a}{x_0}\right|^{n} \\
                &\le \frac{M}{|a|}n\left|\frac{a}{x_0}\right|^{n}.
        \end{align*}
        One again, $|a /x_0|<1$, so the series
        \[
                \sum_{n=1}^{\infty} \frac{M}{|a|}n\left|\frac{a}{x_0}\right|^{n} = \frac{M}{|a|}\sum_{n=1}^{\infty} n\left|\frac{a}{x_0}\right|^{n}
        \] 
        converges, in fact this was proven before as a straightforward application of the ratio test. Applying the Weierstrass M-test once again proves that $\sum_{n=1}^{\infty} na_nx^{n-1}$converges uniformly on $[-a,a]$.

        Finally, since  $na_nx^{n-1}$ is continuous for all $n$, we have that the function  
        \[
                g(x) = \sum_{n=1}^{\infty} na_nx^{n-1}
        \] 
        is continuous (by the corollary); and by the same corollary, we conclude that
        \[
                f'(x) = g(x) = \sum_{n=1}^{\infty} na_nx^{n-1} \quad \text{for all $x $ in $[-a,a]$.}
        \] 
        Since we could have chosen any number $a$ with $0<a<|x_0|$, this result holds true for all  $x$ with $|x|<|x_0|$.
\end{proof}

We can now manipulate power series with relative ease. Most algebraic manipulations are fairly straightforward consequences of general theorems about infinite series. For example, suppose that $f(x) = \displaystyle \sum_{n=0}^{\infty} a_nx^{n}$ and $g(x) = \displaystyle \sum_{n=0}^{\infty} b_nx^{n}$, where the two power series both converge for some $x_0$. Then for $|x| < |x_0|$ we have
 \[
         \sum_{n=0}^{\infty} a_nx^{n}+\sum_{n=0}^{\infty} b_nx^{n} = \sum_{n=0}^{\infty} (a_nx^{n}+b_nx^{n})= \sum_{n=0}^{\infty} (a_n+b_n)x^{n}.
\] 
So the series $h(x) = \displaystyle \sum_{n=0}^{\infty} (a_n+b_n)x^{n}$ also converges for $|x|< |x_0|$, and $h=f+g$  for these $x$.

We can also treat products. If $|x|< |x_0|$, then we know that the series $\displaystyle \sum_{n=0}^{\infty} a_nx^{n}$ and $\displaystyle \sum_{n=0}^{\infty} b_nx^{n}$ converge absolutely, by the last theorem. It then follows from theorems on series that the product $\displaystyle \sum_{n=0}^{\infty} a_nx^{n}\cdot \sum_{n=0}^{\infty} b_nx^{n}$ is given by 
\[
\sum_{i=0}^{\infty} \sum_{j=0}^{\infty} a_ix^{i}g_jx^{j},
\] 
where the elements $a_ix^{i}g_jx^{j}$ are arranged in any order. In particular we can choose the arrangement 
\[
        a_0b_0 + (a_0b_1+a_1b_0)x + (a_0b_2+a_1b_1+a_2b_0)x^2 + \cdots
\] 
which can be written as 
\[
\sum_{n=0}^{\infty} c_nx^{n} \qquad \text{for } c_n = \sum_{k=1}^{n} a_kb_{n-k}.
\] 
This is the ``Cauchy Product'' (see problem 23-10). 
