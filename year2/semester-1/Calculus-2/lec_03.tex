\lecture{3}{Mon 01 Mar 18:17}{Infinite sequences}

We begin with a formalised definition of a rather familiar concept: that of a sequence. Such a rigorous approach is actually not difficult to formulate, indeed, it heavily depends on the concept of function.

\begin{definition}{}{}
        An \textbf{infinite sequence} of real numbers is a function $a$ whose domain is $\mathbb{N}$.
        \begin{align*}
                a &: \mathbb N \to \mathbb R ,\\
                  &: n \mapsto a(n) 
        \end{align*}
\end{definition}

As far as this definition is concerned, a sequence should be designated by a single letter like $a$, and its particular values by $a(1), a(2), a(3), \ldots$, but a subscript notation is almost always used instead ($a_1,a_2,a_3,\ldots$). Moreover, the sequence itself is usually denoted by a symbol like $(a_n)$. Thus $(n), \left( (-1)^{n} \right),$ and $(1 /n)$ denote the sequences defined by:
\begin{align*}
        \alpha_n &= n, \\
        \beta_n &= \left( -1 \right)^{n}, \\
        \gamma_n &= \frac{1}{n}.
\end{align*}

A sequence, like any other function may be graphed on a Cartesian plane, however, such a graph is usually unrevealing, since most of the function cannot be fit on the page. A more convenient representation of a sequence is obtained by simply labelling the points $a_1,a_2,a_3,\ldots$ on a line. This sort of picture shows where the sequence ``is going.''
\begin{figure}[ht]
    \centering
    \incfig[0.5]{sequences}
    \caption{sequences}
    \label{fig:sequences}
\end{figure}

The sequence $(\alpha_n)$ ``goes to infinity,'' the sequence  $(\beta_n)$ ``jumps back and forth between -1 and 1,'' and the sequence  $(\gamma_n)$ ``converges to 0.'' Of the three phrases in quotation marks, the last one is the most crucial of all, and will soon be defined precisely. 
\begin{definition}{}{}
        A sequence $(a_n)$ \textbf{converges to $l$} if for every $\varepsilon > 0$, there is a natural number $N$ such that, for all natural numbers $n$,
        \[
        \text{if } n > N, \text{then } |a_n-l|<\varepsilon.
        \]
        This is denoted by $\displaystyle \lim_{n \to \infty} a_n = l$.
\end{definition}

In addition to the terminology introduced in this definition, we sometimes say that a sequence $(a_n)$ \textbf{approaches} $l$ or has the \textbf{limit}  $l$. A sequence $(a_n)$ is said to \textbf{converge} if such an  $l$ exists, and to \textbf{diverge} if it does not converge.

To show that the sequence $(\gamma_n)$ converges to 0, it suffices to observe the following: If $\varepsilon>0$, there is a natural number $N$ such that $1 /N < \varepsilon$. Then, if $n>N$ we have:
\[
\gamma_n = \frac{1}{n} <\frac{1}{N}<\varepsilon, \quad \text{so } |\gamma_n - 0|<\varepsilon.
\]
\begin{theorem}{}{}
        If $(a_n)$ is a convergent sequence, then it converges to a unique value.
\end{theorem}
\begin{proof}
        Aiming for a contradiction, suppose that
         \[
        \lim_{n \to \infty} a_n = l \text{ and } \lim_{n \to \infty} a_n = m.
        \] 
        Then there exist numbers $N, P \in \mathbb N$ for which, given any $\varepsilon>0$,
        \begin{align*}
                n > N &\implies |a_n-l| < \varepsilon, \\
                n > P &\implies |a_n-m| < \varepsilon.
        \end{align*}

        Let $\lambda = \max\{N, P\}$, then we have
        \[
        n>\lambda \implies |a_n-l|<\varepsilon \text{ and } |a_n-m| < \varepsilon.
        \] 
        Without loss of generality, suppose $m>l$, then the above inequalities may be taken to mean that
         \[
        a_n< \varepsilon+l \text{ and } m-\varepsilon<a_n.
        \] 
        We now proceed to find some value of $\varepsilon$ for which $\varepsilon+l\le m-\varepsilon$. In order that such contition hold, we require that $0 < \varepsilon \le (m - l)/2$. With such a value of $\varepsilon$ we get
        \begin{align*}
                a_n &< \varepsilon + l \\
                    &\le \frac{m-l}{2} +l \\
                    &= \frac{m+2}{2} = \frac{2m-m+l}{2} = m - \frac{m-l}{2} \le m -\varepsilon.
        \end{align*}
        In other words, $a_n< \varepsilon+l < m-\varepsilon < a_n$, which is absurd. Therefore, our assumption must have been false and there cannot be two different limits for the sequence.
\end{proof}

There are some facts about limits of sequences which are analogous to those found about limits of functions, and which, by now, should be clear-cut:

If $\displaystyle \lim_{n \to \infty} a_n$ and $\displaystyle \lim_{n \to \infty} b_n$ both exist, then
\begin{align*}
        \lim_{n \to \infty} (a_n+b_n) &= \lim_{n \to \infty} a_n + \lim_{n \to \infty}, \\
        \lim_{n \to \infty} (a_n\cdot b_n) &= \lim_{n \to \infty} a_n \cdot \lim_{n \to \infty} b_n; 
\end{align*}

moreover, if $\displaystyle \lim_{n \to \infty} b_n \neq 0$ then $b_n \neq 0$ for all n greater than some $N$, and
\[
\lim_{n \to \infty} a_n /b_n = \lim_{n \to \infty} a_n /\lim_{n \to \infty} b_n.
\]
(TODO: Prove these facts recalling that the definition of $\lim_{n \to \infty} a_n=l$ is very similar to the definition of $\lim_{x \to \infty} f(x) = l$.)

The similarities between the definitions of $\lim_{n \to \infty} a_n = l$ and $\lim_{x \to \infty} f(x) =l$ are actually closer than mere analogy; it is indeed possible to define the first in terms of the latter. If $f$ is the function whose graph consists of line segments joining the points in the graph of a sequence $(a_n) $, so that
\begin{figure}[ht]
    \centering
    \incfig[0.7]{function-of-line-segments}
    \caption{function of line segments}
    \label{fig:function-of-line-segments}
\end{figure}
\[
        f(x) = a_n + (a_{n+1}-a_n)(x-n), \quad n\le x\le n+1,
\]
then
 \[
         \lim_{n \to \infty} a_n = l \text{ if and only if } \lim_{x \to \infty} f(x) =l.
\] 
Conversely, if $f$ satisfies $\lim_{x \to \infty} f(x) = l$, and we set $a_n = f(n)$, then $\lim_{n \to \infty} a_n = l$.
\begin{theorem}{}{}
        Let $f$ be a function defined in an open interval containing $c$, except perhaps at $c$ itself, with
        \[
                \lim_{x \to c} f(x) = l.
        \] 
        Suppose that $(a_n)$ is a sequence such that
        \begin{align*}
                & \text{each $a_n$ is in the domain of $f$,} \\
                & \text{each $a_n\neq c$,} \\
                & \lim_{n \to \infty} a_n = c.
        \end{align*}
        Then the sequence $\left( f(a_n) \right) $ satisfies
        \[
                \lim_{n \to \infty} f(a_n) = l.
        \] 
        Conversely, if this is true for every sequence $(a_n)$ satisfying the above conditions, then $\lim_{x \to c} f(x) =l$.
\end{theorem}
\begin{proof}
        Suppose first that $\lim_{x \to c} f(x) = l$. Then for every $\varepsilon>0$, there is a $\delta>0$ such that, for all x,
        \[
                0< |x-c|< \delta \implies |f(x)-l|< \varepsilon.
        \]
        Now, if the sequence $(a_n)$ satisfies $\lim_{n \to \infty} a_n=c$, then in particular there is a natural number $N$ such that
        \[
        n>N \implies |a_n - c|< \delta.
        \] 
        Our choice of delta immediately implies that $|f(a_n)-l|< \varepsilon$, as required to show that
         \[
                 \lim_{n \to \infty} f(a_n)=l.
        \] 

        Conversely, suppose that $\lim_{n \to \infty} f(a_n) = l$ for every sequence $(a_n)$ satisfying $\lim_{n \to \infty} a_n = c$. Striving for a contradiction, suppose that $\lim_{x \to c} f(x)=l$ is not true. Then there is an $\varepsilon>0$ such that for \emph{every} $\delta>0$ there is an $x$ with 
        \[
                0< |x-c|<\delta \text{ but } |f(x)-l| > \varepsilon.
        \] 
        In particular, there must exist a number $x_n$ such that, given any natural number $n$,
        \[
                0< |x_n-c|< \frac{1}{n} \text{ but } |f(x_n)-l| >\varepsilon.
        \] 
        Now, $(x_n)$ can return values as small as desired for sufficiently large $n \in \mathbb N$, so it turns out it converges to $c$ by the first inequality above. However, since  $|f(x_n)-l| > \varepsilon$ for all possible values of $n$, the sequence $(f(x_n))$ does not converge to  $l$. Hence there \emph{is} at least a sequence $(x_n)$ converging to $c$ for which $\lim_{n \to \infty} f(x_n) \neq l$, which is a contradiction to our initial hypothesis. The result then follows.
\end{proof}

This theorem provides many examples of convergent sequences. For example, the sequences $(a_n)$ and $(b_n)$ defined by 
\begin{align*}
        a_n &= \sin\left( 13 + \frac{1}{n^2} \right)  \\
        b_n &= \cos\left( \sin\left( 1+(-1)^{n}\cdot \frac{1}{n} \right)  \right),
\end{align*}
clearly converge to $\sin(13)$ and $ \cos\sin(1)$, respectively. It is important, however, to have some criteria guaranteeing convergence of sequences that are not obviously of this sort. There is one important criterion which is fairly easy to prove, but is fundamental to build up the other results. It is defined in terms that are not unfamiliar to us at this point, namely: A sequence $(a_n)$ is said to be \textbf{increasing} if  $a_{n+1}>a_n$ for all $n$, \textbf{non-decreasing} if $a_{n+1}\ge a_n$, for all $n$, and \textbf{bounded above} if there is a number $M$ such that $a_n \le  M$ for all $n$; decreasing, non-decreasing, and bounded below are defined on a similar fashion.
\begin{theorem}{}{}
        If $(a_n)$ is non-decreasing and bounded above, then $(a_n)$ converges (a similar statement is true if $(a_n)$ is non-increasing and bounded bellow.
\end{theorem}
\begin{proof}
        By our hypotheses, there exists a least upper bound $\alpha$ on the set of images of the sequence $(a_n)$, which certainly satisfies
        \[
        a_n \le \alpha, \quad \text{for all $n \in \mathbb{N}$}.
        \] 
        We claim that $\lim_{n \to \infty} a_n=\alpha$, for which we need resort to the definition. Let $\varepsilon > 0$, then by the Real Plus Epsilon theorem, we have that for any such value of  $\varepsilon$, $\alpha-\varepsilon < \alpha$, which implies that the value to the left of that inequality cannot be a least upper bound for the sequence $(a_n)$. Then there is some number $a_N$ for which $a_N > \alpha - \varepsilon$. 

        Now since  $(a_n)$ is non-decreasing, we have that, for all  $n>N$,
        \[
        a_n \ge a_N > \alpha - \varepsilon.
        \] 
        But by definition of $\alpha$ and by Real Plus Epsilon, $\alpha - \varepsilon < a_n \le \alpha < \alpha + \varepsilon$, so 
        \[
        |a_n-\alpha|<\varepsilon,
        \] 
        proving the statement.
\end{proof}

Note that the hypothesis that $(a_n)$ is bounded above is essential in the above theorem, for if such a condition does not hold, then $(a_n)$ clearly diverges. At first glance, deciding whether a given non-decreasing sequence is bounded above seems pretty straightforward; however, and as we shall see, such a task is hardly trivial. Take, for instance, the classical example of the Harmonic Series:
\[
1, 1+\frac{1}{2}, 1+\frac{1}{2}+\frac{1}{3}, 1+\frac{1}{2}+\frac{1}{3}+\frac{1}{4}, \ldots.
\] 
Even though the previous theorem seems to treat only a very special kind of sequences, it is more useful than one might expect at first, because it is always possible to extract from an arbitrary sequence $(a_n)$ another sequence which is either non-increasing or non-decreasing. To be precise, let us define a \textbf{subsequence} of the sequence  $(a_n)$ to be a sequence of the form
\[
        a_{n_1},a_{n_2},a_{n_3},\ldots,
\] 
where the $n_j$ are natural numbers with
\[
n_1<n_2<n_3< \cdots.
\]
\begin{lemma}{}{}
        Any sequence $(a_n)$ contains a subsequence which is either non-decreasing or non-increasing.
\end{lemma}
\begin{proof}
        Call a natural number $n$ a ``peak point'' of the sequence $(a_n)$ if $a_m < a_n$, for all  $m>n$. There will be two cases.

        \textit{Case 1. The sequence has infinitely many peak points.} In this case, if  $n_1<n_2<n_3<\cdots$ are the peak points, then, by definition, it must be the case that  $a_{n_1}>a_{n_2}>a_{n_3}> \cdots$, so $(a_{n_k})$ is the desired (non-increasing) subsequence.

        \textit{Case 2. The sequence has only finitely many peak points.} In this case, let $n_1$ be any natural number which is greater than all peak points. Since $n_1$ is not a peak point, there must be some  $n_2>n_1$ such that $a_{n_2}\ge a_{n_1}$ (simply by negation of the definition.) Now, since $n_2$ is not a peak point, (it is greater than all peak points) there is some $n_3>n_2$ such that $a_{n_3}\ge a_{n_2}$. Continuing in this way gives the desired (non-decreasing) subsequence.
\end{proof}

This lemma allows us to pick up an extra corollary along the way.
\begin{corollary}{The Bolzano---Weierstrass Theorem}{}
        Every bounded sequence has a convergent subsequence.
\end{corollary}
\begin{proof}
        Obviously, any subsequence of a bounded sequence is also bounded, and since we can construct a non-decreasing or non-increasing subsequence, the third theorem of this section allows us to conclude the result.
\end{proof}

Without some additional assumptions, this is as fas as we can go: It is easy to construct sequences having many, even infinitely many, sub-sequences converging to different numbers (see problem 3). There is, however, a reasonable assumption to add, which yields a necessary and a sufficient condition for convergence of any sequence; and even though this assumption is not crucial for our work, it does simplify many proofs, and is fundamental in more advanced investigations.

If a sequence converges, so that the individual terms are eventually all close to the same number, then the difference between any of such two individual terms should become small. More precisely, if $\lim_{n \to \infty} a_n=l$ for some $l$, then for any $\varepsilon>0$there is an $N$ such that $|a_n-l|<\varepsilon /2$ whenever  $n>N$. Now, if both  $n>N$ and $m>N$, then
\[
        |a_n-a_m| = |(a_n-l)+(l-a_m)| \le |a_n-l|+|l-a_m| <\frac{\varepsilon}{2}+\frac{\varepsilon}{2} = \varepsilon.
\]
This final inequality, which disposes of the limit $l$, can be used to formulate a condition (the Cauchy condition) which is certainly necessary for the convergence of a sequence.

\begin{definition}{}{}
        A sequence $(a_n)$ is a \textbf{Cauchy sequence} if for every $\varepsilon>0$ there is a natural number $N$ such that, for all $m$ and $n$,
        \[
        \text{if } m,n>N, \text{ then } |a_n-a_m|<\varepsilon.
        \] 
        (This is usually written $\displaystyle \lim_{m,n \to \infty} |a_n-a_m| = 0.$)
\end{definition}
The beauty of the Cauchy condition is that it is also sufficient to ensure convergence.
\begin{theorem}{}{}
        A sequence $(a_n)$ converges if and only if it is a Cauchy sequence.
\end{theorem}
\begin{proof} 
        We have already shown that if a sequence converges, then it is a Cauchy sequence. Thus we need only prove that every Cauchy sequence converges. In order to do this, we must first show that every Cauchy sequence $(a_n)$ is bounded.

        Take $\varepsilon=1$ in the definition of a Cauchy sequence. We find that there is some $N  \in \mathbb{N}$ such that
        \[
        |a_m-a_n|<1 \quad \text{for all $m,n>N$}.
        \] 
        In particular, this means that
        \[
        |a_m-a_{N+1}|<1 \quad \text{for all $m>N$}.
        \]
        Thus the sequence $(a_m:m>N)$ is bounded. Since there are only finitely many $a_j$'s in the previous parts of the sequence, the whole sequence is bounded. 

        The Bolzano---Weierstrass theorem thus implies that some subsequence of the Cauchy sequence $(a_n)$ converges. Now there is only one point left: If a subsequence of a Cauchy sequence converges, then the Cauchy sequence itself converges. For this part, suppose there is a sequence $(a_{n_k})$ of the Cauchy sequence $(a_n)$; the former converges to some value $l$, we calim that $(a_n)$ also converges to $l$. We now know the definitions of both convergence and the Cauchy condition, so we may let $N \in \mathbb N$ be such that, for all $n_k,n > N$,
        \[
                |a_{n_k} - l|<\frac{\varepsilon}{2} \text{ and } |a_n - a_{n_k}|<\frac{\varepsilon}{2}.
        \]
        Then we have
        \[
                |a_n - l| = |(a_n - a_{n_k}) + (a_{n_k} - l)| \le |a_n - a_{n_k}| + |a_{n_k} - l| < \varepsilon
        \]
        which means that the sequence $(a_n)$ also converges to $l$, as required.
\end{proof}
