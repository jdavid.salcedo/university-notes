\selectlanguage *[variant=british]{english}
\contentsline {section}{\nonumberline Lecture 1: The trigonometric functions}{2}{section*.3}%
\contentsline {section}{\nonumberline Addendum}{10}{section*.27}%
\contentsline {section}{\nonumberline Lecture 2: The logarithm and exponential functions}{11}{section*.29}%
\contentsline {section}{\nonumberline Lecture 3: Infinite sequences}{17}{section*.50}%
\contentsline {section}{\nonumberline Lecture 4: Infinite Series}{21}{section*.63}%
\contentsline {section}{\nonumberline Lecture 5: Uniform Convergence and Power Series}{34}{section*.87}%
\contentsline {section}{\nonumberline Lecture 6: Approximation by Polynomial Functions}{44}{section*.113}%
\providecommand \tocbasic@end@toc@file {}\tocbasic@end@toc@file 
