\lecture{1}{Thu 04 Feb 12:21}{The trigonometric functions}      

The definitions of the functions sin and cos are considerably subtle and thus require some preparation; which is to say that a formal introduction to them require that some intuition be previously known. 

To begin with, note that in elementary geometry an angle is understood as the union of two half-lines with a common initial point. Such a definition is nevertheless less useful for trigonometry than the concept of ``directed angles,'' which may be regarded as pairs $\left( l_1,l_2 \right) $ of half-lines with the same initial point, visualized as follows:
\begin{figure}[ht]
    \centering
    \incfig[0.8]{some-angles}
    \caption{some angles}
    \label{fig:some-angles}
\end{figure}

Notice that  the angles always go from $l_1$ to $l_2$; also, we may well fix $l_1$ to be the positive half of the horizontal axis, so that an angle is always described by the second half-line (as in the unit circle).
\begin{figure}[ht]
    \centering
    \incfig[0.3]{unit-circle}
    \caption{unit circle}
    \label{fig:unit-circle}
\end{figure}
Of course, since each half-line intersects the unit circle precisely once, we can indeed describe an angle by the sole virtue of a point on the unit circle, a point which will be of the form $\left( x,y \right) $, where $x^2 + y^2 = 1$. 

The above construction gives rise to the definition of the sine and the cosine of a given directed angle: since a directed angle is determined by a point $\left( x,y \right) $, with $x^2+y^2=1$, then the sine of such an angle is defined as $y$ and the cosine as $x$.
\begin{figure}[ht]
    \centering
    \incfig[0.3]{sin-and-cos}
    \caption{sin and cos}
    \label{fig:sin-and-cos}
\end{figure}

This intuitive definition is not yet precise enough, for the sine and cosine functions must tie in with the concept of \emph{number}, rather than with that of angle. It may then sound natural to assign an angle to every number, something that can be done by measuring angles in degrees (i.e., using the common convention that a complete ``rotation'' comprises $360^{\circ}$). One can now define a function, which we shall denote by $\sin^{\circ}$, as follows:
\[
        \sin^{\circ}(x)  = \text{sine of the angle of $x$ degrees}.
\]

However, the convention of $360^{\circ}$ is evidently arbitrary, and it is unlikely that this system will lead to elegant results. Moreover, it is unclear what could be meant, for instance, by an angle of $ \sqrt{2} $ degrees, a difficulty that may be circumvented.

Now, the \emph{radian measure}, offers a remedy for these defects. Given any number $x$, choose a point $P$ on the unit circle such that $x$ is the length of the arc of the circle beginning at $\left( 1,0 \right) $ and running counter-clockwise to $P$. The directed angle determined by $P$ is called ``the angle of $x$ radians.'' Since the length of the whole circle is $2\pi$, the angle of $x$ radians and the angle of $2\pi + x$ radians are identical. A function $ \sin^{r}$ can be now defined:
\[
\sin ^{r} (x) = \text{sine of the angle of $x$ radians};
\]
which in turn implies that 
\[
\sin ^{\circ}x = \sin ^{r} \frac{2\pi x}{360} = \sin ^{r} \frac{\pi x}{180}
.\] 
Henceforth we shall only use the function $\sin ^{r}$, reason for which the superscript will not be written. 

It is easy to intuitively reformulate the definition of radian in terms of areas (see problem 28 for a treatment in terms of lengths). Suppose that $x$ is the length of the arc of the unit circle from $\left( 1,0 \right) $ to $P$; this arc thus contains $x/2\pi$ of the total length $2\pi$ of the circumference of the unit circle. Let $S$ denote the ``sector'' (i.e., area) shown in the figure. Note that $S$ is bounded by the unit circle, the positive half of the horizontal axis, and the half-line though $\left( 0,0 \right) $ and $P$. The area of $S$ is thus $x/2\pi$ times the area inside the whole unit circle, which we expect to be  $\pi$; hence the area:
\[
\frac{x}{2\pi}\cdot\pi = \frac{x}{2}
.\] 
We can therefore define $\cos x$ and $\sin x$ as the coordinates of the point which determines a sector of area $x/2$.
\begin{figure}[ht]
    \centering
    \incfig[0.5]{length-and-area}
    \caption{length and area}
    \label{fig:length-and-area}
\end{figure}

With these remarks in mind, we proceed with the rigorous definition of the functions sine and cosine. To begin with, we identify  $\pi $  as the area of the unit circle, or more precisely, as twice the area of the semicircle:
\begin{definition}{$\pi$}{}
       For the purposes of the constructions to come, we agree to the fact that
        \[
               \pi = 2 \cdot \underbrace{\int_{-1}^{1} \sqrt{1 - x^2} dx}_\text{area of the semicircle} 
       .\]          
\end{definition}

This definition is introduced in order to first define sine and cosine only for $x \in [0,\pi]$.

The second definition is intended to describe, for $x \in [-1,1]$, the area $A(x)$ of the sector bounded by the unit circle, the horizontal axis, and the half line through $\left( x,\sqrt{1-x^2}  \right) $. which can be expressed as:
\[
        \underbrace{\frac{x\sqrt{1-x^2}}{2}}_\text{area of a triangle} + \underbrace{\int_x^{1}\sqrt{1-t^2}dt}_\text{area of a region under the circle}
.\]
In fact, this same formula holds for $x\in [-1,0]$. In such a case, note that the term 
\[
\frac{x\sqrt{1-x^2} }{2}
\]    
is negative.

Now we may regard the following:
\begin{definition}{The $A$ function}{}
        If $-1\le x\le 1$, then 
        \[
                A(x) = \frac{x\sqrt{1-x^2} }{2} + \int_x^{1}\sqrt{1-t^2} dt
        .\]
\end{definition}

Note that if $x \in (-1,1)$ then A is differentiable at $x$ and, by the fundamental theorem of calculus,
\begin{align*}
        A'(x) &= \frac{1}{2}\left( x \cdot -\frac{2x}{2\sqrt{1-x^2} }+\sqrt{1-x^2}\right) - \sqrt{1-x^2} \\
              &= \frac{1}{2}\left( \frac{-x^2+\left( 1-x^2 \right) }{\sqrt{1-x^2} } \right) -\sqrt{1-x^2} \\
              &= \frac{1-2x^2}{2\sqrt{1-x^2} }-\sqrt{1-x^2}  \\
              &= \frac{1-2x^2-2\left( 1-x^2 \right) }{2\sqrt{1-x^2} } \\
              &= -\frac{1}{2\sqrt{1-x^2} }.
\end{align*}

Notice that on the interval $[-1,1]$ the function $A$ decreases (because its derivative is negative on the interior of such an interval) from 
\[
A(-1) = 0 + \int_{-1}^{1}\sqrt{1-x^2} dx = \frac{\pi}{2}
\]
to $A(1) = 0$. (This follows directly from our definition of pi.)
\begin{figure}[ht]
    \centering
    \incfig[0.3]{function-a}
    \caption{function A}
    \label{fig:function-a}
\end{figure}

For $x \in [0,\pi]$ we wish to define cos and sin as the coordinates of a point $P=(\cos x,\sin x)$ on the unit circle which determines a sector whose area is exactly $x / 2$.

In order to introduce the following definition, we need know that for all $x \in [0,\pi]$ there exists some number $y$ for which $A(y) = x / 2$; to justify this fact, notice that $A$ is continuous, and since it takes on the values 0 and $\pi / 2$, the intermediate value theorem immediately leads to the result.
\begin{definition}{Sine and cosine functions on $[0,\pi]$}{}
        If $0\le x\le \pi$ then $\cos x$ is the unique number in $[-1,1]$ such that
         \[
                 A(\cos x) = \frac{x}{2}
        .\] 
        and 
        \[
        \sin x = \sqrt{1 -\cos^2 x} 
        .\] 
\end{definition}
From this point, we proceed with some results:
\begin{theorem}{Derivatives of the sine and cosine functions}{}
        If $0<x<\pi$ then
        \begin{align*}
                \cos'(x) &= -\sin x \\
                \sin'(x) &= \cos x.              
        \end{align*}
\end{theorem}
\begin{proof}
        Let us define a function such that $B = 2A$, then the definition $A(\cos x) = x / 2$ can be written as $B(\cos x) = x$; this just means that cos is the inverse of $B$.

        Now, notice that $B'(x) = 2A'(x)$, for $x$ satisfying the given conditions. Hence 
        \[
                B'(x) = -\frac{1}{\sqrt{1-x^2} },
        \] 
        which in turn allows us to compute the following:
        \begin{align*}
                \cos' (x) &= \left( B^{-1} \right)'(x) \\
                          &= \frac{1}{B'\left( B^{-1}(x) \right) } \\
                          &= \frac{1}{B'\left( \cos x \right) } \\
                          &= -\sqrt{1-\cos^2 x} \\
                          &:= -\sin x.
        \end{align*}
        (Where the last equality follows directly from the definition of sine.) 
        Thus it is easy to see that
        \begin{align*}
                \sin' (x) &= \frac{1}{2} \cdot \frac{-2 \cos x \cdot \cos' x}{\sqrt{1 - \cos^{2}x} } \\
                &= \frac{\sin x \cos x}{\sin x} \\
                & = \cos x.
        \end{align*}
        This completes the proof.
\end{proof}

By recalling that the sine and cosine functions represent the coordinates of a point on the upper half of the unit circle, we may sketch the graphs of such functions on the interval for which they have been defined thus far, i.e., $[0,\pi]$.

Since $\cos' (x) = -\sin x < 0$, whenever $x \in [0,\pi]$, the function cos decreases from $\cos 0 = 1$ to $\cos \pi = -1$. Thus there exists a unique $y \in [0,\pi]$ such that $ \cos y = 0$. To find such a number, we note that, by definition,
\[
        A(\cos x) = \frac{x}{2},
\]
this implies that 
 \[
         A(0) = \frac{y}{2} \implies y = 2 \int_0^{1} \sqrt{1-t^2} dt 
.\]
The fact that $f(x) = \sqrt{1-x^2} $ is an even function makes it easy to determine that 
 \[
         \int_{-1}^{0} \sqrt{1-t^2} dt = \int_{0}^{1} \sqrt{1-t^2} dt, 
\]
and so
\[
        y = \int_{-1}^{1} \sqrt{1-t^2} dt = \frac{\pi}{2}
.\] 
\begin{figure}[ht]
    \centering
    \incfig[0.3]{cosine}
    \caption{cosine}
    \label{fig:cosine}
\end{figure}

On the other hand, for sine we see:
\[
        \sin' (x) = \cos x \begin{cases}
        > 0, & 0 < x < \pi/2 \\
        < 0, & \pi/2 < x < \pi.                
        \end{cases}
\]
Thus sine increases on $[0,\pi/2]$, from $\sin 0 = 0$ to $ \sin \pi /2 = 1$; and decreases on $[\pi/2, \pi]$, to $\sin \pi = 0$.
\begin{figure}[ht]
    \centering
    \incfig[0.3]{sine}
    \caption{sine}
    \label{fig:sine}
\end{figure}

At this point, we wish to define sine and cosine for values not in $[0,\pi]$, something that can be done by a two-step piecing together process:
\begin{enumerate}
        \item If $ \pi \le x \le 2\pi$, then
                \begin{gather*}
                        \sin x = -\sin(2\pi-x),\\
                        \cos x = \cos(2\pi-x).
                \end{gather*}
                (This ``extension'' makes sense when considering the desired form of the graphs, and its relation with the unit circle.)
        \item If $x = 2\pi k + x'$ for some integer $k$ and some $x'\in [0,2\pi]$, then
                \begin{gather*}
                        \sin x = \sin x', \\
                        \cos x = \cos x'.
                \end{gather*}
                (This assigns a periodic nature to the trigonometric functions.)
\end{enumerate}
\begin{figure}[ht]
    \centering
    \incfig[0.8]{sine-extended-to-r}
    \caption{sine extended to $\mathbb R$}
    \label{fig:sine-extended-to-r}
\end{figure}
\begin{figure}[ht]
    \centering
    \incfig[0.8]{cosine-extended-to-r}
    \caption{cosine extended to $\mathbb R$}
    \label{fig:cosine-extended-to-r}
\end{figure}
Now that we have extended these functions to $\mathbb R$, we must prove that their basic properties continue to hold. For instance, it is not difficult to see that
\[
        \sin^2 x + \cos^2 x = 1,
\]
for all $x \in  \mathbb R$. Also, we can prove that
\begin{gather*}
        \sin' (x) = \cos x,\\
        \cos' (x) = -\sin x,
\end{gather*}
if $x$ is not a multiple of $ \pi$. For example, if $ \pi < x < 2\pi$, then
 \[
         \sin x = -\sin(2\pi - x),
 \]
 which implies that (recall, $\pi > 2\pi - x > 0$)
\begin{align*}
        \sin'(x) &= -\sin'(2\pi - x) \cdot (-1)\\
                 &= \cos (2\pi - x) \\
                 &= \cos x 
.\end{align*}
In order to prove the same formulas when $x$ is a multiple of $ \pi$, we resort to a trick which comes down to applying a theorem about derivatives\footnote{Namely: Suppose that $f$ is continuous at $a$, and that  $f'(x)$ exists for all  $x$ in some interval containing  $a$, except perhaps for  $x = a$. Suppose that $\lim_{x \to a} f'(x)$ exists. Then $f'(a)$ also exists, and 
        \[
                f'(a) = \lim_{x \to a} f'(x).
\] }.

The other standard trigonometric functions are defined as follows:
\[
\left.\begin{aligned}
        & \sec x = \displaystyle \frac{1}{\cos x} \\
        & \tan x = \displaystyle \frac{\sin x}{\cos x}
\end{aligned}\right\}
x \neq k\pi + \frac{\pi}{2}
\]
\[
        \left.\begin{aligned}
                & \csc x = \frac{1}{\sin x} \\
                & \cot x = \frac{\cos x}{\sin x}
\end{aligned}\right\}
x \neq k\pi
.\] 
\begin{theorem}{}{}
        If $x \neq k\pi+\pi /2$, then 
        \begin{gather*}
                \sec'(x) = \sec x \tan x,\\
                \tan' (x) = \sec^2 x.
        \end{gather*}
        If $x \neq k\pi$, then 
        \begin{gather*}
                \csc' (x) = -\csc x \cot x,\\
                \cot' (x) = -\csc^2 x.
        \end{gather*}
\end{theorem}
\begin{proof}
        Left as an exercise (a straightforward computation).       
\end{proof}

The inverses of the three main trigonometric functions can also be easily differentiated. Firstly, note that the aforementioned functions are not one-to-one, hence we must restrict them to some interval in which they are. The largest possible length for these suitable intervals is $\pi$, and these intervals are usually
\begin{align*}
        [-\pi /2, \pi /2] &\qquad \text{for sin}\\
        [0, \pi] &\qquad \text{for cos}\\
        (-\pi /2, \pi /2) &\qquad \text{for tan}
\end{align*}

The inverse of the function $f(x) = \sin x,\; -\pi /2 \le x \le \pi /2$ is denoted by \textbf{arcsin}; obviously, the domain of arcsin is $[-1, 1]$. 

The inverse of the function $g(x) = \cos x,\; 0\le x\le \pi$ is denoted my \textbf{arccos}; the domain of arccos is also $[-1, 1]$.

The inverse of the function $h(x) = \tan x,\; -\pi /2 < x < \pi /2$ is denoted by \textbf{arctan}, a function whose domain turns out to be all of $\mathbb{R}$.

The derivatives of these inverse functions are surprisingly simple, and do not involve trigonometric functions. However, to find them it is first necessary to simplify expressions like $\cos(\arcsin x)$ or $\sec(\arctan x)$.

\begin{figure}[ht]
    \centering
    \incfig[0.1]{intuition}
    \caption{intuition}
    \label{fig:intuition}
\end{figure}
A little intuition is always helpful. For instance, suppose we have a directed angle whose sine is $x$ ---such an angle is thus $ \arcsin x$ radians; then $ \cos (\arcsin x)$ is just the length of the other side, namely, $ \sqrt{1-x^2}$. Keep in mind, however that the following proof should not resort to such pictures.

\begin{theorem}{Derivatives of the inverse trigonometric functions}{}
        If $-1<x<1$, then
        \begin{align*}
                \arcsin'(x)&=\frac{1}{\sqrt{1-x^2} }\\
                \arccos'(x)&=-\frac{1}{\sqrt{1-x^2} }.
        \end{align*}
        Moreover, for all $x$ we have
        \[
                \arctan'(x) = \frac{1}{1+x^2}
        .\] 
\end{theorem}
\begin{proof}
        \begin{align*}
                \arcsin'(x) &= \left( f^{-1} \right)'(x)  \\
                            &= \frac{1}{f'\left( f^{-1}(x) \right) } \\
                            &= \frac{1}{\sin'(\arcsin x)} \\
                            &= \frac{1}{\cos(\arcsin x)}
        .\end{align*}
        Now,
        \[
                \left( \sin(\arcsin x) \right)^2 + \left( \cos(\arcsin x) \right)^2 = 1,
        \]
        that is,
        \[
                x^2 + \left( \cos(\arcsin x) \right)^2 =1;
        \] 
        therefore $\cos(\arcsin x) = \sqrt{1-x^2}$. (Note that we use the positive square root because $\arcsin x$ is in $(-\pi /2, \pi /2)$, and so $\cos(\arcsin x) > 0$.) This proves the first formula.

        Similarly, we have
        \begin{align*}
                \arccos'(x) &= \frac{1}{g'\left( g^{-1}(x) \right) }\\
                            &= \frac{1}{\cos'(\arccos x)} \\
                            &= -\frac{1}{\sin(\arccos x)} \\
                            &= -\frac{1}{\sqrt{1-x^2} }
        .\end{align*}

        By the exact same process, we can arrive at
        \[
                \arctan'(x) = \frac{1}{\tan'(\arctan x)} = \frac{1}{\sec^2(\arctan x)}
        .\]
        Dividing both sides of the identity $\sin^2 a + \cos^2 a =1$ by $ \cos^2 a$ yields
        \[
        \tan^2 a + 1 = \sec^2 a
        .\]
        It then follows that
        \[
                \sec^2 (\arctan x) = 1 + \tan^2(\arctan x) = 1 +x^2
        .\] 
        This completes the proof.
\end{proof}

Now we may establish some properties of the trigonometric functions, namely, 
\[
\lim_{h \to 0} \frac{\sin h}{h} = 1,
\] 
and the ``addition formula'':
\[
        \sin(x + y) = \sin x \cos y + \cos x \sin y 
.\]

\begin{lemma}{Characterisation of the 0 function}{}
        Suppose that $f$ has a second derivative everywhere and that
        \begin{align*}
                f'' + f &= 0, \\
                f(0) &= 0, \\
                f'(0) &= 0 
        .\end{align*}
        Then $f = 0$.
\end{lemma}
\begin{proof}
        Multiplying both sides of the first equation by $f'$ yields
        \[
        f'f'' + ff' = 0.
        \]
        Thus 
         \[
                 \left( \left( f' \right)^2 + f^2\right)' = 2\left( f'f'' + ff' \right) = 0  
        ,\]
        so $(f')^2 + f^2$ is a constant function. Since $f(0) = 0$, and $f'(0) = 0$, the constant is 0; thus
         \[
         \left(f'(x)\right)^2 + (f(x))^2 = 0 \qquad \text{for all $x$} 
        .\] 
        This, in turn, implies that $f(x) = 0 $ for all $x$.
\end{proof}
\begin{theorem}{}{}
        If $f$ has a second derivative everywhere and 
        \begin{align*}
                f''+f &= 0, \\
                f(0) &= a, \\
                f'(0) &= b,
        \end{align*}
        then
        \[
        f = b \cdot \sin + a \cdot \cos.
        \] 
        (In particular, if $f(0) = 0$ and $f'(0) = 1$, then $f = \sin$; if $f(0) = 1$ and $f'(0)=0$, then $f = \cos$.)
\end{theorem}
\begin{proof}
        Let us define the function
        \[
                g(x) = f(x) - b\sin x -a\cos x
        .\] 
        Then we have:
        \begin{align*}
                g'(x) &= f'(x) - b\cos x + a\sin x,\\
                g''(x) &= f''(x) + b\sin x + a\cos x
        .\end{align*}
        Therefore,
        \begin{align*}
                g'' + g &= f'' + f = 0, \\
                g(0) &= a - a = 0, \\
                g'(0) &= b -b = 0
        .\end{align*}
        Thus theorem 3 shows that
        \[
                g(x) = f(x) - b\sin x - a \cos x = 0, \qquad \text{for all $x$}
        ;\]
        this completes the proof.
\end{proof}
\begin{theorem}{}{}
                If $x$ and $y$ are any two numbers, then
                \begin{align*}
                        \sin(x + y) &= \sin x \cos y + \cos x \sin y \\
                        \cos(x+y) &= \cos x \cos y - \sin x \sin y
                .\end{align*}
\end{theorem}
\begin{proof}
        For any particular number $y$, we may define the function
        \[
                f(x) = \sin(x+y)
        .\] 
        For such a function the following conditions hold:
        \begin{align*}
                f'(x) &= \cos(x+y), \\
                f''(x) &= -\sin(x+y),
        \end{align*}
        which implies that
        \begin{align*}
                f'' + f &= 0, \\
                f(0) &= \sin y, \\
                f'(0) &= \cos y
        .\end{align*}
        Hence theorem 4 tells us that
        \[
                f(x) = \sin (x + y) = \sin x (\cos y) + \cos x (\sin y)
        .\] 
        This proves the first formula, the second one can be deduced in a similar way.
\end{proof}

\section{Addendum}

Now we will mention an alternative approach to the definition of sin. Note that since
\[
        \arcsin'(x) = \frac{1}{\sqrt{1-x^2} }, \qquad \text{for $-1< x < 1$},
\] 
It follows from the Second Fundamental Theorem of Calculus that
\[
        \arcsin(x) = \arcsin(x) - \arcsin(0) = \int_0^{x}\frac{1}{\sqrt{1-t^2} } dt
.\] 
under such a definition, we could clearly derive the fact that
\[
        \sin'(x) = \sqrt{1-\sin^2 x}
,\]
which would then be defined as cos. Eventually, this would lead to show that $A(\cos x) = x /2$, as expected. An approach of this sort, however utterly unmotivated, can be very reasonable in dome cases.
