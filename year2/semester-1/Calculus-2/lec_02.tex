\lecture{2}{Fri 19 Feb 11:48}{The logarithm and exponential functions}

For certain functions, even  a preliminary definition presents difficulties. Take, for instance the function
\[
        f(x) = 10^{x}.
\] 
Such a function is assumed to be defined for all $x$ and to have an inverse function, defined for positive values of $x$, which is in turn the ``logarithm to the base 10,''
\[
        f^{-1}(x) = \log_{10} x
.\] 
As we shall see in this motivation, algebra defines $10^{x}$ only for \emph{rational} values of $x$, while quietly ignoring its definition for irrational $x$.

Firstly, the symbol $10^{n}$ is defined for the natural numbers $n$. This notation is particularly convenient because of the fact that
\[
10^{n}\cdot 10^{m} = 10^{n+m}  
.\]
Thus the extension of the definition of $10 ^{x}$ to rational $x$ is motivated by the desire to preserve this property; this actually forces the customary definition: Since we want
\[
10^{0}\cdot 10^{n} = 10^{0+n}= 10^{n}
,\] 
we must define $10^{0} = 1$; similarly, since we want that
\[
10^{-n}\cdot 10^{n} = 10^{0}= 1
,\]
we require that $10^{-n}=1 /10^{n}$ ; since we want that
\[
        \underbrace{10^{1 /n} \cdots 10^{1 /n}}_{\text{$n$ times}} = 10^{\underbrace{1 /n \cdots 1 /n}_{\text{$n$ times}}} = 10^{1} = 10,
\]
we must define $10^{1 /n}= \sqrt[n]{10} $; and since we want that 
\[
        \underbrace{10^{1 /n} \cdots 10^{1 /n}}_{\text{$m$ times}} = 10^{\underbrace{1 /n \cdots 1 /n}_{\text{$m$ times}}} = 10^{m /n},
\]
we must have $10^{m /n}= \left(\sqrt[n]{10}\right)^{m} $.

However, at this point, the extension of this definitions comes to a dead end. There is apparently nothing we can say about $10^{x}$ for irrational $x$ from a simple algebraic perspective. For such a reason, we shall try some more sophisticated ways of finding a function $f$ such that
 \[
         f(x+y)=f(x)\cdot f(y), \qquad \text{for all $x$ and $y$}
.\]
Of course, given that our function should not be always zero, we might add the condition that $f(1) \neq 0$. If we meant to be more specific, we could set $f(1) = 10$, and so the above equation would imply that $f(x) = 10^{x}$ for rational $x$, and $10^{x}$ could be \emph{defined} as $f(x)$ for all other $x$. (More generally, we say $f(x) = \left( f(1) \right) ^{x}$, for rational $x$.) 

One may attempt to find such a function by solving the following question: Is there a \emph{differentiable} function $f$ such that
\[
        f(x+y) = f(x)\cdot f(y), \qquad \text{for all $x$ and $y$},
\]
and $f(1)= 10$?
If, for a moment, we suppose that such function indeed exists, we can try to find $f'$ --knowing the derivative may be useful to find the function itself by virtue of the fundamental theorem of calculus. Now:
\begin{align*}
        f'(x) &= \lim_{h \to 0} \frac{f(x+h)-f(x)}{h} \\
              &= \lim_{h \to 0} \frac{f(x) \cdot f(h) - f(x)}{h}\\
              &= f(x) \cdot \lim_{h \to 0} \frac{f(h)-1}{h}
.\end{align*}
Note that, as we know by now, $f(0) = 1$, so the answer to the question about the derivative of $f$ depends on
 \[
         f'(0) = \lim_{h \to 0} \frac{f(h)-1}{h}
;\] 
assume that this limit exists, and denote it by $\alpha$. Then we have
\[
        f'(x) = \alpha \cdot f(x), \qquad \text{for all $x$}
.\] 
Even if we could compute the limit $\alpha$, this approach seems self-defeating, for the derivative $f'$ turns out to be given in terms of $f$ itself.

We thus examine the inverse function $f^{-1} = \log_{10}$:
\begin{align*}
        \log_{10}'(x) &= \frac{1}{f'\left(f^{-1}(x)\right)}\\
                      &= \frac{1}{\alpha \cdot f\left( f^{-1}(x) \right) } = \frac{1}{\alpha \cdot x}
.\end{align*} 

This is quite simple indeed! Now, note that since $\log_{10}1 = 0$, we should have
\[
        \frac{1}{\alpha} \int_1^{x}\frac{1}{t}dt = \log_{10}x - \log_{10}1 = \log_{10}x  
.\] 
This \emph{suggests} a definition; but not quite, since $\alpha$ is still an unknown value. One way of evading this difficulty is to define a function by
\[
\log x = \int_1^{x}\frac{1}{t}dt
,\] 
in the hopes of finding \emph{some} base for which the value of $\alpha$ is exactly 1. In any case, this function will be, from a mathematical perspective, much more reasonable than  $ \log_{10}$.
\begin{definition}{}{}
        If $x>0$, then
        \[
        \log x = \int_1^{x}\frac{1}{t}dt
        .\] 
\end{definition}

The graph of log is shown bellow. Note that if $x>1$, then $\log x > 0$, and if $0 <x<1$, then  $\log x< 0$, since, by our conventions,
\[
\int_1^{x}\frac{1}{t}dt = -\int_x^{1}\frac{1}{t}dt < 0
.\] 
For $x \le 0$, a number $\log x$ cannot be defined in this way, because $f(t) = 1 /t$ would not be bounded on $[x,1]$.
\begin{figure}[ht]
    \centering
    \incfig[0.5]{the-function-log}
    \caption{The function log}
    \label{fig:the-function-log}
\end{figure}

We could justify the notation ``log'' with the following theorem:
\begin{theorem}{}{}
        If $x, y >0$, then
        \[
                \log(xy) = \log x +\log y
        .\] 
\end{theorem}
\begin{proof}
        Notice first that $\log'(x) = 1 /x$, by the fundamental theorem of calculus. Now choose any number $y$ and let
        \[
                f(x) = \log(xy)
        .\] 
        Then we have
        \[
                f'(x) = \frac{1}{xy}\cdot y = \frac{1}{x}
        .\] 
        Thus $f' = \log'$. Hence, there is a number $c$ such that
        \[
                f(x) = \log x + c, \qquad \text{for all $x>0$}
        ,\] 
        that is,
        \[
                \log(xy) = \log x + c, \qquad \text{for all $x>0$}
        .\] 
        The number $c$ can be evaluated by noting that when $x = 1$, we get
        \[
                \log(1\cdot y) = \log 1 + c = c
        ,\]
        and so, $\log(xy) = \log x + \log y$ for all $x, y >0$.
\end{proof}
\begin{theorem}{Corollary}{}
        If $n$ is a natural number and $x>0$, then 
        \[
                \log(x^{n}) = n \log x
        .\] 
\end{theorem}
\begin{proof}
        Left as an exercise. (\emph{Hint}: Use induction.)
\end{proof}
\begin{theorem}{Corollary}{}
        If $x, y>0$, then
        \[
                \log\left(\frac{x}{y}\right) = \log x - \log y
        .\] 
\end{theorem}
\begin{proof}
        This follows directly from the fact that
        \[
                \log x = \log \left(\frac{x}{y}\cdot y\right) = \log\left( \frac{x}{y} \right) + \log y \implies \log\left(\frac{x}{y}\right) = \log x - \log y
        .\] 
\end{proof}

These theorems provide important information about the graph of the function log. it is clearly increasing, but since $\log'(x) = 1 /x$, its derivative decreases as $x$ becomes large; consequently, log grows ever more slowly. It is not clear, however, whether log is bounded on $\mathbb{R}$. Now, notice that, for any natural number $n$,
\[
        \log\left( 2^{n} \right) = n \log 2 \qquad \text{(and log 2 > 0);}
\]
hence, log can become arbitrarily large with arbitrarily large values of $n$. Similarly,
\[
        \log\left( \frac{1}{2^{n}} \right) = \log 1 - \log\left( 2^{n} \right) = -n \log 2;
\] 
which means that log can also become arbitrarily small (it is not bounded bellow on the interval $(0,1)$). These facts imply that the \emph{range} of the function log is $\mathbb{R}$, and so the \emph{domain} of $\log^{-1}$ is precisely $\mathbb{R}$.
\begin{definition}{The exponential function}{}
        The ``exponential function,'' \textbf{exp} is defined as $\log^{-1}$.  
\end{definition}

Note that, since the domain of log is $(0,\infty)$, we have that, for all $x$, $\exp(x) > 0$.
\begin{figure}[ht]
    \centering
    \incfig[0.15]{the-function-exp}
    \caption{The function Exp}
    \label{fig:the-function-exp}
\end{figure}
\begin{theorem}{Derivative of the exponential function}{}
        For all numbers $x$, 
        \[
                \exp'(x) = \exp(x)
        .\] 
\end{theorem}
\begin{proof}
        \begin{align*}
                \exp'(x) &= \left( \log^{-1} \right)'(x) = \frac{1}{\log'\left( \log^{-1}(x) \right) } \\
                         &= \frac{1}{\frac{1}{\log^{-1}(x)}} \\
                         &= \log^{-1}(x) = \exp(x)
        .\end{align*}
\end{proof}

Now we will prove a very significant property of exp:
\begin{theorem}{}{}
        If $x$ and $y$ are any two numbers, then
        \[
                \exp(x+y) = \exp(x) \cdot \exp(y)
        .\] 
\end{theorem}
\begin{proof}
        Let $\tilde x = \exp(x)$ and $\tilde y = \exp(y)$, so that
        \begin{align*}
                x &= \log \tilde x, \\
                y &= \log \tilde y 
        .\end{align*}
        Then,
        \[
                x+y = \log \tilde x + \log \tilde y = \log \left( \tilde{x}\tilde{y} \right) 
        .\]
        Applying exp to both sides yields
        \[
                \exp(x+y) = \tilde{x}\tilde{y} = \exp(x) \cdot \exp(y)
        .\] 
\end{proof}

The discussion at the beginning of the chapter along with the previous theorem suggest the importance of the number $\exp(1)$, which in fact has a special name:
\begin{definition}{The number $e$}{}
        \[
                e = \exp(1)
        .\]
        This is equivalent to the equation
        \[
        1 = \log e = \int_1^{e}\frac{1}{t}dt
        .\] 
\end{definition}
\begin{figure}[ht]
    \centering
    \incfig[0.3]{lower-and-upper-sums}
    \caption{lower and upper sums}
    \label{fig:lower-and-upper-sums}
\end{figure}

As illustrated, we can see that
\[
        \int_1^{2}\frac{1}{t}dt < 1\cdot (2-1) = 1, \qquad \text{since $1\cdot (2-1)$ is an upper sum for $f(t)$ on $[1,2]$,}
\]
and that
\[
        \int_1^{4}\frac{1}{t}dt > \frac{1}{2}(2-1)+\frac{1}{4}(4-2) = 1, \qquad \text{since $\frac{1}{2}(2-1)+\frac{1}{4}(4-2)$ is a lower sum for $f(t)$ on $[1,4]$}
.\] 
Thus we conclude that
\[
\int_1^{2}\frac{1}{t}dt < \int_1^{e}\frac{1}{t}dt < \int_1^{4}\frac{1}{t}dt \implies \log 2 < \log e < \log 4 \implies 2<e<4.
\]

As we remarked at the beginning of the chapter, the equation $\exp(x+y) = \exp(x) \cdot \exp(y)$ implies that 
\[
        \exp(x) = \left( \exp(1) \right)^{x} = e^{x}, \quad \text{for all rational $x$} 
.\]
This justifies the notation for the exponential function, for \emph{any} number $x$.
\begin{definition}{}{}
        For any number $x$,
        \[
                e^{x}=\exp(x)
        .\] 
\end{definition}

We have succeeded in finding $e^{x}$ for any value of $x$ (eve for irrational ones). However, we have yet to define a function $a^{x}$. With this in mind, note that
\[
        a^{x}=\left( e^{\log a} \right)^{x} = e^{x\log a}
.\]
Hence the definition:
\begin{definition}{}{}
        If $a>0$, then 
        \[
        a^{x}=e^{x\log a},
        \]
        for all $x$.
\end{definition}
In such a definition we require that $a>0$, in order that $ \log a$ be defined (for log is only defined for positive $a$.) This definition can now be used to show:
\begin{theorem}{}{}
        If $a>0$, then 
        \[
                \left( a^{b} \right)^{c} = a^{bc}
        ,\]
        for all $b$ and $c$. (Note that $a^{b}$will be positive, so $(a^{b})^{c}$ will certainly be well defined.)
        \[
        a^{1} = a, \text{ and } a^{x+y}=a^{x}\cdot a^{y}.
        .\] 
\end{theorem}
\begin{proof}
        \begin{align}
                \left( a^{b} \right)^{c}&=e^{c\left( \log a^{b} \right) }=e^{c\left( \log e^{b\log a} \right) }=e^{cb\log a}=a^{cb}.\\
                a^{1}&=e^{\log a}=a. \\
                a^{x+y}&=e^{(x+y)\log a}=e^{x\log a + y\log a}=e^{x\log a}\cdot e^{y\log a} =a^{x}\cdot a^{y}.
        \end{align}
\end{proof}

We can understand the behaviour of the function $a^{x}$ in the following manner: Suppose that $a>1$, then $\log a > 0$. Thus
\[
x<y \implies x\log a < y\log a \implies e^{x\log a} < e^{y\log a} \implies a^{x}<a^{y},
\] 
which is to say $f(x) = a^{x}$ is increasing whenever $a>1$. A similar reasoning shows that such a function is decreasing if $0<x<1$.

Just as we have defined the exponential function for any positive base, we can introduce the notation $\log_a x$ in the following way: Suppose that $y = \log_a x$, then 
\[
x=a^{y} \implies x=e^{y\log a} \implies \log x=y\log a \implies y = \frac{\log x}{\log a}
.\] 
Thus $\log_a x = \log x / \log a$.

The derivatives of these new function can be easily found:
\begin{align*}
        f(x) = e^{x\log a} = a^{x} &, \quad f'(x) = e^{x\log a}\cdot \log a = \log a \cdot a^{x},\\
        g(x) = \frac{\log x}{\log a} = \log_a x &, \quad g'(x) = \frac{\frac{1}{x}\log a}{(\log a)^2} = \frac{1}{x\log a}
.\end{align*}
The derivative of a more complicated function like $f(x) = g(x)^{h(x)}$ can also be computed by recalling the aforementioned definitions:
\begin{align*}
        f(x)&= e^{h(x)\log g(x)} \\
        f'(x)&= e^{h(x)\log g(x)}\cdot \left( h'(x)\log g(x) + h(x) \frac{g'(x)}{g(x)} \right)
.\end{align*}

Now we are in position to state the fact that the only function $f$ satisfying  $f'=f$ must be of the form $f(x) = ce^{x}$, for any constant $c$

\begin{theorem}{}{}
        If $f $ is differentiable and $f'(x)=f(x)$ for all $x$, then there is a number $c$ for which
        \[
                f(x) = ce^{x}
        .\] 
\end{theorem}
\begin{proof}
        Let 
        \[
                g(x) = \frac{f(x)}{e^{x}} 
        .\]
        Notice that the above function is indeed well defined, for $e^{x}>0$. Then:
        \[
                g'(x) = \frac{f'(x)e^{x}-f(x)e^{x}}{\left( e^{x} \right)^2 }=0
        .\] 
        Thus $g$ is a constant function, that is, there exists some number $c$ such that, for all $x$, $f(x)=ce^{x}$.
\end{proof}

A proposition that turns out to be true (however difficult to prove), is that any continuous function $f$ with $f(x+y) = f(x) \cdot f(y)$ must be of the form $f(x) = a^{x}$ or $f(x) = 0$. (See problem 38 in Spivak.)

Interestingly, there is a fact about the exponential function that may be useful in the future, namely ``exp grows faster than any polynomial.''
\begin{theorem}{}{}
        For any natural number $n$,
         \[
         \lim_{x \to \infty} \frac{e^{x}}{x^{n}}=\infty
         .\] 
\end{theorem}
\begin{proof}
        This proof is divided into three steps, namely:
        
        \textit{Step 1.} $e^{x} > x$ for all $x$, and so $\lim_{x \to \infty} e^{x}=\infty$.

        In order to show this, it suffices to prove that 
        \[
        x > \log x, \quad \text{for all $x$}
        ,\]
        for the statement is clearly true for $x \le 0$, which follows by the fact that the exponential function always yields positive real numbers. Note that if $0<x<1$, then $ \log x < 0$, so the statement holds. On the other hand, if $x>1$, then $1(x - 1)$ is a lower sum for  $f(t) = 1 /t$ on $[1,x]$, which implies that  $ \log x < x - 1<x$. Lastly, note that the proposition is also true in the particular case in which $x = 1$, since then  $\log x = 0$.

        \textit{Step 2.} $\lim_{x \to \infty} e^{x} /x = \infty$.
        
        Note that
        \[
                \frac{e^{x}}{x}=\frac{e^{x /2}\cdot e^{x /2}}{\displaystyle\frac{x}{2}\cdot 2}=\frac{1}{2} \left( \frac{e^{x /2}}{\displaystyle \frac{x}{2}} \right) e^{x /2}
        .\] 
        By Step 1, and given that $x>0$ (which certainly holds as  $x\to \infty$) the expression in parentheses is greater than 1. Thus, by what has already been shown, $e^{x /2}$ becomes arbitrarily large as $x$ does so, providing us with the desired result. 

        \textit{Step 3.} $\lim_{x \to \infty} e^{x} /x^{n}=\infty$.

        Finally, we must regard the fact that
        \[
                \frac{e^{x}}{x^{n}} = \frac{\left( e^{x/n} \right)^{n} }{\left( \displaystyle \frac{x}{n} \right)^{n} \cdot n^{n} } = \frac{1}{n^{n}} \left( \frac{e^{x /n}}{\displaystyle \frac{x}{n}} \right)^{n} 
        .\] 
        Since the expression in parentheses increases with no bound (by Step 2), it is safe to say that the whole expression behaves analogously in return.
\end{proof}
