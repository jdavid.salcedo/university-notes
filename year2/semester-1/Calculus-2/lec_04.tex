\lecture{4}{Wed 10 Mar 09:03}{Infinite Series}

Infinite sequences were introduced in the previous lectures with the specific intention to regard their ``sums''
\[
a_1+a_2+a_3+\cdots
\] 
in this lecture. However straightforward this may seem, we must not forget that the sum of infinitely many terms is completely undefined. What can be defined are ``partial sums,'' which are of the form
\[
s_n=a_1+\cdots+a_n;
\] 
an infinite sum is thus presumably defined in terms of these partial sums. Indeed, the same mechanism we previously introduce (namely, that of sequences) can be used to get the desired definition. Note that, if there happens to be an infinite sum $a_1+a_2+a_3+\cdots$, then the partial sums $s_n$ should give a better and better approximation to such a value as $n$ is chosen larger and larger. This amounts to say that $a_1+a_2+a_3+\cdots = \lim_{n \to \infty} s_n$. Note that a consequence of this definition is that many ``sums'' of sequences will be undefined, since the sequence $(s_n)$ may well fail to have a limit. For instance, the sequence
\[
1,-1,1,-1,1,\ldots
\] 
with $a_n = (-1)^{n+1}$ yields the new sequence $(s_n)$ with
\begin{align*}
        a_1 &= 1 \\
        a_2 &= 0 \\
        a_3 &= 1 \\
        a_4 &= 0 \\
            &\vdots
\end{align*}
and for which $\lim_{n \to \infty} s_n$ does not exist. Note then that some sequences will unavoidably have no sum. For this reason, any acceptable definition of the sum of a sequence should contain a terminology which distinguishes sequences for which sums can be defined from less fortunate sequences.
\begin{definition}{Summable sequence}{}
        The sequence $(a_n)$ is \textbf{summable} if the sequence $(s_n)$ converges, where
        \[
        s_n=a_1+\cdots+a_n.
        \] 
        In this case, $\displaystyle \lim_{n \to \infty} s_n$ is denoted by 
        \[
                \sum_{n=1}^{\infty} a_n \qquad \text{(or, less formally, by $a_1+a_2+a_3+\cdots$)}
        \] 
        and is called the \textbf{sum} of the sequence $(a_n)$.
\end{definition}

The terminology of this definition is usually replaced by less precise expressions. An infinite sum $\sum_{n=1}^{\infty} a_n$ is usually called an \emph{infinite series}, the word ``series'' is intended to emphasise the connection with the infinite sequence $(a_n)$. The statement that $(a_n) $ is or not summable is conventianally replaced by the statement that the series $\sum_{n=1}^{\infty} a_n$ does or does not converge. Notice how peculiar this terminology is: The symbol $\sum_{n=1}^{\infty} a_n$ can at best denote a number, a limit, (so it cannot ``converge''); notwithstanding the fact that it does not represent anything at all if $(a_n)$ is not summable. Nevertheless, this informal language is quite standard, and unlikely to put the logical grounds of the concept at risk.

Certain elementary arithmetical operations on infinite series are direct consequences of the definition, along with previous results. Namely,
\begin{align*}
        \sum_{n=1}^{\infty} (a_n+b_n) &= \sum_{n=1}^{\infty} a_n + \sum_{n=1}^{\infty} b_n, \\
        \sum_{n=1}^{\infty} c\cdot a_n &= c\cdot \sum_{n=1}^{\infty} a_n.
\end{align*}
As of right now these equations are not very interesting, since we have no examples of summable sequences (except, of course, for the trivial cases in which all the terms are eventually 0). Before giving more examples, however, some general conditions for summablility will be taken into account.

There is a necessary and sufficient condition for summability that can be stated immediately. Recall that, due to the Cauchy Condition, the sequence $(a_n)$ is summable if and only if $(s_n)$ converges , which happens if and only if $\displaystyle\lim_{m,n \to \infty} s_m - s_n = 0$; this can be rephrased as follows.
\begin{theorem}{The Cauchy Criterion}{}
        The sequence $(a_n)$ is summable if and only if
        \[
        \lim_{m,n \to \infty} a_{n+1}+\cdots+a_m = 0.
        \] 
\end{theorem}
Even though this criterion is of theoretical importance, it is not very useful for deciding the summadility of a given sequence. However, one simple consequence of the Cauchy criterion provides an \emph{necessary} condition for summability.
\begin{theorem}{The Vanishing Condition}{}
        If $(a_n)$ is summable, then
        \[
        \lim_{n \to \infty} a_n = 0.
        \] 
\end{theorem}
This condition follows from the Cauchy criterion by taking $m = n+1$ ; it can also be directly proven as follows: If $\displaystyle\lim_{n \to \infty} s_n = l$, then
\[
        \lim_{n \to \infty} a_n = \lim_{n \to \infty} (s_n - s_{n-1}) = \lim_{n \to \infty} s_n - \lim_{n \to \infty} s_{n-1} = l-l = 0.
\] 

Unfortunately, this condition is far from sufficient, for example, $\lim_{n \to \infty} 1 /n = 0$, but the sequence $(1/n)$ is not summable; in fact, the following grouping of the numbers $1 /n$ shows that the sequence $(s_n)$ is not bounded:
\[
        1+\frac{1}{2}+\underbrace{\frac{1}{3}+\frac{1}{4}}_{\ge \frac{1}{2}}+\underbrace{\frac{1}{5}+\frac{1}{6}+\frac{1}{7}+\frac{1}{8}}_{\ge \frac{1}{2}}+\underbrace{\frac{1}{9}+\cdots+\frac{1}{16}}_{\ge \frac{1}{2}} + \cdots
\]

We now provide some examples of convergent series. The most important of all infinite series are called ``geometric series'' 
\[
\sum_{n=0}^{\infty} r^{n} = 1+r+r^2+r^3+\cdots.
\] 
Only the cases $|r|<1$ are interesting, since the individual terms do not approach 0 if $|1| \ge 0$. These series can be managed because the partial sums 
\[
s_n = 1+r+\cdots+r^{n}
\] 
can be evaluated in simple terms. The equations
\begin{align*}
        s_n &= 1+r+r^2+\cdots+r^{n} \\
        rs_n &= r+r^2+\cdots+r^{n}+r^{n+1}
\end{align*}
imply that
\[
        s_n(1-r) = 1-r^{n+1}
\] 
or 
\[
        s_n = \frac{1-r^{n+1}}{1-r}
\] 
(division by $1-r$ is valid because we are not considering the case $r=1$). Now, $\lim_{n \to \infty} r^{n}=0$, since $|r| < 1$. It follows that
\[
        \sum_{n=1}^{\infty} r^{n}=\lim_{n \to \infty} \frac{1-r^{n+1}}{1-r}=\frac{1}{1-r}, \quad |r| < 1
\]
In particular, we can take
\[
        \sum_{n=1}^{\infty} \left( \frac{1}{2} \right) ^n = \sum_{n=0}^{\infty} \left( \frac{1}{1} \right)^{n} - 1 = \frac{1}{1-\frac{1}{2}}-1 = 1,
\] 
that is,
\[
\frac{1}{2}+\frac{1}{4}+\frac{1}{8}+\frac{1}{16}+\cdots = 1.
\] 
For now we will consider only \textbf{non-negative} sequences, that is, sequences $(a_n)$ such that every $a_n\ge 0$. If $(a_n)$ is a non-negative sequence, then $(a_n)$ is clearly non-decreasing. This remark, combined with the Monotone Convergence Theorem, provides a simple test for summability. 

\begin{theorem}{Boundedness Criterion}{}
        A non-negative sequence $(a_n)$ is summable if and only if the set of partial sums $s_n$ is bounded above.
\end{theorem}
\begin{proof}
        For the necessary condition, note that if the sequence $(a_n) $ is summable, then $\lim_{n \to \infty} s_n$ exists, so that $s_n$ is bounded (because every convergent sequence is bounded).

        On the other hand, for the sufficient condition, suppose the sequence $(s_n)$ is bounded (above); due to the fact that $(a_n)$ is non-negative, we can state that $(s_n)$ is also non-decreasing. Hence the Monotone Convergence Theorem implies that $(s_n)$ converges, which is to say $(a_n)$ is summable.
\end{proof}

By itself, this criterion is not very helpful ---for deciding whether the set $s_n$ is bounded or not is, in most cases, what we are unable to do. However, if some convergent series are already available for comparison, this criterion can be used to obtain a simple yet important result.

\begin{theorem}{The Comparison Test}{}
        Suppose that
        \[
        0\le a_n\le b_n \quad \text{for all $n$.}
        \] 
        Then if $\displaystyle \sum_{n=1}^{\infty} b_n$ converges, so does $\displaystyle \sum_{n=1}^{\infty} a_n$.
\end{theorem}
\begin{proof}
        If we let
        \begin{align*}
                s_n = a_1 +\cdots+a_n,
                t_n = b_1+\cdots+b_n,
        \end{align*}
        then the hypotheses imply that
        \[
        0\le s_n\le t_n, \quad \text{for all $n$.}
        \]
        Now, $(t_n)$ is bounded above, since $\displaystyle\sum_{n=1}^{\infty} b_n$ converges. Therefore $(s_n) $ is also bounded above; hence the boundedness criterion implies that $\displaystyle\sum_{n=1}^{\infty} a_n$ converges.
\end{proof}

This test is quite frequently used to analyse very complicated looking series in which most of the complication is actually irrelevant. Take, for instance,
\[
        \sum_{n=1}^{\infty} \frac{2+\sin^3(n+1)}{2^{n}+n^2};
\] 
this sequence converges because we can find a simpler convergent sequence that bounds it above. Namely, $ \sin^3(n+1)$ is at most 1, and since $2^{n}+n^2>2^{n}$, we have $1/(2^{n}+n^2) < 1/2^{n}$. Thus
\[
        0\le \frac{2+\sin^3(n+1)}{2^{n}+n^2} < \frac{3}{2^{n}},
\] 
and the latter expression is nothing but
\[
        3\left( \frac{1}{2} \right) ^{n},
\] 
a sequence which yields a geometric series with ratio $|r|<1$, and thus converges.

Likewise, we would expect the series
\[
\sum_{n=1}^{\infty} \frac{1}{2^{n}-1+\sin^2n^3}
\] 
to converge because, for large $n$, the value of the $n$th term of the series is practically $1/2^n$; and we would expect the series
\[
        \sum_{n=1}^{\infty} \frac{n+1}{n^2+1}
\] 
to diverge because, for large $n$, the values of the  $n$th term become practically equal to $1/n$. These last facts can be directly derived from the following theorem, another kind of ``comparison test.''
\begin{theorem}{The Limit Comparison Test}{}
If $a_n,b_n>0$ and $\displaystyle\lim_{n \to \infty} a_n /b_n = c \neq 0$, then
\[
\sum_{n=1}^{\infty} a_n \text{ converges if and only if } \sum_{n=1}^{\infty} b_n \text{ converges.}
\] 
\end{theorem}
\begin{proof}
Suppose first that $\displaystyle \sum_{n=1}^{\infty} b_n$ converges. Since $\lim_{n \to \infty} a_n /b_n = c$, by setting $\varepsilon=c$, we know that there is some $N$ such that
\[
        \left|\frac{a_n}{b_n} - c\right| < c, \quad \text{for $n\ge  N$}
\] 
this means that
\[
        \frac{|a_n - cb_n|}{|b_n|} < c
\]
or
\[
a_n - c b_n \le |a_n - c b_n| < c b_n \implies a_n < 2c b_n, \quad \text{for $n \ge N$.}
\]
But the sequence $2c \displaystyle\sum_{n=N}^{\infty} b_n$ converges by our hypothesis. Then by Comparison test, $\displaystyle\sum_{n=N}^{\infty} a_n$ also converges, and since the whole series $\displaystyle\sum_{n=1}^{\infty} a_n$ has only finitely many previous terms, we can state that it converges too.

The converse follows immediately, since $\lim_{n \to \infty} b_n /a_n = 1 /c \neq 0$, and we may apply the same process.
\end{proof}

The comparison test yields other important tests when we use previously analysed series as reference points. Choosing the geometric series $\displaystyle\sum_{n=0}^{\infty} r^{n}$ (the convergent series \textit{par excellence}) we obtain the most important of all tests for summability:
\begin{theorem}{The Ratio Test}{}
        Let $a_n>0$ for all $n$, and suppose that
        \[
                \lim_{n \to \infty} \frac{a_{n+1}}{a_n}=r.
        \] 
        Then $\displaystyle\sum_{n=1}^{\infty} a_n$ converges if $r<1$. On the other hand, if $r>1$, then the terms $a_n$ are unbounded so $\displaystyle\sum_{n=1}^{\infty} a_n$ diverges. (Note that it is therefore essential to compute $\lim_{n \to \infty} a_{n+1} /a_n$ and not $\lim_{n \to \infty} a_n /a_{n+1}$.) 
\end{theorem}
\begin{proof}
        Suppose first that $r<1$. Then we can choose a number $\varepsilon$ satisfying $r<r+\varepsilon<1$. The fact that
        \[
                \lim_{n \to \infty} \frac{a_{n+1}}{a_n} = r < 1
        \] 
        implies that there is some $N$ such that
        \[
                \left|\frac{a_{n+1}}{a_n} - r\right| < \varepsilon \implies -\varepsilon+r<\frac{a_{n+1}}{a_n}<r+\varepsilon \quad \text{for $n\ge N$.}
        \]  
        For simplicity, let $s=r + \varepsilon$. This certainly implies that
        \[
                \frac{a_{n+1}}{a_n} < s, \quad  \text{for $n\ge N$.}
        \] 
        This can in turn be written as
        \[
        a_{n+1}<sa_n \quad \text{for $n\ge N$.}
        \]
        Thus
        \begin{align*}
                a_{N+1} &< sa_N, \\
                a_{N+2} &< sa_{N+1} < s^2a_N, \\
                        &\vdots \\
                a_{N+k} &< s^{k}a_N.
        \end{align*}
        Hence, for all $k\in \mathbb{N}$, $a_{N+k}<s^{k}a_N$. Of course, 
        \[
        \sum_{k=0}^{\infty} a_Ns^{k} = a_N\sum_{k=0}^{\infty} s^{k}
        \] 
        is a convergent geometric series, for $|s|<1$; and so Comparison test shows that
         \[
        \sum_{k=0}^{\infty} a_{N+k} = \sum_{n=N}^{\infty} a_n
        \] 
        converges. This implies the convergence of the whole series $\displaystyle \sum_{n=1}^{\infty} a_n$, for it has only finitely many terms before the $N$th one.

        On the other hand, if $r>1$, then we can choose an $\varepsilon$ small enough that $1<r-\varepsilon<r$. Now, by the previously mentioned existence of the limit, we can write
        \[
                \frac{a_{n+1}}{a_n} > r-\varepsilon 
        \] 
        Once again, let $s=r-\varepsilon$, then the above inequality implies that, for all $k\in \mathbb{N}$,
        \[
        a_{N+k} > a_Ns^{k},
        \] 
        given that the right hand side of the inequality is unbounded, for $|s| >1$, we have that the terms of the left side are also unbounded. 
\end{proof}

As a simple application of the ratio test, consider the series $\displaystyle \sum_{n=1}^{\infty} 1 /n!$ Letting $a_n = a /n!$ we obtain
 \[
         \frac{a_{n+1}}{a_n}=\frac{\dfrac{1}{(n+1)!}}{\dfrac{1}{n!}} = \frac{n!}{(n+1)!} = \frac{1}{n+1}.
\] 
Thus
\[
\lim_{n \to \infty} \frac{a_{n+1}}{a_n} = 0,
\] 
which shows that the series $\displaystyle \sum_{n=1}^{\infty} a /n!$ converges.

If we instead consider the series $\displaystyle \sum_{n=1}^{\infty} r^{n} /n!$, where $r$ is some fixed positive number, then
\[
        \lim_{n \to \infty} \frac{\dfrac{r^{n+1}}{(n+1)!}}{\dfrac{r^{n}}{n!}} = \lim_{n \to \infty} \frac{r}{n+1} = 0,
\] 
so that $\displaystyle \sum_{n=1}^{\infty} r^{n} /n!$ converges. The vanishing condition thus implies that
\[
\lim_{n \to \infty} \frac{r^{n}}{n!}=0.
\] 

Finally, if we consider the series $\displaystyle \sum_{n=1}^{\infty} nr^{n}$, where $r$ is some non-negative number, we have
\[
        \lim_{n \to \infty} \frac{(n+1)r^{n+1}}{nr^{n}} = \lim_{n \to \infty} r \cdot \frac{n+1}{n} = r.
\] 
this shows that the series $\displaystyle \sum_{n=1}^{\infty} nr^{n}$ converges if $0\le r <1$, and consequently
\[
\lim_{n \to \infty} nr^{n}= 0.
\]
Although the ratio test will be of utmost theoretical importance, it will often disappoint in practice. One drawback of the ratio test is the fact that $\lim_{n \to \infty} a_{n+1} /a_n$ may be quite difficult to find, and may not even exist. More importantly, a deficiency which arises more regularly than what would be expected is the fact that the limit may equal 1; this case is precisely the one that is inconclusive:  $(a_n)$ might not be summable (for instance, if $a_n=1 /n$ ), but then again it might be. In fact, our very next test will show that $\displaystyle \sum_{n=1}^{\infty} (1/n)^2$ converges, even though
\[
        \lim_{n \to \infty} \frac{\left( \dfrac{1}{n+1} \right)^2}{\left( \dfrac{1}{n} \right)^2} = \lim_{n \to \infty} \left(\frac{n}{n+1}\right)^2 = 1.
\] 

The following theorem is a consequence of the Comparison test, but exhibits a quite novel kind of series.

\begin{theorem}{The Integral Test}{}
        Suppose that $f$ is positive and decreasing on $[0,\infty)$, and that $f(n) = a_n$ for all $n$. Then $\displaystyle \sum_{n=1}^{\infty} a_n$ converges if and only if the limit
        \[
        \int_1^{\infty}f = \lim_{A \to \infty} \int_1^{A}f
        \] 
        exists.
\end{theorem}
\begin{proof}
        The existence of $\displaystyle \lim_{A \to \infty} \int_1^{A}f$ is equivalent to the convergence of the series
        \[
        \int_1^{2}f+\int_1^{3}f+\int_1^{4}f+\cdots.
        \] 
        Now, since $f$ is decreasing we have the following upper and lower sums:
        \[
                [(n+1)-n]\cdot f(n+1) < \int_n^{n+1}f < [(n+1)-n]\cdot f(n) \implies a_{n+1} < \int_n^{n+1}f < a_n.
        \] 
        The first half of the inequality may be taken to mean that the series $\displaystyle \sum_{n=1}^{\infty} a_{n+1}$ may be compared (via Comparison test) to the series $\displaystyle \sum_{n=1}^{\infty} \int_n^{n+1}f$, proving that $\displaystyle \sum_{n=1}^{\infty} a_{n+1}$ (and consequently $\displaystyle \sum_{n=1}^{\infty} a_{n}$) converges if $\displaystyle \lim_{A \to \infty} \int_1^{A}f$ exists. 

        The second half of the inequality shows that the series $\displaystyle \sum_{n=1}^{\infty} \int_n^{n+1}f$ may be compared to the series $\displaystyle \sum_{n=1}^{\infty} a_{n}$, proving that $\displaystyle \lim_{A \to \infty} \int_1^{A}f$ must exist if $\displaystyle \sum_{n=1}^{\infty} a_{n}$ converges (again, by Comparison test).
\end{proof}

Only one example using the integral test will be provided here, but it settles the question for the convergence of infinitely many series at once. It is popularly known as the ``$p$-series criterion.'' If $p>0$, the convergence of $\displaystyle\sum_{n=1}^{\infty} 1/n^{p}$ is equivalent, by the integral test, to the existence of the improper integral
\[
\int_1^{\infty}\frac{1}{x^{p}}dx.
\] 
Now, it is easy to see that
\[
        \int_1^{A}\frac{1}{x^{p}} = \begin{cases}
                -\dfrac{1}{p-1}\cdot \dfrac{1}{A^{p-1}} + \dfrac{1}{p-1}, \quad p\neq 1 \\
                \log A, \quad p=1.
        \end{cases}
\] 
This shows that if $p>1$, then $\displaystyle\lim_{A \to \infty} \int_1^{A}1 /x^{p}$ exists because $1/A^{p-1}\to 0$ as $A\to \infty$; but whenever $p\le 1$, the limit does not exist, for in such a case $1/A^{p-1} \to \infty$. 

Thus $\displaystyle \sum_{n=1}^{\infty} 1/n^{p}$ converges precisely for $p>1$. In particular, the Harmonic Series $\displaystyle \sum_{n=1}^{\infty} 1 /n$ diverges.

The tests considered thus far apply only to non-negative sequences, but non-positive sequences may be handled in precisely the same way; since
\[
        \sum_{n=1}^{\infty} a_n=-\left(\sum_{n=1}^{\infty} -a_n\right),
\]
all considerations about non-positive sequences come down to questions involving non-negative ones. Nevertheless, sequences containing both positive and negative terms are quite different.

If $\displaystyle \sum_{n=1}^{\infty} a_n$ is a sequence with both positive and negative terms, one can consider instead the sequence $\displaystyle \sum_{n=1}^{\infty} |a_n|$, all of whose terms are non-negative. 

\begin{definition}{Absolute Convergence}{}
        The series $\displaystyle \sum_{n=1}^{\infty} a_n$ is \textbf{absolutely convergent} if the series $\displaystyle \sum_{n=1}^{\infty} |a_n|$ is convergent. 

        (More formally, the sequence $(a_n)$ is \textbf{absolutely summable} if the sequence $(|a_n|)$ is summable.)
\end{definition}

Turns out this definition is of utter importance, this will be seen through the following results.

\begin{theorem}{}{}
        Every absolutely convergent series is convergent. Moreover, a series is absolutely convergent if and only if the series formed from its positive terms and the series formed from its negative terms both individually converge.
\end{theorem}
\begin{proof}
        If $\displaystyle\sum_{n=1}^{\infty} |a_n|$ converges, then, by the Cauchy criterion,
        \[
        \lim_{m,n \to \infty} |a_{n+1}|+\cdots+|a_m| = 0.
        \] 
        Note that 
        \[
        |a_{n+1}+\cdots+a_m| \le |a_{n+1}|+\cdots+|a_m|.
        \] 
        Moreover, the definition of the hypothesised limit tells us that for all $\varepsilon>0 $ there exists a number $N$ such that $m,n >N$ implies
         \[
                 \left| |a_{n+1}|+\cdots+|a_m| \right| =|a_{n+1}|+\cdots+|a_m| <\varepsilon,
        \] 
        thus
        \[
        |a_{n+1}+\cdots+a_m|<\varepsilon \implies \lim_{m,n \to \infty} a_{n+1}+\cdots+a_m=0,
\]
which shows that $\displaystyle \sum_{n=1}^{\infty} a_n$ converges.

To prove the second part of the theorem, let
\begin{align*}
        {a_n}^{+}&= \begin{cases}
                a_n, \quad &\text{if $a_n\ge 0$} \\
                0, \quad &\text{if $a_n<0$}.
        \end{cases} \\
        {a_n}^{-}&= \begin{cases}
                a_n, \quad &\text{if $a_n\le 0$} \\
                0, \quad &\text{if $a_n>0$}.
        \end{cases}
\end{align*}
so that $\displaystyle \sum_{n=1}^{\infty} {a_n}^{+}$ is the series formed by the positive terms of $\displaystyle \sum_{n=1}^{\infty} a_n$, and correspondingly for ${a_n}^{-}$.

If $\displaystyle \sum_{n=1}^{\infty} {a_n}^{+}$ and $\displaystyle \sum_{n=1}^{\infty} {a_n}^{-}$ both converge, then
\[
        \displaystyle \sum_{n=1}^{\infty} {a_n}^{+} - \displaystyle \sum_{n=1}^{\infty} {a_n}^{-} = \sum_{n=1}^{\infty} [{a_n}^{+}-{a_n}^{-}] = \sum_{n=1}^{\infty} |a_n|
\] 
also converges, because it is the difference of two convergent series. Hence $\displaystyle \sum_{n=1}^{\infty} a_n$ converges absolutely.

On the other hand, if $\displaystyle \sum_{n=1}^{\infty} |a_n|$ converges, then (as we have already shown) $\displaystyle \sum_{n=1}^{\infty} a_n$ also converges. Now,
\begin{align*}
        a_n + |a_n| &= \begin{cases}
                2a_n, \quad &\text{if $a_n\ge 0$}\\
                0, \quad &\text{if $a<0$}
        \end{cases} \\
        a_n - |a_n| &= \begin{cases}
                2a_n, \quad &\text{if $a_n\le 0$} \\
                0, \quad &\text{if $a>0$},
        \end{cases}
\end{align*} 
so that the corresponding sequences are given by
\[
        \sum_{n=1}^{\infty} {a_n}^{+}=\frac{1}{2}\left( \sum_{n=1}^{\infty} a_n +\sum_{n=1}^{\infty} |a_n| \right)
\] 
and 
\[
        \sum_{n=1}^{\infty} {a_n}^{-}=\frac{1}{2}\left( \sum_{n=1}^{\infty} a_n -\sum_{n=1}^{\infty} |a_n| \right),
\] 
which is to say both are given by a sum of convergent series, so they both converge.
\end{proof}

From this theorem, it follows that any convergent series with positive terms can be used to obtain infinitely many other convergent series, simply by putting in minus signs at random. Not every convergent series, however, can be obtained in this way ---there are series which are convergent but not absolutely convergent (such series are called \textbf{conditionally convergent}. In order to prove this statement we need a test for convergence that applies specifically to series with positive and negative terms.

\begin{theorem}{Leibniz' Theorem or the Alternating Series Criterion}{}
        Suppose that 
        \[
        a_1\ge a_2\ge a_3\ge \cdots \ge 0,
        \] 
        and that 
        \[
        \lim_{n \to \infty} a_n = 0.
        \] 
        Then the series
        \[
                \sum_{n=1}^{\infty} (-1)^{n+1}a_n = a_1 - a_2 +a_3 -a_4 +a_5 - \cdots
        \] 
        converges.
\end{theorem}
\begin{proof}
        We shall show that the following conditions hold:
        \begin{align}
                s_2 &\le s_4\le s_6\le s_8 \le \cdots\\
                s_1 &\ge s_3\ge s_5 \ge s_7 \ge \cdots \\
                s_k &\le s_l, \quad \text{if $k$ is even and $l$ is odd.}
        \end{align}
\begin{figure}[ht]
    \centering
    \incfig[0.5]{proof-of-leibniz'-theorem}
    \caption{proof of Leibniz' Theorem}
    \label{fig:proof-of-leibniz'-theorem}
\end{figure}

To prove the first two inequalities observe that 
\begin{align*}
        s_{2n+2}&=s_{2n}+a_{2n+1}-a_{2n+2} \\
        &\ge s_{2n}, \quad \text{since $a_{2n+1}\ge a_{2n+2}$}\\
        s_{2n+3} &= s_{2n+1}-a_{2n+2}+a_{2n+3} \\
                 &\le s_{2n+1}, \quad \text{since $a_{2n+2}\ge a_{2n+3}$}.
\end{align*}

For the third inequality, notice first that
\begin{align*}
        s_{2n}&=s_{2n-1}-a_{2n} \\
              &\le s_{2n-1}, \quad \text{since $a_{2n}\ge 0$}.
\end{align*}
Of course, this proves only the special case of (3) in which the even and odd numbers are consecutive. The general case is easy to show: If  $k$ is even and $l$ is odd, choose $n$ such that
\[
2n \ge k \text{ and } 2n-1\ge l;
\] 
then, by (1), (2), and the inequality about even and odd consecutive numbers, 
\[
        s_k\le s_{2n}\le s_{2n-1} \le s_{l},
\] 
which proves (3).

Note that these inequalities imply that the sequence $(s_{2n})$ is non-decreasing and bounded above by $s_l$, where $l$ is any odd number; similarly, $(s_{2n+1})$ is non-increasing and bounded bellow by  $s_k$, with some even $k$. Thus they both converge. Let
\begin{align*}
        \alpha &= \sup\{s_{2n}\} = \lim_{n \to \infty} s_{2n}, \\
        \beta &= \inf\,\{s_{2n+1}\} = \lim_{n \to \infty} s_{2n+1}.
\end{align*}
From (3), it follows that $\alpha\le \beta$. Since
\[
s_{2n+1}-s_{2n} = a_{2n+1} \text{ and } \lim_{n \to \infty} a_{n}=0,
\] 
we actually have $\alpha = \beta$. Hence both sequences, for even and odd  $n$, converge to the same value, which is to say that the whole sequence of partial sums converges to such value:
 \[
\alpha = \beta = \lim_{n \to \infty} s_n.
\] 
This completes the proof.
\end{proof}

Now we give motivation for the topic of rearrangements of series. This theorem implies that the series
\[
1-\frac{1}{2}+\frac{1}{3}-\frac{1}{4}+\frac{1}{5}-\cdots
\] 
converges; note, however, that it does not converge absolutely because $\displaystyle \sum_{n=1}^{\infty} 1 /n$ does not converge. If the sum of the sequence is denoted by $x$, the following manipulations lead to a paradoxical result:
\begin{align*}
        x &= 1-\frac{1}{2}+\frac{1}{3}-\frac{1}{4}+\frac{1}{5}-\frac{1}{6}+\cdots \\
        &= 1-\frac{1}{2}-\frac{1}{4}+\frac{1}{3}-\frac{1}{6}-\frac{1}{8}+\frac{1}{5}-\frac{1}{10}-\frac{1}{12}+\frac{1}{7}-\frac{1}{14}-\frac{1}{16}+\cdots \\
        &\text{(the pattern here corresponds to one positive term followed by two negative ones.)}\\
        &= \left(1-\frac{1}{2}\right)-\frac{1}{4}+\left(\frac{1}{3}-\frac{1}{6}\right)-\frac{1}{8}+\left(\frac{1}{5}-\frac{1}{10}\right)-\frac{1}{12}+\left(\frac{1}{7}-\frac{1}{14}\right)-\frac{1}{16}+\cdots \\
        &= \frac{1}{2}-\frac{1}{4}+\frac{1}{6}-\frac{1}{8}+\frac{1}{10}-\frac{1}{12}+\frac{1}{14}-\frac{1}{16}+\cdots \\
        &= \frac{1}{2}\left(1-\frac{1}{2}+\frac{1}{3}-\frac{1}{4}+\frac{1}{5}-\frac{1}{6}+\cdots\right)  \\
        &= \frac{1}{2}x,
\end{align*}
so $x = x /2$, implying that  $x=0$. On the other hand, it is easy to see that $x\neq 0$: The partial sum $s_2$ equals $1 /2$, and the proof of Leibniz' theorem shows that it must be the case that $x\ge s_2$. We arrived at a contradiction!

This contradiction arises from the fact that we have taken for granted that operations valid for finite sums are necessarily valid for infinite sums. Indeed, it is true that the sequence
\[
        (b_n) = 1,-\frac{1}{2},-\frac{1}{4},\frac{1}{3},-\frac{1}{6},\cdots
\]
contains all the numbers in the sequence
\[
        (a_n) =1,-\frac{1}{2},\frac{1}{3},-\frac{1}{4},\frac{1}{5},-\frac{1}{6},\cdots.
\] 
In fact, $(b_n)$ is a \textbf{rearrangement} of $(a_n)$ in the following precise sense: Each $b_n=a_{f(n)}$ where $f$ is a certain function which ``permutes'' the natural numbers, viz., every natural number $m$ is $f(n)$ for \emph{precisely one} $n$. In our example
\begin{align*}
        f(2m+1) = 3m + 1 \quad &\text{(the terms $1,\frac{1}{3},\frac{1}{5},\ldots$ go in the 1st, 4th, 7th, \ldots places)},\\
        f(4m) = 3m \quad &\text{(the terms  $-\frac{1}{4},-\frac{1}{8},-\frac{1}{12},\ldots$ go into the 3rd, 6th, 9th,\ldots places)}, \\
        f(4m+2) = 3m+2 \quad &\text{(the terms $-\frac{1}{2},-\frac{1}{6},-\frac{1}{10},\ldots$ go into the 2nd, 5th, 8th, \ldots places)}.
\end{align*}

Nevertheless, there is no reason to assume that $\sum_{n=1}^{\infty} b_n$ should equal $\sum_{n=1}^{\infty} a_n$ : These are limits of sequences of partials sums, so the particular order of the terms can conceivably matter. The behaviour of the analysed sequence is typical of series which are not absolutely convergent---the following result shows how bad conditionally convergent series are.

\begin{theorem}{The Riemann Series Theorem}{}
        If $\displaystyle \sum_{n=1}^{\infty} a_n$ converges, but does not converge absolutely, then for any number $\alpha$ there is a rearrangement of $(b_n)$ of $(a_n)$ such that $\displaystyle \sum_{n=1}^{\infty} b_n=\alpha$.
\end{theorem}
\begin{proof}
        Let $\displaystyle \sum_{n=1}^{\infty} p_n$ denote the series formed from the positive terms of $(a_n)$, and let $\displaystyle \sum_{n=1}^{\infty} q_n$ denote the series of negative terms. Recall that a series converges absolutely if and only if both the series formed by its positive terms and the one formed by its negative terms converge; hence we know that if the series at issue does not absolutely converge, then either of these series does not converge. 

        As a matter of fact, it must be the case that both of them diverge, since if at least one of them converges we can easily prove that the other one does so as well, implying that $\displaystyle \sum_{n=1}^{\infty} a_n$ converges absolutely and thus contradicting our hypothesis. To show this note that
\begin{align*}
        \frac{a_n + |a_n|}{2} &= \begin{cases}
                a_n, \quad &\text{if $a_n\ge 0$}\\
                0, \quad &\text{if $a<0$}
        \end{cases} \\
                \frac{a_n - |a_n|}{2} &= \begin{cases}
                a_n, \quad &\text{if $a_n\le 0$} \\
                0, \quad &\text{if $a>0$},
        \end{cases}
\end{align*} 
so that $p_n = (a_n +|a_n|) /2$, and  $q_n = (a_n - |a_n|) /2$; note further that
\[
        q_n = a_n - p_n,
\] 
so supposing that, for instance (and without loss of generality), $p_n$ converges and considering the given fact that $a_n$ converges, then so does $q_n$. This is evidently a contradiction, by what we pointed out previously.

Now that we know both the positive and the negative series diverge we may proceed to the proof of the statement. Let $\alpha$ be any given number. Assume, for simplicity, that $\alpha>0$ (the proof for $\alpha<0$ will be a simple modification.) Since the series $\displaystyle \sum_{n=1}^{\infty} p_n$ diverges, it is unbounded, do there is a number $N$ for which 
 \[
\sum_{n=1}^{N} p_n > \alpha.
\] 
We shall chose $N_1$ to be the \emph{smallest} number satisfying this property. This means that
 \begin{align}
        &\sum_{n=1}^{N_1 - 1}p_n \le \alpha,\\
        &\text{but } \sum_{n=1}^{N_1} p_n>\alpha.
\end{align}
Then if we define 
\[
S_1 = \sum_{n=1}^{N_1} p_n,
\] 
we have, from equation (4) that 
\[
        \sum_{n=1}^{N_1-1} p_n = S_1 - p_{N_1} \le \alpha \implies \boxed{S_1-\alpha \le p_{N_1},}
\] 
this relation is intuitively clear from the picture
\begin{figure}[ht]
    \centering
    \incfig[0.5]{proof-of-the-riemann's-series-theorem}
    \caption{proof of the Riemann's series theorem}
    \label{fig:proof-of-the-riemann's-series-theorem}
\end{figure}

To the sum $S_1$ we can now add on just enough negative terms to obtain a new sum $T_1$ which is less than $\alpha$. In other words, we choose the smallest integer $M_1$ for which 
 \[
T_1 = S_1 + \sum_{n=1}^{M_1} q_n <\alpha,
\]
this is to say that
\[
S_1 + \sum_{n=1}^{M_1-1} q_n\ge \alpha, \text{ but } S_1 + \sum_{n=1}^{M_1} q_n < \alpha.
\]
Hence we can conclude that
\[
        S_1 + \sum_{n=1}^{M_1-1} q_n = T_1 - q_{M_1} \ge \alpha \implies \boxed{\alpha - T_1 \le -q_{M_1}.}
\] 
We now continue his procedure indefinitely (which we are allowed to do because the series of partial sums are not bounded), obtaining sums alternately larger and smaller than $\alpha$, each time choosing the smallest $N_k$ or $M_k$ possible. The sequence
\[
p_1,\ldots,p_{n_1},q_1,\ldots,q_{M_1},p_{N_1+1},\ldots,p_{N_2},\ldots
\] 
is a rearrangement of $(a_n)$. The partial sums of this rearrangement increase to $S_1$ m then decrease to $T_1$, then increase to $S_2$, then decrease again to $T_2$, etc. To complete the proof, we note that the boxed equations imply that 
\begin{align*}
        |S_k-\alpha|&\le p_{N_k},\\
        |T_k-\alpha|&\le -q_{M_k},
\end{align*}
and these terms, being terms of the original \emph{convergent} series $(a_n)$ (which, by the way, is convergent because of the Vanishing condition for convergent series), can be made arbitrarily small, thus approaching 0.
\end{proof}

Together with the previous theorem, the next one establishes conclusively the distinction between conditionally convergent and absolutely convergent series.

\begin{theorem}{}{}
        If $\displaystyle \sum_{n=1}^{\infty} a_n$ converges absolutely, and $(b_n)$ is any rearrangement of $(a_n)$, then $\displaystyle \sum_{n=1}^{\infty} b_n$ also converges (absolutely), and
        \[
        \sum_{n=1}^{\infty} a_n= \sum_{n=1}^{\infty} b_n.
        \] 
\end{theorem}
\begin{proof}
        Let us denote the partial sums of $(a_n)$ by $s_n$, and the partial sums of $(b_n)$ by $t_n$. Then suppose that $\varepsilon>0$. Since $\displaystyle \sum_{n=1}^{\infty} a_n$ converges, by definition there is some $N$ such that
        \[
        \left| \sum_{n=1}^{\infty} a_n - s_N\right| < \varepsilon.
        \] 
        Moreover, since $\displaystyle \sum_{n=1}^{\infty} |a_n|$ converges, we can also choose $N$ so that
        \[
                \sum_{n=1}^{\infty} |a_n|-(|a_1|+\cdots+|a_N|)\le \left|\sum_{n=1}^{\infty} |a_n| -(|a_1|+\cdots+|a_N|)\right| < \varepsilon,
        \] 
        i.e., so that 
        \[
        |a_{N+1}|+|a_{N+2}|+|a_{N+3}|+\cdots < \varepsilon
        \] 
        Now choose an integer $M$ so large that each of the $a_1,\ldots,a_N$ appear among $b_1,\ldots,b_M$. Then, whenever $m>M$, the difference $t_m-s_N$ corresponds to the sum of certain $a_i$, where $a_1,\ldots,a_N$ are excluded because they appear both on the sum $t_m$ and on $s_N$, so they cancel out. In fact, all of the terms in the sum $s_N$ cancel out, and we are left with some sum of $a_i$'s which appeared on $t_m$. Consequently,
        \[
        |t_m-s_N|\le |a_{N+1}|+|a_{N+2}|+|a_{N+3}|+\cdots < \varepsilon.
        \] 

        Thus if $m>M$, then
         \begin{align*}
                 \left|\sum_{n=1}^{\infty} a_n-t_m\right| &= \left|\sum_{n=1}^{\infty} a_n-s_N+(-t_m+s_N)\right| \\
                                                          &\le \left|\sum_{n=1}^{\infty} a_n - s_N\right| + |t_m-s_N|\\
&< \varepsilon + \varepsilon.                                                        
        \end{align*}
        Since this is true for any $\varepsilon>0$, the series $\displaystyle \sum_{n=1}^{\infty} b_n$ converges to $\displaystyle \sum_{n=1}^{\infty} a_n$.

        To show that $\sum_{n=1}^{\infty} b_n$ converges absolutely, note that $(|b_n|)$ is a rearrangement of $(|a_n|)$ ; and since $\displaystyle \sum_{n=1}^{\infty} |a_n|$ converges, then $\displaystyle \sum_{n=1}^{\infty} |b_n|$ also converges by the first part of this theorem.
\end{proof}

Absolute convergence is also important when it comes to multiplying two infinite series. Unlike the situation for addition, there is not quite so obvious a candidate for the product
\[
        \left( \sum_{n=1}^{\infty} a_n \right) \cdot \left( \sum_{n=1}^{\infty} b_n \right) = (a_1+a_2+\cdots)\cdot (b_1+b_2+\cdots).
\] 
It would seem that we ought to sum all the products $a_ib_i$ ; the trouble is that these yield a two-dimensional array, rather than a sequence:
\[
\begin{matrix}
        a_1b_1 & a_1b_2 & a_1b_3 & \cdots  \\
        a_2b_1 & a_2b_2 & a_2b_3 & \cdots  \\
        a_3b_1 & a_3b_2 & a_3b_3 & \cdots  \\
        \vdots & \vdots & \vdots & \\
\end{matrix}
\] 
Nevertheless, all the elements of this array can be arranged in a sequence. The picture here shows a way of doing this, and of course, there are infinitely many ways of doing the same thing.
\begin{figure}[ht]
    \centering
    \incfig[0.5]{array}
    \caption{array}
    \label{fig:proof-of-the-riemann-series-theorem}
\end{figure}

Suppose that $(c_n)$ is some sequence of this sort, containing each product $a_ib_j$ just once. Then we might naïvely expect to have
\[
\sum_{n=1}^{\infty} c_n=\sum_{n=1}^{\infty} a_n\cdot \sum_{n=1}^{\infty} b_n.
\] 
But this is not true (see problem 10), an it is not surprising, for we have not said anything about the specific arrangement of the terms. The next theorem shows the result does hold when the arrangement of terms is irrelevant.
\begin{theorem}{}{}
        If $\displaystyle \sum_{n=1}^{\infty} a_n$ and $\displaystyle \sum_{n=1}^{\infty} b_n$ converge absolutely, and $(c_n)$ is any sequence containing the products $a_ib_j$ for each pair $(i,j)$, then
        \[
        \sum_{n=1}^{\infty} c_n= \sum_{n=1}^{\infty} a_n\cdot \sum_{n=1}^{\infty} b_n.
        \] 
\end{theorem}
\begin{proof}
        Notice first that the sequence
        \[
        p_L = \sum_{i=1}^{L} |a_i|\cdot \sum_{j=1}^{L} |b_j|
        \] 
        converges, since $(a_n)$ and $(b_n)$ are absolutely convergent, and since the limit of a product is the product of the limits. So $(p_L)$ converges and thus is a Cauchy sequence, which means that for any $\varepsilon>0$, if $L$ and $L'$ are large enough, then
        \[
                \left| \sum_{i=1}^{L'} |a_i|\cdot \sum_{j=1}^{L'} |b_j| - \sum_{i=1}^{L} |a_i|\cdot \sum_{j=1}^{L} |a_j|\right| = \left| \sum_{\substack{1\le i\le L' \\ 1\le j\le L'}} |a_i|\cdot|b_j| - \sum_{\substack{1\le i\le L \\ 1\le j\le L}}|a_i|\cdot|a_j|\right|  < \frac{\varepsilon}{2}.
        \] 
        Of course, the above difference yields, assuming that $L'>L$, a sum of values $|a_i|\cdot|b_j|$ with $i>L$ or $j>L$, for the values corresponding to the partial sum up to $L$ (values whose indexes satisfy $i,j\le L$) cancel out with the first terms of the sum that goes up to $L'$, thus leaving us with some terms whose indexes are bounded not to be included in the first sum, viz., terms involving $i>L$ or $j>L$, which is precisely the negation of the condition to belong in the sum up to $L$. That is
        \[
                \boxed{\sum_{i \text{ or } j>L} |a_i|\cdot |b_j|<\frac{\varepsilon}{2}.}
        \]

        Now, suppose that $N$ is any number so large that the terms $c_n$ for $n\le N$ include every term $a_ib_j$ for $i,j\le L$. Then the difference
        \[
        \sum_{n=1}^{N} c_n-\sum_{i=1}^{L} a_i\cdot \sum_{j=1}^{L} b_j
        \] 
        consists of terms $a_ib_j$ with $i>L$ or $j>L$, so
        \[
                \boxed{\left|\sum_{n=1}^{N} c_n-\sum_{i=1}^{L} a_i\cdot \sum_{j=1}^{L} b_j\right|\le \sum_{i \text{ or } j >L} |a_i|\cdot |b_j| < \frac{\varepsilon}{2}}
        \] 

        But since the limit of a product is the product of the limits, and absolutely convergent sequences converge, we have 
        \[
                \boxed{\left| \sum_{i=1}^{\infty} a_i\cdot \sum_{j=1}^{\infty} b_j-\sum_{i=1}^{L} a_i\cdot \sum_{j=1}^{L} b_j \right| < \frac{\varepsilon}{2},}
        \] 
        for $L$ large enough. Consequently, if we choose $L$, and then $N$ large enough, we will have 
        \begin{align*}
                \left|\sum_{i_1}^{\infty} a_i\cdot \sum_{j=1}^{\infty} b_j - \sum_{n=1}^{N} c_n\right| &\le \left|\sum_{i=1}^{\infty} a_i\cdot \sum_{j=1}^{\infty} b_j-\sum_{i=1}^{L} a_i\cdot \sum_{j=1}^{L} b_j\right|+\left|\sum_{i=1}^{L} a_i\cdot \sum_{j=1}^{L} b_j-\sum_{n=1}^{N} c_n\right| \\
                                                                                                       &<\frac{\varepsilon}{2}+\frac{\varepsilon}{2} = \varepsilon,
        \end{align*}
        which completes the proof.
\end{proof}
