\lecture{6}{Thu 18 Mar 14:23}{Approximation by Polynomial Functions}

We shall encounter important theoretical results that reduce the computation of $f(x)$, for many functions $f$, to the evaluation of polynomial functions which are close approximations to $f$. In order to guess a polynomial which is appropriate, it is useful to first examine polynomial functions themselves:

Suppose that 
\[
        p(x) = a_0+a_1x+a_2x^2+\cdots+a_nx^{n}.
\] 
It is interesting, and very important for our purposes, to note that the coefficients $a_i$ can be expressed in terms of the value of $p$ and its various derivatives at $0$. To begin with, notice that
\[
        p(0) = a_0.
\] 
Differentiating the original expression yields 
\[
        p'(x) = a_1 + 2a_2x+\cdots+na_nx^{n-1}.
\] 
Therefore,
\[
        p'(0) = p^{(1)}(0) = a_1.
\] 
Differentiating again we get 
\[
        p''(0) = 2a_2+3\cdot 2a_3x+\cdots+n(n-1)a_nx^{n-2}.
\] 
Therefore
\[
        p''(0) = p^{(2)}(0) = 2a_2.
\] 
In general, we will have that 
\[
        p^{(k)}(0) = k! a_k \text{ or } a_k = \frac{p^{(k)}(0)}{k!}.
\] 
This formula holds true for $k=0$ if we define $0! = 1$ and $p^{(0)}=p$.

If we began instead with a ``polynomial in $(x-a)$,''
 \[
         p(x) = a_0+a_1(x-a)+\cdots+a_n(x-a)^{n},
\] 
then a very similar argument would show that 
\[
        a_k = \frac{p^{(k)}(a)}{k!}.
\] 

We are now ready to begin constructing polynomials on the basis of other functions. Suppose that $f$ is a function (not necessarily a polynomial) such that 
\[
        f^{(1)}(a), \ldots, f^{(n)}(a)
\] 
all exist. Let 
\[
        a_k = \frac{f^{(k)}(a)}{k!}, \quad 0\le k\le n,
\] 
and define a polynomial
\[
        P_{n, a}(x) = a_0+a_1(x-a)+\cdots+a_n(x-a)^{n}.
\] 
The polynomial $P_{n, a}$ is called the \textbf{Taylor polynomial of degree $n$ for $f$ at $a$}. (Sometimes this will be denoted by $P_{n,a,f}$, as to indicate the dependence on $f$.) The Taylor polynomial has been defined so that
\[
        {P_{n,a}}^{(k)}(a) = f^{(k)}(a), \quad \text{ for $0\le k\le n$;}
\] 
in fact, it is clearly \emph{the only polynomial of degree $\le n$ with this property}.

Although the coefficients of $P_{n,a,f}$ seem to strongly depend on $f$, the most important elementary functions have extremely simple Taylor polynomials. Consider first the function sin. We have:
\begin{align*}
        \sin(0) &= 0, \\ 
        \sin'(0) &= \cos(0) = 1, \\
        \sin''(0) &= -\sin(0) = 0, \\
        \sin'''(0) &= -\cos(0) = -1, \\
        \sin^{(4)}(0) &= \sin(0) = 0; 
\end{align*}
from this point onwards, it is clear that the derivatives repeat in a cycle of 4. Hence the numbers
\[
        a_k = \frac{\sin^{(k)}(0)}{k!}
\] 
are 
\[
0,1,0,-\frac{1}{3!},0,\frac{1}{5!},0,-\frac{1}{7!},0,\frac{1}{9!},\ldots.
\] 
Therefore the Taylor polynomial $P_{2n+1,0}$ of degree $2n+1$ for sin at 0 is
\[
        P_{2n+1,0} = x-\frac{x^{3}}{3!}+\frac{x^{5}}{5!}-\frac{x^{7}}{7!}+\cdots+\frac{x^{2n+1}}{(2n+1)!}. 
\] 
(Of course, $P_{2n+1,0} = P_{2n+2,0}$.)

Another example is the Taylor polynomial $P_{2n,0}$ of degree $2n$ for cos at 0, 
\[
        P_{2n,0}(x) = 1-\frac{x^2}{2!}+\frac{x^{4}}{4!}-\frac{x^{6}}{6!}+\cdots+(-1)^{n}\frac{x^{2n}}{(2n)!},
\] 
(the computations are left as an exercise). 

The Taylor polynomial for exp is especially easy to compute. Since $ \exp^{(k)}(0) = \exp(0) = 1$ for all $k$, the Taylor polynomial of degree $n$ at 0 is 
\[
        P_{n,0}(x) = 1+\frac{x}{1!}+\frac{x^2}{2!}+\frac{x^3}{3!}+\frac{x^{4}}{4!}+\cdots+\frac{x^{n}}{n!}.
\] 

The Taylor polynomial for log must be computed at some point  $a\neq 0$, since log is not even defined at 0. The standard choice for $a$ is 1. Then we have
\begin{align*}
        \log'(x) = \frac{1}{x}, \quad &\log'(1) = 1, \\
        \log''(x) = -\frac{1}{x^2}, \quad &\log''(1) = -1,\\
        \log'''(x) = \frac{2}{x^3}, \quad &\log'''(1) = 2
\end{align*}
in general, it can be proven by induction that for $n\ge 1$
\[
        \log^{(k)}(x) = \frac{(-1)^{k-1}(k-1)!}{x^{k}}, \quad \log^{(k)}(1) = (-1)^{k-1}(k-1)!
\] 
Therefore the Taylor polynomial of degree $n$ for log at 1 is 
 \[
         P_{n,1} = (x-1) - \frac{(x-1)^2}{2} + \frac{(x-1)^3}{3}+\cdots+\frac{(-1)^{n-1}(x-1)^{n}}{n}.
\]

It is, however, more convenient to regard the function $f(x) = \log(1+x)$; in this case we can choose $a=0$. We have
\[
        f^{(k)}(x) = \log^{(k)}(1+x),
\] 
so 
\[
        f^{(k)}(0) = \log^{(k)}(1) = (-1)^{k-1}(k-1)! \quad \text{as before.}
\] 
Therefore, the Taylor polynomial of degree $n$ for $\log(1+x)$ at 0 is 
\[
        P_{n,0}(x) = x - \frac{x^2}{2} + \frac{x^3}{3} - \frac{x^{4}}{4}+\cdots+\frac{(-1)^{n-1}x^{n}}{n}.
\]

There is one other elementary function whose Taylor polynomial is important: arctan. The computations of the derivatives begin
\begin{align*}
        \arctan'(x) = \frac{1}{1+x^2}, \qquad & \arctan'(0)=1, \\
        \arctan''(x) = \frac{-2x}{(1+x^2)^2}, \qquad & \arctan''(0)=0, \\
        \arctan'''(x) = \frac{(1+x^2)^2\cdot(-2)+2x\cdot 2(1+x^2)\cdot 2x}{(1+x^2)^{4}}, \quad & \arctan'''(0) = -2.
\end{align*}
t appears clear that this brute-force computation will never do. Nevertheless, the Taylor polynomials of arctan will be easily find after we have examined the features of Taylor polynomials in greater detail. We must find a deeper relation between $f$ and $P_{n,a,f}$.

Examining the Taylor polynomial of degree 1, we can notice some sort of relation between $f$ and its approximation:
 \[
         P_{1,a}(x) = f(a) + f'(a)(x-a) \implies f(x) - P_{1,a}(x) = f(x) - f(a) + f'(a)(x-a)
\] 
Note that
\[
        \frac{f(x) - P_{1,a}(x)}{x-a} = \frac{f(x)-f(a)}{x-a}-f'(a),
\] 
which implies that
\[
        \lim_{x \to a} \frac{f(x) - P_{1,a}(x)}{x-a} = \lim_{x \to a} \frac{f(x)-f(a)}{x-a} - f'(a) = f'(a) -f'(a) = 0.
\] 
In other words, as $x$ approaches $a$, the difference $f(x)-P_{1,a}(x)$ becomes small, even compared to $x-a$. 

This fact can be illustrated in particular with the function $f(x) = e^{x}$, whose Taylor polynomials of degree 1 and 2 at 0 are
 \begin{gather*}
         P_{1,0}(x) = 1+x \\
         P_{2,0}(x) = 1+x+\frac{x^2}{2}.
\end{gather*}
\begin{figure}[ht]
    \centering
    \incfig[0.6]{exp-and-two-of-its-taylor-polynomials}
    \caption{exp and two of its Taylor polynomials}
    \label{fig:exp-and-two-of-its-taylor-polynomials}
\end{figure}
As $x$ approaches 0, the difference $f(x) - P_{2,0}(x)$ seems to be getting small even faster than the difference $f(x) - P_{1,0}(x)$. This assertion may not be very precise as of now, but we are now ready to give it a definite meaning. We just noted that, in general, 
\[
        \lim_{x \to a} \frac{f(x) - P_{1,a}(x)}{x-a} = 0;
\] 
for $f(x) = e^{x}$ and $a=0$ this means that
\[
        \lim_{x \to 0} \frac{f(x) - P_{1,0}(x)}{x} = \lim_{x \to a} \frac{e^{x}-1-x}{x} = 0.
\]
On the other hand, an easy double application of l'Hôpital's Rule shows that
\[
\lim_{x \to 0} \frac{e^{x}-1-x}{x^2} = \lim_{x \to 0} \frac{e^{x}-1}{2x} = \lim_{x \to 0} \frac{e^{x}}{2} = \frac{1}{2} \neq 0.
\] 
Thus, although $f(x) - P_{1,0}(x)$ becomes small compared to $x$, as $x$ approaches 0, it does not become small compared to $(x-0)^2$. For $P_{2,0}$ the situation is different though; the extra term $x^2 /2$ provides just the right compensation:
\[
\lim_{x \to 0} \frac{e^{x}-1-x-\frac{x^2}{2}}{x^2} = \lim_{x \to 0} \frac{e^{x}-1-x}{2x} = \lim_{x \to 0} \frac{e^{x}-1}{2} = 0.
\] 
As it turns out, this result holds in general; if $f'(a)$ and $f''(a)$ exist, then 
\[
        \lim_{x \to a} \frac{f(x) - P_{2,a}(x)}{(x-a)^2} = 0;
\] 
in fact, the analogous assertion for $P_{n,a}$ is also true.

\begin{theorem}{}{}
        Suppose that $f$ is a function for which 
        \[
                f'(a), \ldots, f^{(n)}(a)
        \]
        all exist. Let 
        \[
                a_k = \frac{f^{(k)}(a)}{k!}, \quad 0\le k\le n,
        \] 
        and define
        \[
                P_{n,a} = a_0 + a_1(x-a) + a_2(x-a)^2 + \cdots + a_n(x-a)^{n}.
        \] 
        Then 
        \[
                \lim_{x \to a} \frac{f(x) - P_{n,a}(x)}{(x-a)^{n}} = 0.
        \] 
\end{theorem}
\begin{proof}
        Writing out $P_{n,a}(x)$ explicitly we obtain
        \[
                \frac{f(x) - P_{n,a}(x)}{(x-a)^n} = \frac{f(x) - \displaystyle \sum_{i=0}^{n-1} \dfrac{f^{(i)}(a)}{i!}(x-a)^{i}}{(x-a)^{n}} - \frac{f^{(n)}(a)}{n!}.
        \] 
        It will help to define the new functions
        \[
                Q(x) = \sum_{i=0}^{n-1} \frac{f^{(i)}(a)}{i!}(x-a)^{i} \text{ and } g(x) = (x-a)^{n};
        \] 
        we must therefore prove that
        \[
                \lim_{x \to a} \frac{f(x)-Q(a)}{g(x)} = \frac{f^{(n)}(a)}{n!},
        \] 
        since the last term is a constant. Notice that, for the $k$-th derivative of $Q$, all the terms whose exponents are less than $k$ will be cancelled out, whereas the $k$-th term is left as a constant. Hence
        \begin{align*}
                Q^{(k)}(a) &= f^{(k)}(a) + \sum_{i = k+1}^{n-1} \frac{f^{(i)}(a)}{i!}(a-a)^{i} = f^{(k)}(a), \quad k\le n-1, \\
                g^{(k)}(x) &= n(n-1)\cdots(n-(k-1))(x-a)^{n-k} = n!(x-a)^{n-k} /(n-k)! \\
        \end{align*}
        Note further that each polynomial $Q^{(k)}(x)$ is a continuous function; moreover, since $f^{(k)}(a)$ exists for $0\le k\le n-1$, and differentiability implies continuity, we get that
        \begin{align*}
                \lim_{x \to a} [f(x)-Q(x)] &= f(a)-Q(a) = f(a) - f(a) = 0, \\
                \lim_{x \to a} [f'(x)-Q'(x)] &= f'(a)-Q'(a) = f'(a)-f'(a) = 0, \\
                                             &\vdots\\
                \lim_{x \to a} [f^{(n-2)}(x) - Q^{(n-2)}(x)] &= f^{(n-2)}(a)-Q^{(n-2)}(a) = 0;
        \end{align*}
        and, since $g$ is clearly continuous,
        \[
                \lim_{x \to a} g(x) = \lim_{x \to a} g'(x) = \cdots = \lim_{x \to a} g^{(n-2)}(x) = 0.
        \] 
        We may therefore apply l'Hôpital's Rule $n-1$ times to obtain. 
        \[
                \lim_{x \to a} \frac{f(x) - Q(x)}{(x-a)^{n}} = \lim_{x \to a} \frac{f^{(n-1)}(x)-Q^{(n-1)}(x)}{n!(a-x)}.
        \] 
        Since $Q$ is a polynomial of degree $n-1$, its $(n-1)$-st derivative is a constant; indeed, $Q^{(n-1)}(x)=f^{(n-1)}(a)$. Thus we see that 
        \[
                \lim_{x \to a} \frac{f(x) - Q(x)}{(x-a)^{n}} = \lim_{x \to a}\frac{f^{(n-1)} - f^{(n-1)}(a)}{n!(x-a)},
        \] 
        and the last term is by definition $f^{(n)}(a)$, so the limit equals $f^{(n)}(a)/n!$. This completes the proof.
\end{proof}

A simple consequence of this theorem allows us to perfect the test for local maxima and minima which was developed on the basis of second derivatives. According to a theorem we have already shown, if $f''(a) < 0$, then the function $f$ has a local maximum at $a$; and if $f''(a) > 0$, then the function $f$ has a local minimum at $a$. If $f''(a)= 0$, no conclusion was possible; it is nevertheless conceivable that the sign of $f'''(a)$ might give further information in the case where $f''(a)=0$; and if $f'''(a)=0$, then the sign of $f^{(4)}(a)$ might be useful. More generally, we can ask what happens when
\[
        f'(a) = f''(a) = f'''(a) = \cdots = f^{(n-1)}(a) = 0,
\] 
but 
\[
        f^{(n)}(a) \neq 0.
\] 
The situation in this case can be guessed by examining the functions 
\begin{align*}
        f(x) &= (x-a)^{n} \\
        g(x) &= -(x-a)^{n}
\end{align*}
for which all derivatives up to the $(n-1)$-st one equal 0. Notice that if $n$ is odd, then $a$ is neither a local maximum nor a local minimum point for $f$ or $g$. On the other hand, if $n$ is even, then $f$ (with $f^{(n)}(a) > 0$) has a local minimum at $a$, while $g$ (with $g^{(n)}(a) < 0$) has a local maximum at $a$. This of course amounts to a particular case; however, they represent the general situation with functions of this nature.

\begin{figure}[ht]
    \centering
    \incfig[0.3]{local-maxima-and-minima}
    \caption{local maxima and minima}
    \label{fig:local-maxima-and-minima}
\end{figure}
\begin{theorem}{}{}
        Suppose that 
        \begin{align*}
                f'(a) &= \cdots = f^{(n-1)}(a) = 0, \\
                f^{(n)}(a) &\neq 0.
        \end{align*}
        \begin{enumerate}
                \item If $n$ is even and $f^{(n)}(a) > 0$, then $f$ has a local minimum at $a$.
                \item If $n$ is even and $f^{(n)}(a) < 0$, then $f$ has a local maximum at $a$.
                \item If $n$ is odd, then $f$ has neither a local maximum nor a local minimum at $a$.
        \end{enumerate}
\end{theorem}
\begin{proof}
        Let $g(x) = f(x) -f(a)$, so that $g(a) = 0$. Since  $f(a)$ is a constant, none of the hypotheses are modified, and thus apply to $g$. Now, since the first $n-1$ derivatives of $g$ at $a$ are 0, the Taylor polynomial $P_{n,a}$ of $f$ is 
        \begin{align*}
                P_{n,a}(x) &= g(a) + \frac{f'(a)}{1!}(x-a) + \cdots + \frac{f^{(n)}(a)}{n!}(x-a)^{n} \\
                           &= \frac{f^{(n)}(a)}{n!}(x-a)^{n}.
        \end{align*}
        Thus the first theorem of this chapter shows that
        \[
                0 = \lim_{x \to a} \frac{g(x) - P_{n,a}(x)}{(x-a)^{n}} = \lim_{x \to a} \left( \frac{f(x)}{(x-a)^{n}} - \frac{f^{(n)}(a)}{n!} \right). 
        \] 
        Consequently, if $x$ is sufficiently close to $a$, then 
        \[
                \frac{g(x)}{(x-a)^{n}} \text{ has the same sign as } \frac{g^{(n)}(a)}{n!},
        \] 
        for otherwise the limit would not be 0. 

        Now, suppose that $n$ is even. In this case $(x-a)^{n}>0$ for all $x \neq a$. Since $g(x) /(x-a)^{n}$ has the same sign as $g^{(n)}(a) /n!$ for $x$ sufficiently close to $a$, it follows that $g(x)$ itself must have the same sign as $g^{(n)}(a) /n!$ for $x$ sufficiently close to $a$. 
         \begin{enumerate}
                 \item If $f ^{(n)}(a)>0$, then $g^{(n)}>0$; this means that
                          \[
                                  g(x) > 0 \implies f(x)-f(a) > 0 \implies f(x) > f(a),
                          \] 
                          for $x$ sufficiently close to $a$. Since this happens to be true for $x$'s close "to the left" and "to the right" of $a$, we conclude that $a$ is a local minimum at $a$.
                  \item If $f^{(n)}(a)<0$, then $g^{(n)}(a) <0$; this implies that
                          \[
                                  f(x) < f(a)
                          \] 
                          for $x$ sufficiently close to $a$. Hence $f$ has a local maximum at $a$. 

                  \item Now suppose that $n$ is odd. A similar argument may be applied: Note that $g^{(n)}(a)/n!$ always has the same sign; on the other hand $g(x)/(x-a)^{n}$ must have the same sign as the former expression for $x$ sufficiently close to $a$. Therefore
        \[
                \frac{g(x)}{(x-a)^{n}} \text{ always has the same sign.}
        \] 
        But $(x-a)^{n}>0$ for $x>a$, and  $(x-a)^{n}<0$ for $x<a$. Hence $g(x)$ has different signs for $x>a$ and $x<a$; this proves that either of the following cases is true:
        \begin{align*}
                &\text{if } x>a, \text{ then } f(x) > f(a), \text{ and} \\
                &\text{if } x<a, \text{ then } f(x) < f(a)
        \end{align*}
        or 
        \begin{align*}
                &\text{if } x>a, \text{ then } f(x) < f(a), \text{ and} \\
                &\text{if } x<a, \text{ then } f(x) > f(a).
        \end{align*}
        And either way $f$ has neither a local maximum nor a local minimum at $a$.
        \end{enumerate}
\end{proof}
The conclusion of the first theorem is usually expressed in terms of the concept of ``order of equality.'' Two functions $f$ and $g$ are \textbf{equal up to order $n$ at $a$} if
\[
        \lim_{x \to a} \frac{f(x)-g(x)}{(x-a)^{n}}=0.
\] 
In the terminology of this definition, the fist theorem states that the Taylor polynomial $P_{n,a,f}$ equals $f$ up to order $n$ at $a$. The Taylor polynomial is in fact designed to make this fact plausible, because there is at most one polynomial of degree $\le n$ with this property. This assertion is a corollary of following theorem.

\begin{theorem}{}{}
        Let $P$ and $Q$ be two polynomials in $(x-a)$, of degree $\le n$, and suppose that $P$ and $Q$ are equal up to order $n$ at $a$. Then $P = Q$.
\end{theorem}   
\begin{proof}
        Let us define $R=P-Q$. Since $R$ is a polynomial of degree $\le n$, it is only necessary to prove that if
        \[
                R(x) = b_0 + b_1(x-a) + \cdots + b_n(x-a)^{n}
        \] 
        satisfies 
        \[
                \lim_{x \to a} \frac{R(x)}{(x-a)^{n}} = 0,
        \] 
        then $R = 0$. Now, the fact that $P$ and $Q$ are equal up to order $n$ certainly implies that
        \[
                \lim_{x \to a} \frac{R(x)}{(x-a)^{n}}=0 \quad \text{for $0\le i\le n$}.
        \] 
        For $i=0$, this condition simply means that $\lim_{x \to a} R(x) = 0$; on the other hand, since all coefficients are constants,
        \[
                \lim_{x \to a} R(x) = \lim_{x \to a} b_0+b_1(x-a) + \cdots + b_n(x-a)^{n} = b_0
        \] 
        Thus $b_0 = 0$, and
         \[
                 R(x) = b_1(x-a)+\cdots+b_n(x-a)^{n}.
        \] 
        Therefore, 
        \[
                \frac{R(x)}{x-a} = b_1 +b_2(x-a)+\cdots+b_n(x-a)^{n-1}
        \] 
        and applying once again the fact that $P$ and $Q$ are equal up to order $n$, we have
        \[
                \lim_{x \to a} \frac{R(x)}{x-a} = b_1 = 0.
        \] 
        Thus $b_1=0$ and
        \[
        R(x) = b_2(x-a)^2+\cdots+b_n(x-a)^{n}.
        \]
        Continuing in this way, we find that
        \[
        b_0 = \cdots = b_n = 0,
        \] 
        which completes the proof.
\end{proof}
\begin{corollary}{}{}
        Let $f$ be $n$-times differentiable at $a$, and suppose that $P$ is a polynomial in $(x-a)$ of degree $\le n$, which equals $f$ up to order $n$ at $a$. Then $P=P_{n,a}$.
\end{corollary}
\begin{proof}
        Note that the fact that $f$ is $n$-times differentiable implies that we can define the Taylor polynomial $P_{n,a,f}$. It is fairly easy to see that since $P$ and $P_{n,a,f}$ both equal $f$ up to order $n$, then for $x$ sufficiently close to $a$ we have 
        \begin{align*}
                \frac{|f(x)-P(x)|}{|x-a|^{n}} &< \frac{\varepsilon}{2} \\
                \frac{|f(x)-P_{n,a,f}(x)|}{|x-a|^{n}} &< \frac{\varepsilon}{2},
\end{align*}
this means that for $x$ sufficiently close to $a$ we also obtain
\begin{align*}
        \left| \frac{P(x)-P_{n,a,f}(x)}{(x-a)^{n}} \right| &= \frac{|P(x)-P_{n,a,f}(x)|}{|x-a|^{n}} \\
                                                           &\le \frac{|P(x)-f(x)| + |f(x)-P_{n,a,f}(x)|}{|x-a|^{n}}
                                                           < \varepsilon;
\end{align*}
this means that $P$ and $P_{n,a,f}$ are equal up to order $n$ at $a$. The theorem thus implies that $P=P_{n,a,f}$.
\end{proof}

Note that the existence of a polynomial $P$ that equals $f$ up to order $n$ is not sufficient to guarantee the existence of sufficiently many derivatives for the Taylor polynomial $P_{n,a,f}$ to exist. For instance, suppose that
 \[
         f(x) = \begin{cases}
                 x^{n+1}, \quad  \text{$x$ irrational} \\
                 0, \quad \text{$x$ rational.}
         \end{cases}
\] 
Notice that if $P(x) = 0$, then $P$ is certainly a polynomial of degree $\le n$ which equals $f$ up to order $n$ at 0, because $\lim_{x \to a} f(x) /x^{n} = 0$ (this can be easily seen by regarding separately the case in which $x$ is rational and the one in which it is not.) On the other hand $f'(a)$ does not exist for any $a\neq 0$, so $f''(0)$ is undefined.

When $f$ does have $n$ derivatives at $a$, however, the corollary may provide a useful method for finding the Taylor polynomial of $f$. In particular, we would like to find the Taylor polynomial for arctan, for which our initial attempt ended in failure. The equation
\[
\arctan x = \int_0^{x}\frac{1}{1+t^2}dt
\] 
suggests a promising method of finding a polynomial close to arctan. Notice first that the sum
\[
S = 1+x+x^2+\cdots+x^{n}
\] 
can be expressed in terms of a rational expression as follows:
\[
        xS = x+x^2+x^3+\cdots+x^{n+1} \implies S(1-x) = 1-x^{n+1},
\] 
thus
\[
        1+x+x^2+\cdots+x^{n} = \frac{1-x^{n+1}}{1-x} = \frac{1}{1-x} - \frac{x^{n+1}}{1-x},
\] 
which can also be written 
\[
\frac{1}{1-x} = 1+x+x^2+\cdots+x^{n} +\frac{x^{n+1}}{1-x}. 
\]
Now let $x = -t^2$, this gives
\[
        \frac{1}{1+t^2} = 1-t^2+t^4-t^{6}+\cdots+(-1)^{n}t^{2n} + \frac{(-1)^{n+1}t^{2n+2}}{1+t^2}.
\]

This formula shows that
\begin{align*}
        \arctan x &= \int_0^{x} \left(1-t^2+t^4-t^{6}+\cdots+(-1)^{n}t^{2n}\right)dt + (-1)^{n+1} \int_0^{x} \frac{t^{2n+2}}{1+t^2} dt \\
                  &= \underbrace{x - \frac{x^3}{3} +\frac{x^{5}}{5}-\cdots+(-1)^{n}\frac{x^{2n+1}}{2n+1}}_{P(x)} + (-1)^{n+1} \int_0^{x}\frac{t^{2n+2}}{1+t^2} dt
\end{align*}
Given the fact that $\arctan$ is $n$-times differentiable, we can conclude that the polynomial that appears here is the $(2n+1)$-degree Taylor polynomial for $\arctan$ at 0 if 
\[
        \lim_{x \to 0} \frac{\arctan x - P(x)}{x^{2n+1}} = 0,
\] 
hence if
\[
\lim_{x \to 0} \frac{\displaystyle \int_0^{x}\frac{t^{2n+2}}{1+t^2} dt}{x^{2n+1}} = 0
\] 
Since 
\[
\left|\int_0^{x}\frac{t^{2n+2}}{1+t^2}dt\right| \le \left|\int_0^{x}t^{2n+2}dt\right| = \frac{|x|^{2n+3}}{2n+3},
\] 
we can apply the Squeeze theorem to get that the above limit is indeed 0. Thus we have found that the Taylor polynomial of degree $2n+1$ for arctan at 0 is 
\[
        P_{2n+1,0}(x) = x - \frac{x^3}{3} +\frac{x^{5}}{5}-\cdots+(-1)^{n}\frac{x^{2n+1}}{2n+1}.  
\] 

Note that the estimate 
\[
\left|\int_0^{x}\frac{t^{2n+2}}{1+t^2}dt\right| \le \frac{|x|^{2n+3}}{2n+3}
\] 
implies that when $|x|<1$, this expression is at most  $1 /(2n+3)$, and we can make this as small as we like by simply choosing $n$ large enough. In other words, for $|x|\le 1$, \textbf{we can use the Taylor polynomials for arctan to compute $\arctan x$ as accurately as we desire}. The most important theorems about Taylor polynomials extend this result to other functions, and the Taylor polynomials will become of utter importance. Henceforth we will compare Taylor polynomials $P_{n,a}$ for fixed $x$ and different $n$. Firstly, we shall introduce some new terminology.

If $f$ is a function for which $P_{n,a}(x)$ exists, we define the \textbf{remainder term} $R_{n,a}(x)$ by 
\begin{align*}
        f(x) &= P_{n,a}(x) + R_{n,a}(x) \\
             &= f(a)+f'(a)(x-a)+\cdots+\frac{f^{(n)(a)}}{n!}(x-a)^{n}+R_{n,a}(x).
\end{align*}

We would like to have an expression for $R_{n,a}(x)$ whose size is easy to estimate. There is such an expression, involving an integral,just as in the case for arctan. One way to guess this expression is to begin with the case $n=0$:
 \[
         f(x) = f(a) + R_{0,a}(x).
\] 
Recall that, by the fundamental theorem of Calculus, we can write
\[
        f(x)-f(a) = \int_a^{x}f'(t) dt \implies f(x) = f(a) + \int_a^{x}f'(t) dt,
\] 
so that 
\[
        R_{0,a} = \int_a^{x} f'(t) dt.
\] 

A similar expression for $R_{1,a}(x)$ can be derived from this formula by using integration by parts in a rather tricky way: Let 
\[
        u(t) = f'(t) \text{ and } v(t) = t-x
\] 
(notice that $x$ is a fixed value, and so $v'(t) = 1$); then
 \begin{align*}
         \int_a^{x} f'(t)dt&=\int_0^{x} u(t)v'(t) dt \\
                           &= u(t)v(t)\Big|_a^{x} - \int_0^{x} u'(t)v(t)dt \\
                           &= f'(x)(x-x) - f'(a)(a-x) - \int_a^{x}f''(t)(t-x)dt.
\end{align*}
Hence we obtain, replacing in the expression,
\begin{align*}
        f(x) &= f(a) + \int_a^{x}f'(t)dt \\
             &= f(a) + f'(a)(x-a) + \int_a^{x} + \int_a^{x}f''(t)(x-t)dt.
\end{align*}
Thus
\[
        R_{1,a}(x) = \int_a^{x}f''(t)(x-t)dt.
\] 

It is easy to guess the formula for $R_{2,a}(x)$ if we let
\[
        u(t) = f''(t) \text{ and } v(t) = -\frac{(x-t)^2}{2};
\] 
in this case we have $v'(t) = x-t$, so that
 \begin{align*}
         \int_a^{x}f''(t)(x-t)dt &=\int_0^{x} u(t)v'(t) dt \\
                                 &= u(t)v(t)\Big|_a^{x} - \int_a^{x}u'(t)v(t)dt \\
                                 &= \frac{f''(a)(x-a)^2}{2} + \int_a^{x}\frac{f'''(t)}{2}(x-t)^2 dt. 
 \end{align*}
 This shows that 
 \[
         R_{2,a}(x) = \int_a^{x}\frac{f^{(3)}(t)}{2}(x-t)^2 dt.
 \] 

 We may then proceed inductively as follows: Suppose that $f^{(n+1)}$ is continuous on $[a,x]$, and suppose that 
  \[
          R_{n-1,a}(x) = \int_a^{x} \frac{f^{(n)}(t)}{(n-1)!}(x-t)^{n-1} dt.
 \] 
 Then we may establish the following:
 \[
         u(t) = f^{(n)}(t) \text{ and } v(t) = - \frac{(x-t)^{n}}{n},
 \] 
 in this case $v'(t) = (x-t)^{n-1}$, and 
  \begin{align*}
          \int_a^{x} \frac{f^{(n)}(t)}{(n-1)!}(x-t)^{n-1} dt &= \frac{1}{(n-1)!} \int_a^{x} u(t)v'(t) dt \\
                                                             &= \frac{1}{(n-1)!} \left(u(t)v(t) \Big|_a^{x} - \int_a^{b} u'(t)v(t) dt \right)\\
                                                             &= \frac{1}{(n-1)!} \left( \frac{f^{(n)}(a)}{n} (x-a)^{n} + \int_a^{x} \frac{f^{(n+1)}(t)}{n}(x-t)^{n} dt\right) \\
                                                                     &= \frac{f^{(n)}(a)}{n!} (x-a)^{n} + \int_a^{x} \frac{f^{(n+1)}(t)}{n!}(x-t)^{n} dt,
 \end{align*}
 and since the function $f^{(n+1)}$ is continuous on $[a,x]$, the last integral exists. This proves that, in general,
  \[
          R_{n,a}(x) = \int_a^{x} \frac{f^{(n+1)}(t)}{n!}(x-t)^{n} dt.
 \]

 This formula is called the integral form of the remainder, and we can easily estimate it in terms of estimates for $f^{(n+1)}/n!$ on the interval $[a,x]$. If $m$ and $M$ are the minimum and maximum of $f^{(n+1)}/n!$ on $[a,x]$, then  $R_{n,a}(x)$ satisfies
 \[
         m \int_a^{x}(x-t)^{n}dt \le R_{n,a}(x) \le M \int_a^{x}(x-t)^{n} dt,
 \] 
 so we can write
 \[
         R_{n,a}(x) = \alpha \cdot \frac{(x-a)^{n+1}}{n+1}
 \] 
 for some number $\alpha $ between $m$ and $M$. Since we have assumed that $f^{(n+1)}$ is continuous on $[a,x]$, we know that it takes on every value between $f(a)$ and  $f(x)$; hence $f^{(n+1)}/n!$ takes on all values between $f(a) /n!$ and $f(x) /n!$, and such an interval contains the interval of numbers between $M$ and $m$. Hence, for some $t$ in $(a,x)$, we can write
 \[
         R_{n,a}(x) = \frac{f^{(n+1)}(t)}{n!} \frac{(x-a)^{n+1}}{n+1} = \frac{f^{(n+1)}(t)}{(n+1)!}(x-a)^{n+1},
 \] 
 which is called the Lagrange form of the remainder.

 The Lagrange form of the remainder is the one we will need in almost all cases, and we can even give a proof that does not require $f^{(n+1)}$ to be continuous (this refinement is of little importance in most applications, though, because we often assume that $f$ has derivatives of all orders). This is the form of the remainder that we will choose for Taylor's theorem.

 \begin{lemma}{}{}
         Suppose that the function $R$ is $(n+1)$-times differentiable on $[a,b]$, and
         \[
                 R^{(k)}(a) = 0 \quad \text{for } k= 0,1,2,3,\ldots,n.
         \] 
         Then for any $x$ in $(a,b]$ we have
          \[
                  \frac{R(x)}{(x-a)^{n+1}} = \frac{R^{(n+1)}(t)}{(n+1)!} \quad \text{for some $t$ in $(a,x)$}.
         \] 
 \end{lemma}
 \begin{proof}
         For $n=0$, this is just the Mean Value Theorem: Recall that $R(a) = 0$, so there is some $t$ in $(a,x)$ so that
         \[
                 R'(t) = \frac{R(x) - R(a)}{x-a} = \frac{R(x)}{x-a}.
         \] 
         
         We will proceed by induction on $n$. So suppose that any $(n+1)$-times differentiable function $R$ on a closed interval $I$ with 
         \[
                 R^{(1)}(a) = R^{(2)}(a) = \cdots = R^{(n)}(a) = 0
         \] 
         satisfies  
         \[
         \frac{R(x)}{(x-a)^{n+1}} = \frac{R^{(n+1)}(t)}{(n+1)!} \quad \text{for some $t$ in the interior of $I$}.
         \] 

         To prove that this also holds for an $n+2$ differentiable function $R$, we use the Cauchy Mean Value Theorem to write
         \[
                 \frac{R(x)}{(x-a)^{n+2}} = \frac{R(x)-R(a)}{(x-a)^{n+2}-(a-a)^{n+2}} = \frac{R'(z)}{(n+2)(z-a)^{n+1}} = \frac{1}{n+2}\frac{R'(z)}{(z-a)^{n+1}}\quad \text{for some $z$ in $(a,x)$}.  
         \]
         Note that if $R$ is $(n+2)$-times differentiable, then $R'$ is $(n+1)$-times differentiable, so we may apply the induction hypothesis to $R'$ on the interval $[a,z]$ to get
         \begin{align*}
                 \frac{R(x)}{(x-a)^{n+2}}&= \frac{1}{n+2}\frac{(R')^{(n+1)}(t)}{((n+1)!} \quad \text{for some $t$ in $(a,z)\subset (a,x)$} \\
                                         &= \frac{R^{(n+2)}(t)}{(n+2)!}.
         \end{align*}
         This completes the proof.
 \end{proof}

 \begin{theorem}{Taylor's Theorem}{}
         Suppose that $f', \ldots, f^{(n+1)}$ are defined on $[a,x]$, and that $R_{n,a}(x)$ is defined by
         \[
                 f(x) = f(a) +f'(a)(x-a)+\cdots+\frac{f^{(n)}(a)}{n!}(x-a)^{n}+R_{n,a}(x).
         \] 
         Then 
         \[
                 R_{n,a}(x) = \frac{f^{(n+1)}(t)}{(n+1)!}(x-a)^{n+1} \quad \text{for some $t$ in $(a,x)$}
         \] 
         (Lagrange form of the remainder).
 \end{theorem}
 \begin{proof}
         The function $P_{n,a}$ is certainly $(n+1)$-times differentiable according to how it has been defined (because it is a polynomial, and polynomials are infinitely many times differentiable); therefore
          \[
                  \frac{R_{n,a}(x)}{(x-a)^{n+1}} = \frac{{R_{n,a}}^{(n+1)}(t)}{(n+1)!}
         \] 
         for some $t$ in $(a,x)$. But $R_{n,a} - f$ is an $n$-degree polynomial, so differentiating it $(n+1)$ times gives 0; hence
         \[
                 {R_{n,a}}^{(n+1)} = f^{(n+1)}.
         \] 
         Finally, this implies
         \[
                 R_{n,a}(x) = \frac{f^{(n+1)}(t)}{(n+1)!}(x-a)^{n+1},
         \] 
         completing the proof.
 \end{proof}

 Applying Taylor's Theorem to the functions sin, cos, and exp, with  $a=0$, we obtain the following formulas:
 \begin{align*}
         \sin x &= x-\frac{x^3}{3!}+\frac{x^{5}}{5!}-\cdots+(-1)^{n}\frac{x^{2n+1}}{(2n+1)!} + \frac{\sin^{(2n+2)}(t)}{(2n+2)!}x^{2n+2} \\
         \cos x &= 1-\frac{x^2}{2!}+\frac{x^{4}}{4!}-\cdots+(-1)^{n}\frac{x^{2n}}{(2n)!} + \frac{\cos^{(2n+1)}(t)}{(2n+1)!}x^{2n+1} \\
         e^{x} &= 1+x+\frac{x^2}{2!}+\cdots+\frac{x^{n}}{n!} + \frac{e^{t}}{(n+1)!}x^{n+1}
 \end{align*}
 (of course, we could have gone one power further in the first two expressions, for the next power of the taylor polynomial has coefficient 0.)
 
 Estimates for the first two are espexially easy to find. Since 
 \[
         |\sin^{(2n+2)}(t)|\le 1,
 \] 
 we have 
 \[
         \left| \frac{\sin^{(2n+2)}(t)}{(2n+2)!}x^{2n+2}\right| \le \frac{|x|^{2n+2}}{(2n+2)!}.
 \] 
 Similarly, we can show that
 \[
         \left| \frac{\cos^{(2n+1)}(t)}{(2n+1)!}x^{2n+1}\right| \le \frac{|x|^{2n+1}}{(2n+1)!}.
 \] 
 These estimates are particularly interesting because for any $\varepsilon>0$, we can show that
 \[
 \frac{x^{n}}{n!}<\varepsilon
 \] 
 for large enough values of $n$ (how large $n$ must be will depend on $x$). Indeed, if we have some $n \ge 2x$, then $n+1>2x$; this is to say that $1/2 > x/(n+1)$. Hence
 \[
         \frac{x^{n+1}}{(n+1)!} = \frac{x}{n+1} \frac{x^{n}}{n!} < \frac{1}{2} \frac{x^{n}}{n!}. 
 \] 
 With this in mind, let $n_0 \in \mathbb{N}$ with $n_0 \ge 2x$. Then, whatever value $x^{n_0}/(n_0)!$, the succeeding values satisfy:
\begin{align*}
        \frac{x^{n_0+1}}{(n_0+1)!} &< \frac{1}{2} \frac{x^{n_0}}{(n_0)!} \\
        \frac{x^{n_0+2}}{(n_0+2)!} &< \frac{1}{2} \frac{x^{n_0+1}}{(n_0+1)!} < \frac{1}{2}\frac{1}{2}\frac{x^{n_0}}{(n_0)!} \\
                                    &\vdots \\
         \frac{x^{n_0+k}}{(n_0+k)!} &< \frac{1}{2^{k}} \frac{x^{n_0}}{(n_0)!} \\
\end{align*}
Hence if we let $k$ be so large that $\displaystyle 2^{k}>\frac{x^{n_0}}{(n_0)!\varepsilon}$, then we would have
\[
        \frac{x^{n_0+k}}{(n_0+k)!}<\varepsilon,
\] 
which proves the statement.

The conditions mentioned above enable us to compute $\sin x$ to any degree of accuracy desired simply by evaluating the proper Taylor polynomial $P_{n,0}(x)$. For instance, suppose that we wish to compute $\sin 2$ with an error of less than $10^{-4}$. Since 
\[
        \sin 2 = P_{2n+1,0}(2) + R, \quad \text{where } |R| < \frac{2^{2n+2}}{(2n+2)!},
\] 
we can use $P_{2n+1,0}(2)$ as our answer provided that 
\[
\frac{2^{2n+2}}{(2n+2)!} < 10^{-4}
\] 
A number $n$ with this property may be found by a straightforward search (it helps to have a table of values for $n!$ and $2^{n}$). In this case, it turns out that $n=5$ works, so that
\begin{align*}
        \sin 2 &= P_{11,0}(2) + R \\
               &= 2-\frac{2^{3}}{3!}+\frac{2^{5}}{5!}-\frac{2^{7}}{7!}+\frac{2^{9}}{9!}-\frac{2^{11}}{11!} + R, \quad \text{where } |R|<10^{-4}.
\end{align*}

That's it.
